angular.module('vpApp').config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider){
    $routeProvider.when('/admin', {
        templateUrl: 'Admincontroller/loadView/dashboard'
    })
    .when('/admin/dashboard', {
        templateUrl: 'Admincontroller/loadView/dashboard'
    })
    .when('/admin/languages', {
        templateUrl: 'Admincontroller/loadView/languages'
    })
    .when('/admin/users', {
        templateUrl: 'Admincontroller/loadView/users'
    })
    .when('/admin/about', {
        templateUrl: 'Admincontroller/loadView/about'
    })
    .when('/admin/about-2', {
        templateUrl: 'Admincontroller/loadView/about2'
    })
    .when('/admin/contact', {
        templateUrl: 'Admincontroller/loadView/contact'
    })
    .when('/admin/partner', {
        templateUrl: 'Admincontroller/loadView/partner/contact'
    })
    .when('/admin/vi-tri', {
        templateUrl: 'Admincontroller/loadView/position/position'
    })
    .when('/admin/tong-quan-vi-tri', {
        templateUrl: 'Admincontroller/loadView/tong-quan/position'
    })
    .when('/admin/chi-tiet-vi-tri', {
        templateUrl: 'Admincontroller/loadView/chi-tiet/position'
    })
    .when('/admin/noi-dung-loi-ich', {
        templateUrl: 'Admincontroller/loadView/tong-quan/benefit'
    })
    .when('/admin/danh-sach-loi-ich', {
        templateUrl: 'Admincontroller/loadView/list/benefit'
    })
    .when('/admin/tai-lieu', {
        templateUrl: 'Admincontroller/loadView/tailieu'
    })
    .when('/admin/content', {
        templateUrl: 'Admincontroller/loadView/content'
    })
    .when('/admin/connect', {
        templateUrl: 'Admincontroller/loadView/connect'
    })
    .when('/admin/templates', {
        templateUrl: 'Admincontroller/loadView/templates'
    })
    .when('/admin/categories', {
        templateUrl: 'Admincontroller/loadView/categories/category'
    })
    .when('/admin/productCategories', {
        templateUrl: 'Admincontroller/loadView/categories/category'
    })
    .when('/admin/article', {
        templateUrl: 'Admincontroller/loadView/articles/article'
    })
    .when('/admin/article-tq', {
        templateUrl: 'Admincontroller/loadView/articletq/article'
    })
    .when('/admin/widgets', {
        templateUrl: 'Admincontroller/loadView/widgets'
    })
    .when('/admin/menu', {
        templateUrl: 'Admincontroller/loadView/navigates'
    })  
    .when('/admin/slide', {
        templateUrl: 'Admincontroller/loadView/cateSlides'
    }) 
    .when('/admin/image', {
        templateUrl: 'Admincontroller/loadView/image'
    }) 
    .when('/admin/slide/:cateId', {
        templateUrl: 'Admincontroller/loadView/slide'
    }) 
    .when('/admin/product', {
        templateUrl: 'Admincontroller/loadView/products/product'
    })  
    .when('/admin/orders', {
        templateUrl: 'Admincontroller/loadView/orders'
    })        
    .when('/admin/customer', {
        templateUrl: 'Admincontroller/loadView/customers'
    })
    .when('/admin/postman', {
        templateUrl: 'Admincontroller/loadView/postmans'
    })     
    .when('/admin/service', {
        templateUrl: 'Admincontroller/loadView/services'
    })
    .when('/admin/address', {
        templateUrl: 'Admincontroller/loadView/map'
    })
    .when('/admin/resources', {
        templateUrl: 'Admincontroller/loadView/resourceManage'
    })
    .when('/admin/contactmail', {
        templateUrl: 'Admincontroller/loadView/mailcontact'
    })    
    .when('/admin/article/add', {
        templateUrl: 'Admincontroller/loadView/editArticle/article'
    })
    .when('/admin/product/add', {
        templateUrl: 'Admincontroller/loadView/editProduct/product'
    })    
    .when('/admin/category/add', {
        templateUrl: 'Admincontroller/loadView/editCategory/category'
    })
    .when('/admin/productCategory/add', {
        templateUrl: 'Admincontroller/loadView/editCategory/category'
    })
    .when('/admin/article/edit/:id', {
        templateUrl: 'Admincontroller/loadView/editArticle/article'
    })
    .when('/admin/product/edit/:id', {
        templateUrl: 'Admincontroller/loadView/editProduct/product'
    })
    .when('/admin/category/edit/:id', {
        templateUrl: 'Admincontroller/loadView/editCategory/category'
    })
    .when('/admin/productCategory/edit/:id', {
        templateUrl: 'Admincontroller/loadView/editCategory/category'
    })
    .when('/admin/bills', {
        templateUrl: 'Admincontroller/loadView/bills/bill'
    })
    .when('/admin/bills/add', {
        templateUrl: 'Admincontroller/loadView/billDetail/bill'
    }) 
    .when('/admin/billDetail/:id', {
        templateUrl: 'Admincontroller/loadView/billDetail/bill'
    }) 
    .when('/admin/plus-service', {
        templateUrl: 'Admincontroller/loadView/plusService'
    })
    .when('/admin/prices', {
        templateUrl: 'Admincontroller/loadView/postalCharges/price'
    }) 
    .when('/admin/packages', {
        templateUrl: 'Admincontroller/loadView/packages/package'
    }) 
    .when('/admin/tapping-to/packages', {
        templateUrl: 'Admincontroller/loadView/packageTappingTo/package',
        controller: 'packageTappingToCtrl'
    })
    .when('/admin/package/add', {
        templateUrl: 'Admincontroller/loadView/packageDetail/package'
    }) 
    .when('/admin/package/detail/:id', {
        templateUrl: 'Admincontroller/loadView/packageDetail/package'
    }) 
    .when('/admin/tapping-to/package/detail/:id', {
        templateUrl: 'Admincontroller/loadView/packageTappingToDetail/package',
        controller: 'packageTappingToDetailCtrl'
    }) 
    .when('/admin/shipment', {
        templateUrl: 'Admincontroller/loadView/shipments/shipment',
        controller: 'shipmentCtrl'
    }) 
    .when('/admin/tapping-to/shipment', {
        templateUrl: 'Admincontroller/loadView/shipmentTappingTo/shipment',
        controller: 'shipmentTappingToCtrl'
    }) 
    .when('/admin/shipment/add', {
        templateUrl: 'Admincontroller/loadView/shipmentDetail/shipment',
        controller: 'shipmentDetailCtrl'
    }) 
    .when('/admin/shipment/detail/:id', {
        templateUrl: 'Admincontroller/loadView/shipmentDetail/shipment',
        controller: 'shipmentDetailCtrl'
    }) 
    .when('/admin/tapping-to/shipment/detail/:id', {
        templateUrl: 'Admincontroller/loadView/shipmentDetailTappingTo/shipment',
        controller: 'shipmentTappingToDetailCtrl'
    }) 
    .when('/admin/assignments', {
        templateUrl: 'Admincontroller/loadView/assignments/assign'
    })
    .when('/admin/approve-assign', {
        templateUrl: 'Admincontroller/loadView/approve/assign'
    })
    .when('/admin/delivery-assign', {
        templateUrl: 'Admincontroller/loadView/delivery/assign'
    })
    .when('/admin/list-assign', {
        templateUrl: 'Admincontroller/loadView/list/assign'
    })
    .when('/admin/mat-bang', {
        templateUrl: 'Admincontroller/loadView/mat-bang/matbang'
    })
    .when('/admin/loai-mat-bang', {
        templateUrl: 'Admincontroller/loadView/loai-mat-bang/matbang'
    })
    .when('/admin/chi-tiet-mat-bang', {
        templateUrl: 'Admincontroller/loadView/chi-tiet-mat-bang/matbang'
    })
    .when('/admin/chi-tiet-mat-bang/add', {
        templateUrl: 'Admincontroller/loadView/edit-chi-tiet-mat-bang/matbang'
    })
    .when('/admin/chi-tiet-mat-bang/edit/:id', {
        templateUrl: 'Admincontroller/loadView/edit-chi-tiet-mat-bang/matbang'
    })
    .when('/admin/utilities-tq', {
        templateUrl: 'Admincontroller/loadView/utilities_tq/utilities'
    })
    .when('/admin/tien-ich', {
        templateUrl: 'Admincontroller/loadView/tienich-cateSlides/library'
    })
    .when('/admin/tien-ich/:cateId', {
        templateUrl: 'Admincontroller/loadView/tienich-slide/library'
    }) 
    .when('/admin/tien-do', {
        templateUrl: 'Admincontroller/loadView/td-cateSlides/library'
    })
    .when('/admin/tien-do/:cateId', {
        templateUrl: 'Admincontroller/loadView/td-slide/library'
    })
    .when('/admin/home', {
        templateUrl: 'Admincontroller/loadView/home'
    })
    .when('/admin/process-tq', {
        templateUrl: 'Admincontroller/loadView/processtq/process'
    })
    .when('/admin/contact-tq', {
        templateUrl: 'Admincontroller/loadView/contacttq/contact'
    })
    .when('/admin/add-script', {
        templateUrl: 'Admincontroller/loadView/script'
    })
    .when('/admin/file-du-an', {
        templateUrl: 'Admincontroller/loadView/file'
    })
    .when('/admin/bt-tien-ich', {
        templateUrl: 'Admincontroller/loadView/bt-tienich-cateSlides/library'
    })
    .when('/admin/bt-tien-ich/:cateId', {
        templateUrl: 'Admincontroller/loadView/bt-tienich-slide/library'
    }) 
    .when('/admin/bt-noi-that', {
        templateUrl: 'Admincontroller/loadView/bt-noithat-cateSlides/library'
    })
    .when('/admin/bt-noi-that/:cateId', {
        templateUrl: 'Admincontroller/loadView/bt-noithat-slide/library'
    }) 
    .when('/admin/ch-tien-ich', {
        templateUrl: 'Admincontroller/loadView/ch-tienich-cateSlides/library'
    })
    .when('/admin/ch-tien-ich/:cateId', {
        templateUrl: 'Admincontroller/loadView/ch-tienich-slide/library'
    }) 
    .when('/admin/ch-noi-that', {
        templateUrl: 'Admincontroller/loadView/ch-noithat-cateSlides/library'
    })
    .when('/admin/ch-noi-that/:cateId', {
        templateUrl: 'Admincontroller/loadView/ch-noithat-slide/library'
    }) 
    .when('/admin/ch-mat-bang', {
        templateUrl: 'Admincontroller/loadView/ch-matbang-cateSlides/library'
    })
    .when('/admin/ch-mat-bang/:cateId', {
        templateUrl: 'Admincontroller/loadView/ch-matbang-slide/library'
    })
    .otherwise({
        redirectTo: '/admin'
    });    
    $locationProvider.html5Mode(true);
}]);   