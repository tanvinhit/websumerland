angular.module('vpApp').factory('baseService',['$http', '$q', '$window', 'Flash', function($http, $q, $window, Flash){          
    return angular.extend($http, {
        ROOT_URL: 'http://summerland.swap.vn/',                
        WebsiteName: ' | B.A.T',
        currentLang: {},      
        ckeditorOptions: {
            language: 'vi',
            skin: 'office2013',
            allowedContent: true,
            entities: false,
            height: 500
        },
        ckeditorOptions2: {
            language: 'vi',
            skin: 'moono-lisa',
            allowedContent: true,
            entities: false,
            height: 150,
            toolbar: [
                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                {name: 'editing', items: ['Scayt']},
                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                {name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar']},
                {name: 'tools', items: ['Maximize']},
                {name: 'document', items: ['Source']},
                '/',
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat']},
                {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']},
                {name: 'styles', items: ['Styles', 'Format']},
                {name: 'about', items: ['About']},
            ]
        },
        showToast: function(msg, classType){
            var message = '<strong> Notice!</strong><br/> ' + msg;
            var id = Flash.create(classType, message, 5000, {class: 'custom-flash', id: 'custom-id'}, true);
        },
        showToast2: function(msg, classType, title){
            var message = '<strong> '+ title +'!</strong><br/> ' + msg;
            var id = Flash.create(classType, message, 5000, {class: 'custom-flash', id: 'custom-id'}, true);
        },
        getAlias: function(str){ 
            if(str !== undefined){
                str= str.toLowerCase();
                str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
                str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
                str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
                str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
                str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
                str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
                str= str.replace(/đ/g,"d");
                str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"-");
                str= str.replace(/-+-/g,"-"); /*thay thế 2- thành 1-*/
                str= str.replace(/^\-+|\-+$/g,"");/*cắt bỏ ký tự - ở đầu và cuối chuỗi*/
            }
            else{
                str = '';
            }
            return str;
        },
        TO_UNSIGN_NO_LOWERCASE: function (str) {
            if (str !== undefined) {
                str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
                str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
                str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
                str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
                str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
                str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
                str = str.replace(/đ/g, "d");
                str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
                str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
                str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
                str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
                str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
                str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
                str = str.replace(/Đ/g, "D");
                str = str.replace(/ +/g, "");
                str = str.trim();
            }
            else {
                str = '';
            }

            return str;
        },
		convertToUnsign: function(str){ 
            if(str !== undefined){
                str= str.toLowerCase();
                str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
                str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
                str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
                str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
                str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
                str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
                str= str.replace(/đ/g,"d");
                str= str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g,"");
                str= str.replace(/ +/g,"");
                str = str.trim();
            }
            else{
                str = '';
            }
            return str;
        },
        vnToString: function(str){
            if(str !== undefined){
                str= str.toLowerCase();
                str= str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
                str= str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
                str= str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
                str= str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
                str= str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
                str= str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
                str= str.replace(/đ/g,"d");                
            }
            else{
                str = '';
            }
            return str;
        },
        getDatetimeNow: function(){
            var currentdate = new Date(); 
            var datetime =  currentdate.getFullYear() + "-"
                            + (currentdate.getMonth()+1)  + "-" 
                            + currentdate.getDate() + " "  
                            + currentdate.getHours() + ":"  
                            + currentdate.getMinutes() + ":" 
                            + currentdate.getSeconds();
            
            return datetime;
        },
        getDateNow: function(){
            var currentdate = new Date(); 
            var datetime =  currentdate.getFullYear() + "-"
                            + (currentdate.getMonth()+1)  + "-" 
                            + currentdate.getDate();
            
            return datetime;
        },
        GET: function(controller){
            var result = $q.defer();            
            $http({
                method: 'POST',
                url: controller,
                dataType: 'application/json',                
                data: { '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>' }
            }).then(function(response){
                result.resolve(response.data);
            }, result.reject.bind(null));
            return result.promise;
        },
        POST: function(controller, param){
            var result = $q.defer();
            var arg = { 
                data: param,
                '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
            };
            $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';                        
            $http({
                method: 'POST',
                url: controller,
                dataType: 'application/json',
                data: $.param(arg)                
            }).then(function(response){
                result.resolve(response.data);
            }, result.reject.bind(null));
                        
            return result.promise;
        },
        POSTDATA: function(controller, param){
            var result = $q.defer();
            var arg = {
                data: param  
            };
            $http.defaults.headers.post['Content-Type'] = undefined;
            $http({
                method: 'POST',
                url: controller,
                data: arg
            }).then(function(response){
                result.resolve(response.data); 
            }, result.reject.bind(null));
            
            return result.promise;
        },
        getImageUrl: function(url){
            var newUrl = url.substr(url.indexOf('assets'));
            return newUrl;
        },
        GetNotAsync: function(controller){
            var result = $q.defer();
            $http({
                method: 'GET',
                url: controller,
                async: false
            }).then(function (response){
                result.resolve(response.data); 
            }, result.reject.bind(null));
            
            return result.promise;
        },
        getCoordinateOnGoogleMap: function(address){
            var result = $q.defer();
            $http.get('http://maps.google.com/maps/api/geocode/json?address=' + address + '&sensor=false').then(function(response){
                if(response.data.results.length > 0)
                    result.resolve(response.data.results[0].geometry.location);
                else
                    console.log('No result');
            }, result.reject.bind(null));
            
            return result.promise;
        },
        datetime: {
            getDate: function(datetime){
                var date = new Date(datetime);
                return date.getDate();
            },
            getMonth: function(datetime){
                var date = new Date(datetime);
                var month = date.getMonth() + 1;
                var result = '';
                switch(month){
                    case 1:
                        result = 'Jan';
                        break;
                    case 2:
                        result = 'Feb';
                        break;
                    case 3:
                        result = 'Mar';
                        break;
                    case 4:
                        result = 'Apr';
                        break;
                    case 5:
                        result = 'May';
                        break;
                    case 6:
                        result = 'June';
                        break;
                    case 7:
                        result = 'July';
                        break;
                    case 8:
                        result = 'Aug';
                        break;
                    case 9:
                        result = 'Sep';
                        break;
                    case 10:
                        result = 'Oct';
                        break;
                    case 11:
                        result = 'Nov';
                        break;
                    default:
                        result = 'Dec';
                        break;
                }
                return result;
            },
            getYear: function(datetime){
                var date = new Date(datetime);
                return date.getFullYear();
            }
        }
    });
}]);