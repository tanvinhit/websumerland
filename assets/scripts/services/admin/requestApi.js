angular.module('vpApp').factory('requestApiHelper', function(){
    return {        
        USER:{
            ALL: 'Usercontroller/loadUsers',
            LOGOUT: 'Usercontroller/actionLogout',
            CURRENT: 'Usercontroller/getCurrentUser',
            DESTROY_SESSION: 'Usercontroller/destroySession',
            UPDATE: 'Usercontroller/updateUser',
            DELETE: 'Usercontroller/deleteUser',
            ADD: 'Usercontroller/addUser',
            CHECK_MAIL: 'Usercontroller/checkEmail',
            CHECK_ACCOUNT: 'Usercontroller/checkAccount',
            GET_BY_PROVINCE: "Usercontroller/getByProvince",
        },
        MASTER_PLAN_TYPE: {
            ALL: 'Materplantypecontroller/getAll',
            UPDATE_DB: 'Materplantypecontroller/updateDB',
            DELETE_DB: 'Materplantypecontroller/deleteDB',
            ADD_NEW: 'Materplantypecontroller/insertDB',
        },
        MASTER_PLAN: {
            ALL: 'Materplancontroller/getAll',
            ALL_CLIENT: 'Materplancontroller/getClient',
            UPDATE_MASTER_PLAN: 'Materplancontroller/updateMasterPlan',
            DELETE_MASTER_PLAN: 'Materplancontroller/deleteMasterPlan',
            ADD_MASTER_PLAN: 'Materplancontroller/insertMasterPlan',
        },
        ABOUT: {
            ALL: 'Aboutcontroller/getAll',
            UPDATE_ABOUT: 'Aboutcontroller/updateAbout'
        },
        POSITION: {
            ALL: 'Positioncontroller/getAll',
            UPDATE_POSITION: 'Positioncontroller/updatePosition'
        },
        TAILIEU: {
            ALL: 'Tailieucontroller/getAll',
            UPDATE_TL: 'Tailieucontroller/updateTL'
        },
        CONTENT: {
            ALL: 'Contentcontroller/getAll',
            UPDATE_ABOUT: 'Contentcontroller/updateContent'
        },
        CONNECT: {
            ALL: 'Connectcontroller/getAll',
            UPDATE_CONNECT: 'Connectcontroller/updateConnect'
        },
        SCRIPT: {
            ALL: 'Scriptcontroller/getAll',
            UPDATE_SCRIPT: 'Scriptcontroller/updateScript'
        },
        FILE: {
            ALL: 'Filecontroller/getAll',
            UPDATE_FILE: 'Filecontroller/updateFile'
        },
        CONTACT_PAGE: {
            ALL: 'Contactcontroller/getAll',
            UPDATE_CONTACT: 'Contactcontroller/updateContact'
        },
        DETAIL_MASTER_PLAN: {
            ALL: 'Materplandetailcontroller/getAll',
            UPDATE_MASTER_DETAIL: 'Materplandetailcontroller/updateMasterPlanDetail',
            DELETE_MASTER_DETAIL: 'Materplandetailcontroller/deleteMasterPlanDetail',
            ADD_MASTER_DETAIL: 'Materplandetailcontroller/insertMasterPlanDetail',
            EDIT_MASTER_DETAIL: 'Materplandetailcontroller/editMasterPlanDetail',
        },
        POSTMAN: {
            ALL: 'Usercontroller/loadUsers',
            LOGOUT: 'Usercontroller/actionLogout',
            CURRENT: 'Usercontroller/getCurrentUser',
            DESTROY_SESSION: 'Usercontroller/destroySession',
            UPDATE: 'Usercontroller/updateUser',
            DELETE: 'Usercontroller/deleteUser',
            ADD: 'Usercontroller/addUser',
            SEARCH_PROVINCE: 'Usercontroller/searchProvince',
            CHECK_PROVINCE: 'Usercontroller/checkProvince',
            SEARCH_POSTMAN: 'Usercontroller/searchPostman',
        },            
        DASHBOARD: 'Dashboardcontroller/dashboardStatistic',
        LANGUAGE: {
            GET: 'Languagecontroller/getAllLanguages',
            UPDATE: 'Languagecontroller/updateLanguage',
            ADD: 'Languagecontroller/addLanguage',
            DELETE: 'Languagecontroller/deleteLanguage',
            CHECK: 'Languagecontroller/checkLanguage',
            DEL_MULTI: 'Languagecontroller/deleteMultiLang',
        },  
        TEMPLATE: {
            PAGES: 'Templatecontroller/getPages',
            GET: 'Templatecontroller/getAllTemplates',
            UPDATE: 'Templatecontroller/updateTemplate',
            ADD: 'Templatecontroller/addTemplate',
            DELETE: 'Templatecontroller/deleteTemplate',
            DEL_MULTI: 'Templatecontroller/deleteMultiTemp',
            SEO: 'Templatecontroller/getSeo',
        },       
        CATEGORY: {
            GET: 'Categorycontroller/loadCategories',            
            ALL: 'Categorycontroller/loadAllCategories',
            EDIT: 'Categorycontroller/editCategory',
            UPDATE: 'Categorycontroller/updateCategory',
            ADD: 'Categorycontroller/addCategory',
            DELETE: 'Categorycontroller/deleteCategory', 
            DEL_MULTI: 'Categorycontroller/deleteMultiCategory',
        },
        ARTICLE: {
            GET: 'Articlecontroller/loadArticles',
            GET_BY_META: 'Articlecontroller/getArtMeta',
            ALL: 'Articlecontroller/loadAllArticles',
            UPDATE: 'Articlecontroller/updateArticle',
            EDIT: 'Articlecontroller/editArticle',
            ADD: 'Articlecontroller/addArticle',
            DELETE: 'Articlecontroller/deleteArticle',
            DEL_MULTI: 'Articlecontroller/delMultiArticles', 
        }, 
        MAIL_CONTACT: {
            ADD: 'Mailcontactcontroller/addMail',
        }, 
        PRODUCT: {
            GET: 'Productcontroller/loadProducts',
            ALL: 'Productcontroller/loadAllProducts',
            UPDATE: 'Productcontroller/updateProduct',
            EDIT: 'Productcontroller/editProduct',
            ADD: 'Productcontroller/addProduct',
            DELETE: 'Productcontroller/deleteProduct',
            DEL_MULTI: 'Productcontroller/delMultiProducts',  
        },
        NAVIGATE: {
            ADD_LIST: 'Navigatecontroller/addNavigatesList',
            ADD_CUSTOM: 'Navigatecontroller/addCustomNavigate',
            UPDATE: 'Navigatecontroller/updateNavigate',
            DELETE: 'Navigatecontroller/deleteNavigate',
            GET: 'Navigatecontroller/getNavigates',
            CHANGE_POSITION: 'Navigatecontroller/changePosition',    
        },
        HEADER: {
            GET: 'Widgetcontroller/getHeader',
            UPDATE: 'Widgetcontroller/updateHeader',            
        },
        WIDGET: {
            UPDATE: 'Widgetcontroller/updateWidget',
            ADD: 'Widgetcontroller/addWidget',
            DELETE: 'Widgetcontroller/delWidget',
            ALL: 'Widgetcontroller/getAllWidgets',
            SORT: 'Widgetcontroller/sortableWidget',
        },
        MAP: {
            GET: 'Mapcontroller/getMapList',
            UPDATE: 'Mapcontroller/updateMarker',
            ADD: 'Mapcontroller/addNewMarker',
            DELETE: 'Mapcontroller/deleteMarker',
        },
        SLIDE: {
            GET: 'Slidecontroller/getSlides',
            GET_BY_CATEID: 'Slidecontroller/getByCateId',
            UPDATE: 'Slidecontroller/updateSlide',
            ADD: 'Slidecontroller/addSlide',
            DELETE: 'Slidecontroller/deleteSlide',
            DEL_MULTI: 'Slidecontroller/deleteMultiSlide',
        },     
        CUSTOMER: {
            GET: 'Customercontroller/getCustomers',
            GET_BY_ID: 'Customercontroller/getById',
            UPDATE: 'Customercontroller/updateCustomer',
            ADD: 'Customercontroller/addCustomer',
            DELETE: 'Customercontroller/deleteCustomer',
        },
        SERVICE: {
            GET: 'Servicecontroller/getServices',
            UPDATE: 'Servicecontroller/updateService',
            ADD: 'Servicecontroller/addService',
            DELETE: 'Servicecontroller/deleteService',
            GET_BY_ID: 'Servicecontroller/getById'
        },
        CONTACT: {
            GET: 'Mailcontactcontroller/getMailList',
            DELETE: 'Mailcontactcontroller/deleteMail',
            DEL_MULTI: 'Mailcontactcontroller/deleteMultiContact',
            SEND_MAIL: 'Mailcontactcontroller/sendMail'
        },
        RESOURCE: {
            GET_TYPE: 'Resourcecontroller/getResourceType',
            GET_CATE: 'Resourcecontroller/getResourceCate',
            GET: 'Resourcecontroller/getResources',
            ADD: 'Resourcecontroller/addResource',
            UPDATE: 'Resourcecontroller/updateResource',
            DELETE: 'Resourcecontroller/delResource',
            ADD_CATE: 'Resourcecontroller/addResourceCat',
            UPDATE_CATE: 'Resourcecontroller/updateResourceCat',
            DELETE_CATE: 'Resourcecontroller/delResourceCat',  
        },
        ORDER: {
            ALL: "Cartcontroller/getAll",
            UPDATE: "Cartcontroller/update",
            UPDATE_STATUS: "Cartcontroller/updateStatus",
            GET_BY_ID: "Cartcontroller/getById",
            DELETE: "Cartcontroller/delete",
            DEL_MULTI: "Cartcontroller/deleteMultiOrder"
        },
        BILL: {
            ALL: "Billcontroller/getAll",
            ADD: "Billcontroller/insert",
            CREATE_MANY: "Billcontroller/insertMany",
            UPDATE_STATUS: "Billcontroller/updateStatus",
            DELETE: "Billcontroller/delete",
            DEL_MULTI: "Billcontroller/deleteMulti",
            UPDATE: "Billcontroller/update",
            GET_BY_ID: "Billcontroller/getById",
            GET_BY_CODE: "Billcontroller/getByCode",
            GET_BY_PROVINCE: "Billcontroller/getByProvince",
        },
        ADDRESS: {
            GET_PROVINCES: 'Mapcontroller/getProvinces',
            GET_DISTRICTS_BY_PROVINCE_ID: 'Mapcontroller/getDistrictsByProvinceId',
            GET_DISTRICTS_BY_ID: 'Mapcontroller/getDistrictsById',
            GET_WARDS_BY_DISTRICT_ID: 'Mapcontroller/getWardsByDistrictId',
            GET_HUB_BY_ID: 'Mapcontroller/getHubById'
        },
        PLUS_SERVICE: {
            GET: 'Plusservicecontroller/getAll',
            INSERT: 'Plusservicecontroller/insert',
            UPDATE: 'Plusservicecontroller/update',
            DELETE: 'Plusservicecontroller/delete'
        },
        POSTAL_CHARGE: {
            GET: 'Pricecontroller/getAll',
            GET_BY_ID: 'Pricecontroller/getByCondition',
            INSERT: 'Pricecontroller/insert',
            UPDATE: 'Pricecontroller/update',
            DELETE: 'Pricecontroller/delete',
            DEL_MULTI: 'Pricecontroller/deleteMulti',
            CREATE_MANY: 'Pricecontroller/insertMany'
        },
        PACKAGE: {
            GET: 'Packagecontroller/getAll',
            GET_TAPPING_TO: 'Packagecontroller/getTappingTo',
            GET_BY_ID: 'Packagecontroller/getById',
            GET_BY_CODE: 'Packagecontroller/getByCode',
            GET_DETAIL_BY_ID: 'Packagecontroller/getDetailById',
            INSERT: 'Packagecontroller/insert',
            UPDATE: 'Packagecontroller/update',
            DELETE: 'Packagecontroller/delete',
            DEL_MULTI: 'Packagecontroller/deleteMulti',
            ADD_PACKAGE_DETAIL: 'Packagecontroller/insertDetail',
            DEL_PACKAGE_DETAIL: 'Packagecontroller/deleteDetail',
            DEL_MULTI_PACKAGE_DETAIL: 'Packagecontroller/deleteMultiDetail',
            TAPPING_TO_ALL_BILL: 'Packagecontroller/tappingToAllBill',
            TAPPING_TO_BILL: 'Packagecontroller/tappingToBill',
        },
        SHIPMENT: {
            GET: 'Shipmentcontroller/getAll',
            GET_TAPPING_TO: 'Shipmentcontroller/getTappingTo',
            GET_BY_ID: 'Shipmentcontroller/getById',
            GET_DETAIL_BY_ID: 'Shipmentcontroller/getDetailById',
            INSERT: 'Shipmentcontroller/insert',
            UPDATE: 'Shipmentcontroller/update',
            DELETE: 'Shipmentcontroller/delete',
            DEL_MULTI: 'Shipmentcontroller/deleteMulti',
            ADD_SHIPMENT_DETAIL: 'Shipmentcontroller/insertDetail',
            DEL_SHIPMENT_DETAIL: 'Shipmentcontroller/deleteDetail',
            DEL_MULTI_SHIPMENT_DETAIL: 'Shipmentcontroller/deleteMultiDetail',
            TAPPING_TO_ALL_PACKAGE: 'Shipmentcontroller/tappingToAllPackage',
            TAPPING_TO_PACKAGE: 'Shipmentcontroller/tappingToPackage',
        },
        ASSIGN: {
            ADD: 'Assignmentcontroller/addAssignment',
            DELETE: 'Assignmentcontroller/deleteAssign',
            UPDATE: 'Assignmentcontroller/updateAssignment',
            UPDATE_APPROVE: 'Assignmentcontroller/approveAssignment',
            UPDATE_DELIVERY: 'Assignmentcontroller/deliveryAssignment',
            GET_HISTORY: 'Assignmentcontroller/getById',
            GET_BY_ID: 'Assignmentcontroller/getByIdofAssign',
            GET_BY_USER_ID: 'Assignmentcontroller/getByUserId',
            SEND_APPROVE: 'Assignmentcontroller/sendApproveByUserId',
        },
        TQVT: {
            ADD: 'Tongquanvtcontroller/insertTQVT',
            DELETE: 'Tongquanvtcontroller/deleteTQVT',
            UPDATE: 'Tongquanvtcontroller/updateTQVT',
            GETALL: 'Tongquanvtcontroller/getAll',
        },
        CTVT: {
            ADD: 'Chitietvtcontroller/insertCTVT',
            DELETE: 'Chitietvtcontroller/deleteCTVT',
            UPDATE: 'Chitietvtcontroller/updateCTVT',
            GETALL: 'Chitietvtcontroller/getAll',
        },
        BENEFIT_TQ: {
            ALL: 'Benefittqcontroller/getAll',
            UPDATE_BENEFIT: 'Benefittqcontroller/updateBenefittq'
        },
        BENEFIT: {
            ADD: 'Benefitcontroller/insertBenefit',
            DELETE: 'Benefitcontroller/deleteBenefit',
            UPDATE: 'Benefitcontroller/updateBenefit',
            GETALL: 'Benefitcontroller/getAll',
        },
        PARTNER: {
            ADD: 'Partnercontroller/insertPartner',
            DELETE: 'Partnercontroller/deletePartner',
            UPDATE: 'Partnercontroller/updatePartner',
            GETALL: 'Partnercontroller/getAll',
        },
        HOME_PAGE: {
            ALL: 'Homepagecontroller/getAll',
            UPDATE_HOME_PAGE: 'Homepagecontroller/updateHomepage'
        },
        UTILITIES: {
            ALL: 'Utilitiescontroller/getAll',
            UPDATE_UTILITIES: 'Utilitiescontroller/updateUtilities'
        },
        ARTICLE_TQ: {
            ALL: 'Articletqcontroller/getAll',
            UPDATE_ARTICLE: 'Articletqcontroller/updateArticletq'
        },
        CONTACT_TQ: {
            ALL: 'Contacttqcontroller/getAll',
            UPDATE_CONTACT: 'Contacttqcontroller/updateContacttq'
        },
        PROCESS_TQ: {
            ALL: 'Processtqcontroller/getAll',
            UPDATE_PROCESS: 'Processtqcontroller/updateProcess'
        },
    };
});