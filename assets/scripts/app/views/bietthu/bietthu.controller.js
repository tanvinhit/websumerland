(function () {
    'use strict';
    angular
      .module('vpApp')
      .controller('BietthuController', BietthuController);
  
    BietthuController.$inject = ['fullPageService', "ngMeta", "requestApiHelper", "$scope", "$rootScope", "baseService", "$timeout"];
  
    function BietthuController(fullPageService, ngMeta, requestApiHelper, $scope, $rootScope, baseService, $timeout) {
      var options = {
        anchors: ['tinh-hoa-cua-bien-ca', 'mat-bang-tong-the', 'dai-cong-vien-nuoc', 'cac-tien-ich-noi-troi', 'hinh-anh-noi-that'],
        menu: '#bietthu-nav',
        lockAnchors: false,
        css3: false,
        responsiveWidth: 1100,
        verticalCentered: false,
        navigation: false,
        navigationPosition: 'right',
        resetSliders:true,
        responsive: 1025,
        afterResponsive: function (isResponsive) {
          if (isResponsive) {
            $('html, body').css('overflow-x', 'hidden')
          }
        },
        afterLoad: function (origin, destination, direction) {
          
        },
        onLeave: function (index, nextIndex, direction) {
          $('.main-nav ').removeClass('active-nav')
          $('.body-wrapper').removeClass('active-body-wrapper')
          $('.wrapper-detail').removeClass('active-wrapper-detail')
          $('.menu').removeClass('show-menu')
          $('.button-nav').removeClass('active-button')
        },
      };

      var rebuild = function() {
        destroyFullPage();
  
        $('#fullpage2').fullpage(options);
      };
  
      var destroyFullPage = function() {
        if ($.fn.fullpage.destroy) {
          $.fn.fullpage.destroy('all');
        }
      };
      rebuild();

      $scope.dataLoaded_4 = false;
      $scope.dataLoaded_5 = false;

      $scope.section = {
        section4_SLIDE: [],
        section4_CATESLIDE: [],
        section5_CATESLIDE: [],
        section5_SLIDE: [],
      };

      

      function getSeo(){
        var controllerSeo = requestApiHelper.TEMPLATE.SEO;
          var data = {
              meta: 'biet-thu',
          };
          baseService.POST(controllerSeo, data).then(function (response) {
  
              ngMeta.setTitle(response.SeoTitle);
              ngMeta.setTag('description', response.SeoDescribes);
              ngMeta.setTag('keywords',  response.SeoKeyword);
              ngMeta.setTag('og:image',  baseService.ROOT_URL.concat(response.Image));
              ngMeta.setTag('og:locale',  'vi');
              ngMeta.setTag('author', 'summerland');
          }, function (err) {
              console.log(err);
          });
        
      };


      var cSection4_2 = requestApiHelper.CATEGORY.GET + '/' + 'vi';
      var data_Section4_2 = {
          type: 'admin',
          dataType: 9
      };
      baseService.POST(cSection4_2, data_Section4_2).then(function (res4) {
        $scope.section.section4_CATESLIDE = res4;
        var param_a = {
          type: 'admin'
        };
        var tabsRequest = [];
        for (let i = 0; i < $scope.section.section4_CATESLIDE.length; i++) {
          var controller_a = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + $scope.section.section4_CATESLIDE[i].CatID;
          tabsRequest.push(baseService.POST(controller_a, param_a));
        }
    
        Promise.all(tabsRequest).then(values => {
          values.forEach((res_a, i) => {
            var oList = {
              tab: "tab" + $scope.section.section4_CATESLIDE[i].CatID,
              list: res_a
            }
            $scope.section.section4_SLIDE.push(oList);
          })
    
          $timeout(function () {
            $scope.item_4 = $scope.section.section4_SLIDE;
            loadData_4($scope.item_4);
          }, 0);
        }, function (err) {
          console.log(err);
        })
        console.log($scope.section);
      }, function (err) {
        console.log(err);
      });

      function loadData_4(x){
        $timeout(function(){
          $scope.itemAs_4 = x;
          $scope.dataLoaded_4 = true;
        }, 0);
      };

      var cSection5_2 = requestApiHelper.CATEGORY.GET + '/' + 'vi';
      var data_Section5_2 = {
          type: 'admin',
          dataType: 10
      };
      baseService.POST(cSection5_2, data_Section5_2).then(function (res4) {
        $scope.section.section5_CATESLIDE = res4;
        var param_a = {
          type: 'admin'
        };
        var tabsRequest = [];
        for (let i = 0; i < $scope.section.section5_CATESLIDE.length; i++) {
          var controller_a = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + $scope.section.section5_CATESLIDE[i].CatID;
          tabsRequest.push(baseService.POST(controller_a, param_a));
        }
    
        Promise.all(tabsRequest).then(values => {
          values.forEach((res_a, i) => {
            var oList = {
              tab: "tab" + $scope.section.section5_CATESLIDE[i].CatID,
              list: res_a
            }
            $scope.section.section5_SLIDE.push(oList);
          })
    
          $timeout(function () {
            $scope.item_5 = $scope.section.section5_SLIDE;
            loadData_5($scope.item_5);
          }, 0);
        }, function (err) {
          console.log(err);
        })
      }, function (err) {
        console.log(err);
      });

      function loadData_5(x){
        $timeout(function(){
          $scope.itemAs_5 = x;
          console.log($scope.itemAs_5);
          $scope.dataLoaded_5 = true;
        }, 0);
      };
  
      getSeo();

    }
  
  })();