(function () {
  'use strict';
  angular
    .module('vpApp')
    .controller('TintucController', TintucController);

  TintucController.$inject = ["$scope",
    "$rootScope",
    "$window",
    "$sce",
    "baseService",
    "requestApiHelper",
    "$location",
    "$timeout",
    "ngMeta"
  ];

  function TintucController($scope, $rootScope, $window, $sce, baseService, requestApiHelper, $location, $timeout, ngMeta) {
    var options = {
      anchors: ['tin-tuc'],
      lockAnchors: false,
      css3: false,
      responsiveWidth: 1100,
      verticalCentered: false,
      navigation: false,
      navigationPosition: 'right',
      resetSliders: true,
      afterResponsive: function (isResponsive) {
        if (isResponsive) {
          $('html, body').css('overflow-x', 'hidden')
        }
      },
      afterLoad: function (origin, destination, direction) {

      },
      onLeave: function (index, nextIndex, direction) {
        $('.main-nav ').removeClass('active-nav')
        $('.body-wrapper').removeClass('active-body-wrapper')
        $('.wrapper-detail').removeClass('active-wrapper-detail')
        $('.menu').removeClass('show-menu')
        $('.button-nav').removeClass('active-button')
      },
    };

    var rebuild = function () {
      destroyFullPage();

      $('#tintucfullpage').fullpage(options);
    };

    var destroyFullPage = function () {
      if ($.fn.fullpage.destroy) {
        $.fn.fullpage.destroy('all');
      }
    };
    rebuild();

    $scope.dataLoadedNews = false;
    $scope.initNews = [],
    $scope.initNews_mobile = [];
   
    var cSection5 = requestApiHelper.ARTICLE.GET + '/' + 'vi';
    var paramSection5 = {
      type: 'admin'
    };
    baseService.POST(cSection5, paramSection5).then(function (res5) {
      $scope.articles = res5;
      var arrData = [];
   
      for (let i = 0; i < $scope.articles.length; i++) {
        const createDate = new Date($scope.articles[i].DateCreated);
        const item = Object.assign({},$scope.articles[i]);
        item.date_month_html = item.DayBinding + '<span ng-bind="">' + item.MonthYearBinding + '</span>'
        arrData.push(item);
      }

      $timeout(function(){
        $scope.initNews = arrData;
        $scope.initNews_mobile = arrData;
        $scope.dataLoadedNews = true;
      }, 500);
    }, function (error) {
      console.log(error.message);
    });

    $scope.mydetail = function(item) {
      $location.path('tin-tuc/' + item.ArtMeta);  
    };

    function getSeo(){
      var controllerSeo = requestApiHelper.TEMPLATE.SEO;
        var data = {
            meta: 'tin-tuc',
        };
        baseService.POST(controllerSeo, data).then(function (response) {

            ngMeta.setTitle(response.SeoTitle);
            ngMeta.setTag('description', response.SeoDescribes);
            ngMeta.setTag('keywords',  response.SeoKeyword);
            ngMeta.setTag('og:image',  baseService.ROOT_URL.concat(response.Image));
            ngMeta.setTag('og:locale',  'vi');
            ngMeta.setTag('author', 'summerland');
        }, function (err) {
            console.log(err);
        });
      
    };

    getSeo();

  }

})();