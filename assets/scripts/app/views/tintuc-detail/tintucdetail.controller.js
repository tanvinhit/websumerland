(function () {
    'use strict';
    angular
        .module('vpApp')
        .controller('TintucDetailController', TintucDetailController);

    TintucDetailController.$inject = ["$scope",
        "$rootScope",
        "$window",
        "$sce",
        "baseService",
        "requestApiHelper",
        "$location", 
        "$timeout", 
        "ngMeta"
    ];

    function TintucDetailController($scope, $rootScope, $window, $sce, baseService, requestApiHelper, $location, $timeout, ngMeta) {
        var options = {
            anchors: ['tin-tuc'],
            lockAnchors: false,
            css3: false,
            responsiveWidth: 1100,
            verticalCentered: false,
            navigation: false,
            navigationPosition: 'right',
            resetSliders: true,
            afterResponsive: function (isResponsive) {
              if (isResponsive) {
                $('html, body').css('overflow-x', 'hidden')
              }
            },
            afterLoad: function (origin, destination, direction) {
      
            },
            onLeave: function (index, nextIndex, direction) {
              $('.main-nav ').removeClass('active-nav')
              $('.body-wrapper').removeClass('active-body-wrapper')
              $('.wrapper-detail').removeClass('active-wrapper-detail')
              $('.menu').removeClass('show-menu')
              $('.button-nav').removeClass('active-button')
            },
          };
      
          var rebuild = function () {
            destroyFullPage();
      
            $('#tintucDetailfullpage').fullpage(options);
          };
      
          var destroyFullPage = function () {
            if ($.fn.fullpage.destroy) {
              $.fn.fullpage.destroy('all');
            }
          };
          rebuild();

        gsap.from('.detail-logo', 1.5, {
            opacity: 0,
            ease: "expo.out",
            delay: 0.5,
            x: -500,
            scale: 0
        })
        gsap.from('.text-slogan-page', 1.5, {
            opacity: 0,
            delay: 0.5,
            ease: "expo.out",
            x: -100,
        })
        gsap.from(".content-popup", 1.5, {
            opacity: 0,
            ease: "expo.out",
            delay: 0.5,
            y: -200,
        }, 1)
        gsap.from(".list-register-news", 1.5, {
            opacity: 0,
            ease: "expo.out",
            delay: 0.7,
            y: 200,
        }, 1)
        gsap.from(".close-detail", 1.5, {
            opacity: 0,
            ease: "expo.out",
            delay: 0.4,
            x: 200,
            rotation: 180,
        }, 1)

        $scope.dataLoaded = false;

        $scope.oNew = {};

        function init() {
            checkConditionToOpenNews();
        }

        function checkConditionToOpenNews() {
            let pathname = location.pathname;

            var re = new RegExp('(\/(tin-tuc)\/[^/$]+)$');
            if (re.test(pathname)) {
                const artMeta = pathname.substring(pathname.lastIndexOf('/') + 1);
                reloadNew(artMeta);
            }
        }

        function reloadNew(artMeta) {
            var controller = requestApiHelper.ARTICLE.GET_BY_META;
            var param = {
                meta: artMeta
            };
            baseService.POST(controller, param).then(
                function (response) {
                    response.link = baseService.ROOT_URL + "tin-tuc/" + response.ArtMeta;
                    $timeout(function () {
                        loadData(response);
                    }, 100);
                },
                function (err) {
                    console.log(err);
                }
            );
        }

        init();

        $scope.shareFB = function (post) {
            FB.ui({
                method: 'feed',
                name: post.ArtName,
                link: baseService.ROOT_URL + "tin-tuc/" + post.ArtMeta,
                picture: baseService.ROOT_URL + post.Image,
                caption: post.ArtDescribes,
                description: post.ArtDescribes,
                message: post.ArtDescribes
            });
        }
          

        function loadData(x) {
            $timeout(function () {
                $scope.oNew = x;
                $scope.dataLoaded = true;
                ngMeta.setTitle(x.SeoTitle);
                ngMeta.setTag('description', x.SeoKeyword);
                ngMeta.setTag('keywords',  x.SeoKeyword);
                ngMeta.setTag('og:image',  baseService.ROOT_URL.concat(x.Image));
                ngMeta.setTag('og:locale',  'vi');
                ngMeta.setTag('author', 'summerland');
            }, 0);
        };

        $scope.trustedHtml = function (plainText) {
            return $sce.trustAsHtml(plainText);
        }

        $scope.copyLink = function (post) {
            var text_to_share = baseService.ROOT_URL + "tin-tuc/" + post.ArtMeta;

            // create temp element
            var copyElement = document.createElement("span");
            copyElement.appendChild(document.createTextNode(text_to_share));
            copyElement.id = 'tempCopyToClipboard';
            angular.element(document.body.append(copyElement));

            // select the text
            var range = document.createRange();
            range.selectNode(copyElement);
            window.getSelection().removeAllRanges();
            window.getSelection().addRange(range);

            // copy & cleanup
            document.execCommand('copy');
            window.getSelection().removeAllRanges();
            copyElement.remove();
        }

        $(window).on('resize', function () {
            setNavRight();
        });
        setNavRight();
        function setNavRight() {
            var $width = $(window).innerWidth();
            if($width > 1024) {
                $('.nav-right').hide();
            } else {
                $('.nav-right').show();
            }
        }
        
                
    }
})();