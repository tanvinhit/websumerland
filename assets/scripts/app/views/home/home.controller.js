angular
  .module('vpApp')
  .controller('HomeController', [
    "$scope",
    "$rootScope",
    "baseService",
    "requestApiHelper",
    "$timeout",
    "$http",
    '$location',
    "ngMeta",
    '$sce',
  function ($scope, $rootScope, baseService, requestApiHelper,$timeout,$http,$location, ngMeta, $sce) {
    $('.nav-right').css('display' , 'flex')
    $scope.Enable_CHECK = {
      enable_li: true,
      enable_ti: true,
      check_ti: true
    };
    var options = {
      anchors: ['home', 'gioithieu', 'vitri', 'loaihinhsanpham', 'tienich' , 'loiichdautu', 'tintucduan',  'tiendoduan', 'lienhe'],
      menu: '#home-nav',
      lockAnchors: false,
        css3: false,
        responsiveWidth: 1100,
        verticalCentered: false,
        navigation: false,
        navigationPosition: 'right',
        responsive: 1025,
      afterResponsive: function (isResponsive) {
        if (isResponsive) {
          $('html, body').css('overflow-x', 'hidden')
        }
      },
      afterLoad: function (origin, destination, direction) {

      },
      onLeave: function (anchorCurrent, anchorNext, direction) {
        $('.main-nav ').removeClass('active-nav')
        $('.body-wrapper').removeClass('active-body-wrapper')
        $('.wrapper-detail').removeClass('active-wrapper-detail')
        $('.menu').removeClass('show-menu')
        $('.button-nav').removeClass('active-button')

        if (anchorNext.anchor === "loiichdautu") {
          gsap.from('.item-circle', 1, {
            opacity: 0,
            x: -200
          }, 0.05)
          gsap.from('.item-loiich-center .title-loiich-mobile', 1, {
            opacity: 0,
            x: -200
          }, 0.05)
          gsap.from('.item-loiich-center .item-loiich-road', 1, {
            opacity: 0,
            x: -200
          }, 0.05)
          gsap.from('.item-loiich-center .item-content-loi-ich', 1, {
            opacity: 0,
            x: -200
          }, 0.05)
        }

        if (anchorNext.anchor === "loiichdautu"){
          $scope.Enable_CHECK.enable_li = true;
        }else{
          $scope.Enable_CHECK.enable_li = false;
        }
        if (anchorNext.anchor === "tienich") {
          $scope.Enable_CHECK.enable_ti = true;
          $scope.Enable_CHECK.check_ti = true;
          var arrLi =[];
         
          for (let i = 0; i < $('#tabs-nav li').find('a').length; i++) {
            arrLi.push($('#tabs-nav li').find('a')[i].getAttribute('href'));
          }

          function longForLoop(limit) {
            var i = 0;
            var ref = setInterval(() => {
              if($scope.Enable_CHECK.enable_ti == true){
                $('#tabs-nav li').removeClass('active');
                $('.tab-content').hide();
                $(arrLi[++i]).fadeIn(1100);
                $('#tabs-nav li[data-name="'+ i +'"]').addClass('active')
                $('.slide-tienich,.thumb-tienich').slick("setPosition", 0);
                if (i == limit) {
                  $('#tabs-nav li').removeClass('active');
                  $('.tab-content').hide();
                  $(arrLi[0]).fadeIn(1100);
                  $('#tabs-nav li[data-name="'+ 0 +'"]').addClass('active')
                  $('.slide-tienich,.thumb-tienich').slick("setPosition", 0);
                  clearInterval(ref);
                } 
              }else{
                if($scope.Enable_CHECK.check_ti == false){
                  $('#tabs-nav li').removeClass('active');
                  $('.tab-content').hide();
                  $(arrLi[0]).fadeIn(1100);
                  $('#tabs-nav li[data-name="'+ 0 +'"]').addClass('active')
                  $('.slide-tienich,.thumb-tienich').slick("setPosition", 0);
                }
                clearInterval(ref);
              }
            }, 3000);
          }
          longForLoop(arrLi.length);
        }else{
          $scope.Enable_CHECK.enable_ti = false;
           $scope.Enable_CHECK.check_ti = false;
        }
        if(anchorCurrent && anchorCurrent.item && anchorCurrent.item.clientWidth < 1025) {
          if($scope.Enable_CHECK.enable_li == true){
            var deviceWidth = $(window).width();
            if(deviceWidth <= 480 && anchorNext.anchor === "loiichdautu") {

              if($scope.Enable_CHECK.enable_li == true) setInterval(swapFunc, 3000);

                function swapFunc() {
                  if($scope.Enable_CHECK.enable_li == true){
                    for( let i = 0; i < $scope.tempsdata.length; i++){
                      $timeout(function(){
                        if($scope.Enable_CHECK.enable_li == true){
                          $scope.tempsdata.swap( i, 2);
                        } 
                      }, 5000);
                    }
                  } 
                }

                Array.prototype.swap = function (x,y) {
                  var b = this[x];
                  this[x] = this[y];
                  this[y] = b;
                  gsap.from('.loiich-wrapper', 1, {
                    opacity: 0,
                    x: -200
                  }, 0.05)
                  gsap.from('.item-loiich-center', 1, {
                    opacity: 0,
                    x: -200
                  }, 0.05)
                  return this;
                }
            }
          }
          return;
        }
        if (anchorNext.anchor === "gioithieu") {
          gsap.from('.tropical-paradise .wrapper-tropical-paradise .left-tropical-paradise', 1, {
            opacity: 0,
            x: -200,
            ease: "expo.out",
          }, 1)
          gsap.from(".tropical-paradise .wrapper-tropical-paradise .right-tropical-paradise", 2, {
            opacity: 0,
            y: 400,
            ease: "expo.out",
          }, 1)
        }
        if (anchorNext.anchor === "vitri") {
          gsap.from('.home-vitri .map-customize', 1, {
            opacity: 0,
            x: -200,
          }, 1)
          gsap.from(".home-vitri .content", 2, {
            opacity: 0,
            y: 200,
          }, 0.05)
        }
        if (anchorNext.anchor === "loaihinhsanpham") {
          gsap.from('.overall-wrapper .box-image #box-item-first', 1, {
            opacity: 0,
            x: -200,
          }, 1)
          gsap.from('.overall-wrapper .title-overall', 1, {
            opacity: 0,
            y: -200,
          }, 1)
          gsap.from(".overall-wrapper .box-image #box-item-last", 1, {
            opacity: 0,
            x: 200,
          }, 1)
        }
        if (anchorNext.anchor === "tienich") {
          gsap.from('.home-tienich #tabs-nav', 1.5, {
            opacity: 0,
            x: -200
          }, 1)
          gsap.from('.home-tienich .heading-tabs', 1, {
            opacity: 0,
          }, 1)
          gsap.from('.home-tienich #tabs-content', 1, {
            opacity: 0,
            x: 200,
            delay: .5
          }, 1)
        }
        if (anchorNext.anchor === "tintucduan") {
          gsap.from('.grid-first', 1, {
            opacity: 0,
            x: -400,
          }, 1)
          gsap.from(".grid-second", 2, {
            opacity: 0,
            ease: "expo.out",
            x: 800,
          }, 0.05)
        }
        if (anchorNext.anchor === "loiichdautu") {
          gsap.from(".group-choose .items", 1.2, {
            opacity: 0,
            x: -30,
            ease: "expo.out",
            stagger: {
              amount: 1
            }
          }, 0.08)
          gsap.from('.heading-connect', 1, {
            opacity: 0,
            x: -100
          }, 0.05)
        }
        if (anchorNext.anchor === "tiendoduan") {
          gsap.from(".home-project-outline .title-project-outline", 1, {
            opacity: 0,
            ease: "expo.out",
            delay: 0.5,
            x: -200,
          }, 1)
          gsap.from(".home-project-outline .subtitle-project-outline", 2, {
            opacity: 0,
            ease: "expo.out",
            x: 600,
          }, 1)
          gsap.from('.thumb-project-outline', 1, {
            opacity: 0,
            y: 200,
          }, 0.08)

          
        }
        if (anchorNext.anchor === "lienhe" && direction == "down") {
          gsap.from(".home-footer .footer-wrapper .footer-right", 1, {
            opacity: 0,
            ease: "expo.out",
            delay: 0.5,
            rotation: 180,
            x: -200,
          }, 1)
          gsap.from(".home-footer .footer-wrapper .footer-left .logo-footer", 1, {
            opacity: 0,
            ease: "expo.out",
            delay: 0.5,
            y: -200,
          }, 1)
          gsap.from(".home-footer .footer-wrapper .footer-left .content-left", 1, {
            opacity: 0,
            ease: "expo.out",
            delay: 0.5,
            x: -200,
          }, 1)
          gsap.from(".home-footer .footer-wrapper .footer-left .content-right", 1, {
            opacity: 0,
            ease: "expo.out",
            delay: 0.5,
            x: 600,
          }, 1)
          gsap.from(".home-footer .home-footer .info-footer", 1, {
            opacity: 0,
            ease: "expo.out",
            delay: 0.5,
            y: 1000,
          }, 1)
        }
      },
    };

    var rebuild = function () {
      destroyFullPage();

      $('#fullpage1').fullpage(options);
    };

    var destroyFullPage = function () {
      if ($.fn.fullpage.destroy) {
        $.fn.fullpage.destroy('all');
      }
    };
    rebuild();

    

    $scope.mailContactInfo = {
      Name: "",
      Email: "",
      Phone: "",
      Subject: "",
      Content: "",
      DateSent: getDate(new Date()),
      DeviceType: getDeviceType(),
      IP: "",
    };

    $scope.section = {
      section1: {},
      section2: {},
      section3: {},
      section3_TQ: [],
      section3_DT: [],
      section4: {},
      section4_html: '',
      section4_CATESLIDE: [],
      section4_SLIDE: [],
      section4_SLIDE_A: [],
      section6: [],
      section6_mobile: [],
      section8_CATESLIDE: [],
      section8_SLIDE: [],
      checkBinding: false,
      section7: {},
      section7_list: [],
      section9_list: [],
      section6_tq: {},
      section8: {},
      sectionFooter: {}
    };
    $scope.dataLoaded = false;
    $scope.dataLoadedNews = false;
    $scope.dataLoadedSlide = false;
    $scope.dataLoadedSlideLI = false;

    function init(){
      var cSection1 = requestApiHelper.HOME_PAGE.ALL;
      baseService.GET(cSection1).then(function(res1){
        $scope.section.section1 = res1[0];
      }, function(err){
          console.log(err);
      });

      var cSection2 = requestApiHelper.ABOUT.ALL;
      baseService.GET(cSection2).then(function(res2){
        $scope.section.section2 = res2[0]; 
      }, function(err){
        console.log(err);
      });

      var cSection3_1 = requestApiHelper.POSITION.ALL;
      baseService.GET(cSection3_1).then(function(res3_1){
        $scope.section.section3 = res3_1[0];
      }, function(err){
        console.log(err);
      });

      var cSection3_2 = requestApiHelper.TQVT.GETALL;
        baseService.GET(cSection3_2).then(function(res3_2){
          $scope.section.section3_TQ = res3_2;
        }, function(err){
          console.log(err);
      });

      var cSection3_3 = requestApiHelper.CTVT.GETALL;
        baseService.GET(cSection3_3).then(function(res3_3){
          $scope.section.section3_DT = res3_3;
        }, function(err){
          console.log(err);
      });

      var cSection4_2 = requestApiHelper.CATEGORY.GET + '/' + 'vi';
      var data_Section4_2 = {
          type: 'admin',
          dataType: 4
      };
      baseService.POST(cSection4_2, data_Section4_2).then(function (res4) {
        $scope.section.section4_CATESLIDE = res4;
        var param_a = {
          type: 'admin'
        };
        var tabsRequest = [];
        for (let i = 0; i < $scope.section.section4_CATESLIDE.length; i++) {
          var controller_a = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + $scope.section.section4_CATESLIDE[i].CatID;
          tabsRequest.push(baseService.POST(controller_a, param_a));
        }
    
        Promise.all(tabsRequest).then(values => {
          values.forEach((res_a, i) => {
            var oList = {
              tab: "tab" + $scope.section.section4_CATESLIDE[i].CatID,
              list: res_a
            }
            $scope.section.section4_SLIDE.push(oList);
          })
    
          $timeout(function () {
            $scope.itemAB = $scope.section.section4_SLIDE;
            loadData($scope.itemAB);
          }, 0);
        }, function (err) {
          console.log(err);
        })
        console.log($scope.section);
      }, function (err) {
        console.log(err);
      });

      var cSection5 = requestApiHelper.ARTICLE.GET + '/' + 'vi';
        var paramSection5 = {type: 'admin'};
        baseService.POST(cSection5,paramSection5).then(function(res5){                           
            $scope.articles = res5;    
            var arrData = [];
     
            for (let i = 0; i < $scope.articles.length; i++) {
              const item = Object.assign({},$scope.articles[i]);
              item.date_month_html = item.DayBinding + '<span ng-bind="">' + item.MonthYearBinding + '</span>'
              arrData.push(item);
            }
            $timeout(function(){
              $scope.section.section6 = arrData;
              $scope.section.section6_mobile = arrData;
              $scope.dataLoadedNews = true;
            }, 500);
           
        }, function(error){
            console.log(error.message);
        });
        var cSection8_2 = requestApiHelper.CATEGORY.GET + '/' + 'vi';
        var data_Section8_2 = {
            type: 'admin',
            dataType: 5
        };
        baseService.POST(cSection8_2, data_Section8_2).then(function (res8_2) {
            $scope.data_section_8 = res8_2;
            $scope.data_section_8_list = [];
            for (let i = 0; i < $scope.data_section_8.length; i++) {
              var controller_b = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + $scope.data_section_8[i].CatID;
              var param_b = {type: 'admin'};
              baseService.POST(controller_b, param_b).then(function(res_b){
                  var oList = {
                    date: $scope.data_section_8[i].CatName,
                    tab_date: $scope.data_section_8[i].CatName.split('/'),
                    date_sort: new Date(01, $scope.data_section_8[i].CatName.split('/')[1] - 1, $scope.data_section_8[i].CatName.split('/')[0]),
                    images_urls: res_b
                  }
                  $scope.data_section_8_list.push(oList)
                  if((i + 1) === $scope.data_section_8.length){
                    $timeout(function(){
                      $scope.data_section_8_list.sort(function(a, b) {
                        var c = new Date(a.date_sort);
                        var d = new Date(b.date_sort);
                        return c-d;
                      });
                      loadDataSlide($scope.data_section_8_list || []);
                    }, 500);
                  }
              }, function(err){
                  console.log(err);
              });
            }
        }, function (err) {
            console.log(err);
        });
        var cSection7_1 = requestApiHelper.BENEFIT_TQ.ALL;
        baseService.GET(cSection7_1).then(function(res7_1){
            $scope.section.section7 = res7_1[0];
        }, function(err){
            console.log(err);
        });
        var cSection7_2 = requestApiHelper.BENEFIT.GETALL;
        baseService.GET(cSection7_2).then(function(res7_2){
          $timeout(function(){
            loadDataSlideLI(res7_2);
          }, 500);
        }, function(err){
            console.log(err);
        });

        var cSection9_2 = requestApiHelper.PARTNER.GETALL;
        baseService.GET(cSection9_2).then(function(res9_2){
            $scope.section.section9_list = res9_2; 
        }, function(err){
            console.log(err);
        });

        var cSection4_1 = requestApiHelper.UTILITIES.ALL;
        baseService.GET(cSection4_1).then(function(res4_1){
            $scope.section.section4 = res4_1[0]; 
        }, function(err){
            console.log(err);
        });

        var cSection6_1 = requestApiHelper.ARTICLE_TQ.ALL;
        baseService.GET(cSection6_1).then(function(res6_1){
            $scope.section.section6_tq = res6_1[0]; 
        }, function(err){
            console.log(err);
        });

        var cSection8_1 = requestApiHelper.PROCESS_TQ.ALL;
        baseService.GET(cSection8_1).then(function(res8_1){
            $scope.section.section8 = res8_1[0]; 
        }, function(err){
            console.log(err);
        });

        var cSectionFooter = requestApiHelper.CONTACT_TQ.ALL;
        baseService.GET(cSectionFooter).then(function(res_footer){
          $scope.section.sectionFooter = res_footer[0]; 
        }, function(err){
            console.log(err);
        });

    };
    init();  
    
   function loadData(x){
      $timeout(function(){
        $scope.itemAs = x;
        $scope.dataLoaded = true;
      }, 0);
    };

    function chunck(array, size) {
      var results = [];
      for (var i=0,len=array.length; i<len; i+=size){
        if(array.slice(i,i+size).length < size){
          results.push(array.slice(i - (size-array.slice(i,i+size).length),i+size));
        }else{
          results.push(array.slice(i,i+size));
        }
      }
      return results;
    };

    function loadDataSlide(x){
      $timeout(function(){
        let slides = [];
        x.forEach((item,index) => {
          if(item.images_urls.length > 8) {
            const datapages = chunck(item.images_urls,8);
            datapages.forEach(page => {
              slides.push({...item, images_urls: page});
            });
          } else {
            slides.push(item)
          }
        });

        $scope.section.section8_SLIDE = slides;
        $scope.dataLoadedSlide = true;
        setTimeout(() => {
          $('.thumb-project-outline').slick('slickGoTo', (slides.length - 1));
        },300);
      }, 0);
    };

    function loadDataSlideLI(x){
      $timeout(function(){
        $scope.section.section7_list = x;
        $scope.dataLoadedSlideLI = true;
      }, 0);
    };

    function getIP() {
      $http
        .get("https://api.ipify.org/?format=json")
        .success(function (data) {
          if (data) {
            $scope.mailContactInfo.IP = data.ip || "";
          }
        })
        .error(function (data, status) {
          console.log(data);
        });
    }

    function getDate(date) {
      var dateStr =
        date.getFullYear() +
        "/" +
        ("00" + (date.getMonth() + 1)).slice(-2) +
        "/" +
        ("00" + date.getDate()).slice(-2) +
        " " +
        ("00" + date.getHours()).slice(-2) +
        ":" +
        ("00" + date.getMinutes()).slice(-2) +
        ":" +
        ("00" + date.getSeconds()).slice(-2);
      return dateStr;
    }

    function getDeviceType() {
      const ua = navigator.userAgent;
      if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
        return "tablet";
      }
      if (
        /Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
          ua
        )
      ) {
        return "mobile";
      }
      return "desktop";
    }

    $scope.sendCustomerInfo = function addCustomer() {
      var btnSubmit = document.getElementById("btn-register-submit"); 
      btnSubmit.disabled = true;
      $scope.messageName = "";
      $scope.messageEmail = "";
      $scope.messagePhone = "";

      if (!$scope.mailContactInfo.Name) {
        $scope.checkName = true;
        $scope.messageName = "Vui lòng nhập tên";
      }else{
        if(stringContainsNumber($scope.mailContactInfo.Name)){
          $scope.messageName = "Định dạng tên không kèm số";
          $scope.checkName = true;
         }else{
          $scope.checkName = false; 
         }
      }

      

      

      if (!$scope.mailContactInfo.Email) {
        $scope.checkEmail = true;
        $scope.messageEmail = "Vui lòng nhập địa chỉ email.";
      }else{
        if (!validateEmail($scope.mailContactInfo.Email)) {
          $scope.messageEmail = "Vui lòng nhập đúng định dạng email.";
          $scope.checkEmail = true;
        }else{
          $scope.checkEmail = false; 
        }
      }

      const numberCharPhone = ($scope.mailContactInfo.Phone || "").length;
      if (
        !$scope.mailContactInfo.Phone
      ) {
        $scope.messagePhone = "Vui lòng nhập số điện thoại.";
        $scope.checkPhone = true; 
      }else{
        if (numberCharPhone < 10 ||
          numberCharPhone > 11
        ){
          $scope.messagePhone = "Vui lòng nhập đúng định dạng số điện thoại.";
          $scope.checkPhone = true; 
        }else{
          $scope.checkPhone = false; 
        }
        
      }

      if($scope.checkName == false && $scope.checkEmail == false  && $scope.checkPhone == false ){
        alert('Bạn đã gửi thông tin');
        var data = $scope.mailContactInfo;
        $scope.mailContactInfo = {
          Name: "",
          Email: "",
          Phone: "",
          Subject: "",
          Content: "",
          DateSent: getDate(new Date()),
          DeviceType: getDeviceType(),
          IP: "",
        };
        var controller = requestApiHelper.MAIL_CONTACT.ADD;
        var param = data;
        baseService.POST(controller, param).then(
          function (response) {
            $timeout(function(){
              btnSubmit.disabled = false;
            }, 500);
          },
          function (err) {
            console.log(err);
          }
        );
      }else{
        btnSubmit.disabled = false;
        return;
      }
      

      function validateEmail(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
      }

      function stringContainsNumber(_input){
        let string1 = String(_input);
        for( let i = 0; i < string1.length; i++){
            if(!isNaN(string1.charAt(i)) && !(string1.charAt(i) === " ") ){
              return true;
            }
        }
        return false;
      }

    };


    $scope.tempsdata = [
      {
        imageUrl:"assets/clients/imgs/loiich/icon/vi-tri-dat-gia.png",
        title: 'vị trí đắt giá',
        title_1: 'vị trí',
        title_2: 'đắt giá',
        content: 'Tọa lạc ngay trung tâm Thủ Phủ Resort, gần sân bay Phan Thiết nhất trong bán kính 2km'
      },
      {
        imageUrl:"assets/clients/imgs/loiich/icon/doc-dao.png",
        title: 'Concept độc đáo',
        title_1: 'Concept',
        title_2: 'độc đáo',
        content: 'Mô hình All-inclusive resort khác biệt và hiện đại hàng đầu Phan Thiết'
      },
      {
        imageUrl:"assets/clients/imgs/loiich/icon/phap-li-chuan-chinh.png",
        title: 'Pháp lí chuẩn chỉnh',
        title_1: 'Pháp lí',
        title_2: 'chuẩn chỉnh',
        content: 'Pháp lý chuẩn chỉnh và minh bạch nhất thị trường Phan Thiết, sở hữu lâu dài'
      },
      {
        imageUrl:"assets/clients/imgs/loiich/icon/suc-bat-ha-tang.png",
        title: 'sức bật hạ tầng',
        title_1: 'sức bật',
        title_2: 'hạ tầng',
        content: 'Đón đầu cơ hội khi Sân bay Phan Thiết và Cao tốc Dầu Giây - Phan Thiết sẽ hoàn thành năm 2022'
      },
      {
        imageUrl:"assets/clients/imgs/loiich/icon/the-manh-du-lich.png",
        title: 'thế mạnh du lịch',
        title_1: 'thế mạnh',
        title_2: 'du lịch',
        content: 'Phan Thiết định hướng trở thành Đô thị du lịch biển cấp quốc gia thu hút du khách trong & ngoài nước'
      }
    ];

    $scope.onClickLoiIch = function(value) {
      $scope.Enable_CHECK.enable_li = false;
      $scope.tempsdata = $scope.tempsdata.swap(value, 2);
    }

    $scope.onChangeDisable = function(){
      $scope.Enable_CHECK.enable_ti = false;
    }

    Array.prototype.swap = function (x,y) {
      var b = this[x];
      this[x] = this[y];
      this[y] = b;
      gsap.from('.loiich-wrapper', 1, {
            opacity: 0,
            x: -200
          }, 0.05)
          gsap.from('.item-loiich-center', 1, {
            opacity: 0,
            x: -200
          }, 0.05)
      return this;
    }

    $scope.mydetail = function(item) {
      $location.path('tin-tuc/' + item.ArtMeta);  
    };

    getIP();

    function getSeo(){
      var controllerSeo = requestApiHelper.TEMPLATE.SEO;
        var data = {
            meta: 'trang-chu',
        };
        baseService.POST(controllerSeo, data).then(function (response) {

            ngMeta.setTitle(response.SeoTitle);
            ngMeta.setTag('description', response.SeoDescribes);
            ngMeta.setTag('keywords',  response.SeoKeyword);
            ngMeta.setTag('og:image',  baseService.ROOT_URL.concat(response.Image));
            ngMeta.setTag('og:locale',  'vi');
            ngMeta.setTag('author', 'summerland');
        }, function (err) {
            console.log(err);
        });
      
    };

    getSeo();
    $scope.downloadfile = function () {
      var controller = requestApiHelper.FILE.ALL;
        baseService.GET(controller).then(function(response){
            if(response[0].File !== null && response[0].File.trim() !== ""){
              var a = document.createElement("a");
              a.href = baseService.ROOT_URL.concat(response[0].File);
              a.target = "_blank";
              a.click();
            }else{
              alert("Hiện tại chưa có file dự án!")
            }
        }, function(err){
            console.log(err);
        });
    };

    function Animation(){
      var deviceWidth = $(window).width();
          if(deviceWidth <= 480) {

          setInterval(swapFunc, 3000);

          function swapFunc() {
            for( let i = 0; i < $scope.tempsdata.length; i++){
              $timeout(function(){
                $scope.tempsdata.swap( i, 2);
              }, 5000);
            }
          }

          Array.prototype.swap = function (x,y) {
            var b = this[x];
            this[x] = this[y];
            this[y] = b;
            return this;
          }
      }
    }

    // Animation();
  }]);