(function () {
  'use strict';
  angular
    .module('vpApp')
    .controller('TienichtangController', TienichtangController);

  TienichtangController.$inject = ['fullPageService'];

  function TienichtangController(fullPageService) {
    var options = {
      anchors: ['tang-1', 'tang-4', 'tang-5', 'tang-27'],
      menu: '#tienichtang-nav',
      lockAnchors: false,
      css3: false,
      responsiveWidth: 1200,
      verticalCentered: false,
      navigation: false,
      navigationPosition: 'right',
      resetSliders: true,
      responsive: 1201,
      afterResponsive: function (isResponsive) {
        if (isResponsive) {
          $('html, body').css('overflow-x', 'hidden')
        }
      },
      afterLoad: function (origin, destination, direction) {

      },
      onLeave: function (index, nextIndex, direction) {
        $('.main-nav ').removeClass('active-nav')
        $('.body-wrapper').removeClass('active-body-wrapper')
        $('.wrapper-detail').removeClass('active-wrapper-detail')
        $('.menu').removeClass('show-menu')
        $('.button-nav').removeClass('active-button')
      },
    };

  


    
    

    var rebuild = function () {
      destroyFullPage();
      $('#fullpagetienichtang').fullpage(options);
    };

    var destroyFullPage = function () {
      if ($.fn.fullpage.destroy) {
        $.fn.fullpage.destroy('all');
      }
    };
    rebuild();


    $(".all-dot-top a").on("mouseenter click", function (e) {
        e.preventDefault(), e.stopPropagation(), $(".all-dot-top a, .note-facilities li").removeClass("current"),
          $(this).addClass("current"), $(".show-box-pic").removeClass("showup");
        var t = $(this).attr("data-name"),
          a = $(this).offset().left,
          o = $(this).offset().top,
          l = $(this).attr("data-box"),
          i = $(".show-box-pic[data-pic='" + l + "']").innerHeight(),
          s = $(".show-box-pic[data-pic='" + l + "']").innerHeight();
        var widthText = $(".show-box-pic[data-pic='" + l + "']").innerWidth();

        return $(window).width() > 1100 ? ($(".show-box-pic.no-pic[data-pic='" + l + "']").css({
          left: i > 80 ? a - ((widthText / 2) - 15) : a - (widthText + 20),
          top: i > 80 ? o + 35 : o - 10
        }).addClass("showup"), $(".show-box-pic:not(.no-pic)[data-pic='" + l + "']").css({
          left: a + 60,
          top: o - i / 2
        }).addClass("showup"), $(".note-facilities li[data-text='" + t + "']").addClass("current")) : ($(".show-box-pic[data-pic='" + l + "']").css({
          left: a - ((widthText / 2) - 8),
          top: i > 80 ? o - 20 : o - 40
        }).addClass("showup"), $(".note-facilities li[data-text='" + t + "']").addClass("current")), !1
      }),
      $(".note-facilities li, .all-dot-top a").on("mouseleave", function () {
        $(".all-dot-top a, .note-facilities li").removeClass("current"), $(".show-box-pic").removeClass("showup")
      }),
      $(".all-dot-top a:not(.no-pic)").on("click", function (e) {
        if (e.preventDefault(), e.stopPropagation(), $(".show-box-pic").removeClass("current"), $(window).width() > 1100) {
          var t = $(this).attr("data-name");
          if ($(".show-box-pic[data-pic='" + t + "']").removeClass("showup").addClass("current"), "" !== t) {
            var a = $(".show-box-pic[data-pic='" + t + "']").find("img").attr("data-src"),
              o = $(".show-box-pic[data-pic='" + t + "']").find(".faci-text h3").text();
            ThumbZoom(a, o), $(".all-dot-top a, .note-facilities li").removeClass("current")
          }
        }
        return !1
      }), $(".note-facilities li").on("mouseenter click", function (e) {
        e.preventDefault(), e.stopPropagation(), $(".all-dot-top a, .note-facilities li").removeClass("current"), $(".show-box-pic").removeClass("showup"), $(this).addClass("current");
        var t = $(this).attr("data-text");
        $(".all-dot-top a[data-name='" + t + "']").trigger("mouseenter")
      }), $(".show-box-pic:not(.no-pic)").on("click", function (e) {
        e.preventDefault(), e.stopPropagation(), $(".all-dot-top a, .note-facilities li").removeClass("current"), $(".show-box-pic").removeClass("current"), $(this).removeClass("showup").addClass("current");
        var t = $(this).find("img").attr("data-src"),
          a = $(this).find(".faci-text h3").text();
        return ThumbZoom(t, a), $(".show-box-pic").removeClass("showup"), !1
      }), $(".show-box-pic.no-pic").on("click", function (e) {
        return e.preventDefault(), e.stopPropagation(), $(".show-box-pic").removeClass("showup"), $(".all-dot-top a, .note-facilities li").removeClass("current"), !1
      }), $(".container").on("click", function (e) {
        return e.preventDefault(), $(".all-dot-top a, .note-facilities li").removeClass("current"), $(".show-box-pic").removeClass("current"), $(".show-box-pic").removeClass("showup"), !1
      }),

      $(window).width() <= 1100 && $(".note-facilities").on('mousewheel',function (e, t) {
        $(this).scrollLeft(this.scrollLeft + 40 * -t), e.preventDefault()
      }), $(window).width() > 1100 && $(".box-nav li.current").length && setTimeout(function () {
        $(".box-nav li.current a").trigger("click")
      }, 1e3);

      function ResizeWindows() {
        var e = ($(window).height() > $(window).width(),
            $(window).height() <= $(window).width()),
          t = $(window).width(),
          a = $(window).height(),
          i = t / (2400 + t*0.25),
          o = a / 1100,
          n = t / (2000);

        if (440 >= t)
          var r = t / (1500 + t);
        else
          var r = t / (1400 + t);
       
        if (1200 >= t)
          1 == e ? 
            ($(".facilities-map").css({
              height: $(".facilities-bg").height() * n
            }),$(".facilities-bg").css('transform', 'scale('+n+')')) : 
            ($(".facilities-map").css({
              height: $(".facilities-bg").height() * r
            }),
            $(".facilities-bg").css('transform', 'scale('+r+')')),
          440 >= t ? (
            $(".facilities-bg").css({
              left: t / 2 - 1170,
              top: $(".facilities-map").height() / 2 - 560
            })) : (
            $(".facilities-bg").css({
              left: t / 2 - 1150,
              top: $(".facilities-map").height() / 2 - 580
            }));
        else if (t > 1200) {
          var mmm = $(".facilities-bg");
          console.log(mmm);
          if (a / t > 1 ? ($(".facilities-bg").css('transform', 'scale('+o+')')) : ($(".facilities-bg").css('transform', 'scale('+i+')')),
            $(".facilities-map").height(a),
            t > 1200 && 750 > a ? $(".facilities-bg").css({
              left: t / 2 - 1350,
              top: a / 2 - 640
            }) : $(".facilities-bg").css({
                left: t / 2 - 1350,
                top: a / 2 - 640
            })
          ) {}
        }
      }
      $(window).on("resize", function () {
        ResizeWindows();
      });
      ResizeWindows();
  }

})();