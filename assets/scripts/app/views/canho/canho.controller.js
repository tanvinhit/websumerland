(function () {
    'use strict';
    angular
      .module('vpApp')
      .controller('CanhoController', CanhoController);
  
    CanhoController.$inject = ['fullPageService', "ngMeta", "requestApiHelper", "$scope", "$rootScope", "baseService", "$timeout", "$sce"];
  
    function CanhoController(fullPageService, ngMeta, requestApiHelper, $scope, $rootScope, baseService, $timeout, $sce) {
      var options = {
        anchors: ['gioi-thieu', 'tien-ich','tien-ich-tang', 'mat-bang-tong-the','hinh-anh-noi-that'],
        menu: '#canho-nav',
        lockAnchors: false,
        css3: false,
        responsiveWidth: 1100,
        verticalCentered: false,
        navigation: false,
        navigationPosition: 'right',
        afterResponsive: function (isResponsive) {
          if (isResponsive) {
            $('html, body').css('overflow-x', 'hidden')
          }
        },
        afterLoad: function (origin, destination, direction) {
          var deviceWidth = $(window).width();
          if(deviceWidth < 1200 && $.fn.fullpage.setMouseHijack) {
            $.fn.fullpage.setMouseHijack(false);
          }
          
        },
        onLeave: function (anchorCurrent, anchorNext, direction) {
          $('.main-nav ').removeClass('active-nav')
          $('.body-wrapper').removeClass('active-body-wrapper')
          $('.wrapper-detail').removeClass('active-wrapper-detail')
          $('.menu').removeClass('show-menu')
          $('.button-nav').removeClass('active-button')
          
          if (anchorNext.anchor === "tien-ich-tang" ){
            $scope.dataLoaded_3 = true;
            $timeout(function () {
              $(window).on("resize", function () {
                ResizeWindows();
              });
                ResizeWindows();
                EventWindows();
            }, 0);
          }else{
            if (anchorNext.anchor === "tien-ich" && $scope.section.section2_CATESLIDE.length == 0 && $scope.section.section2_SLIDE.length == 0){
              var cSection2_2 = requestApiHelper.CATEGORY.GET + '/' + 'vi';
              var data_Section2_2 = {
                  type: 'admin',
                  dataType: 6
              };
              baseService.POST(cSection2_2, data_Section2_2).then(function (res2) {
                $scope.section.section2_CATESLIDE = res2;
                var param_a = {
                  type: 'admin'
                };
                var tabsRequest = [];
                for (let i = 0; i < $scope.section.section2_CATESLIDE.length; i++) {
                  var controller_a = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + $scope.section.section2_CATESLIDE[i].CatID;
                  tabsRequest.push(baseService.POST(controller_a, param_a));
                }
            
                Promise.all(tabsRequest).then(values => {
                  values.forEach((res_a, i) => {
                    var oList = {
                      tab: "tab" + $scope.section.section2_CATESLIDE[i].CatID,
                      list: res_a
                    }
                    $scope.section.section2_SLIDE.push(oList);
                  })
            
                  $timeout(function () {
                    $scope.item_2 = $scope.section.section2_SLIDE;
                    loadData_2($scope.item_2);
                  }, 0);
                }, function (err) {
                  console.log(err);
                })
              }, function (err) {
                console.log(err);
              });
  
              function loadData_2(x){
                $timeout(function(){
                  $scope.itemAs_2 = x;
                  $scope.dataLoaded_2 = true;
                }, 0);
              };
            }
  
            if (anchorNext.anchor === "mat-bang-tong-the" && $scope.section.section4_CATESLIDE.length == 0 && $scope.section.section4_SLIDE.length == 0){
              var cSection4_2 = requestApiHelper.CATEGORY.GET + '/' + 'vi';
              var data_Section4_2 = {
                  type: 'admin',
                  dataType: 7
              };
              baseService.POST(cSection4_2, data_Section4_2).then(function (res4) {
                $scope.section.section4_CATESLIDE = res4;
                var param_a = {
                  type: 'admin'
                };
                var tabsRequest = [];
                for (let i = 0; i < $scope.section.section4_CATESLIDE.length; i++) {
                  var controller_a = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + $scope.section.section4_CATESLIDE[i].CatID;
                  tabsRequest.push(baseService.POST(controller_a, param_a));
                }
            
                Promise.all(tabsRequest).then(values => {
                  values.forEach((res_a, i) => {
                    var oList = {
                      tab: "tab" + $scope.section.section4_CATESLIDE[i].CatID,
                      list: res_a
                    }
                    $scope.section.section4_SLIDE.push(oList);
                  })
            
                  $timeout(function () {
                    $scope.item_4 = $scope.section.section4_SLIDE;
                    loadData_4($scope.item_4);
                  }, 0);
                }, function (err) {
                  console.log(err);
                })
                console.log($scope.section);
              }, function (err) {
                console.log(err);
              });
  
              function loadData_4(x){
                $timeout(function(){
                  $scope.itemAs_4 = x;
                  $scope.dataLoaded_4 = true;
                }, 0);
              };
            }
  
            if (anchorNext.anchor === "hinh-anh-noi-that" && $scope.section.section5_CATESLIDE.length == 0 && $scope.section.section5_SLIDE.length == 0){
              var cSection5_2 = requestApiHelper.CATEGORY.GET + '/' + 'vi';
              var data_Section5_2 = {
                  type: 'admin',
                  dataType: 8
              };
              baseService.POST(cSection5_2, data_Section5_2).then(function (res5) {
                $scope.section.section5_CATESLIDE = res5;
                var param_a = {
                  type: 'admin'
                };
                var tabsRequest = [];
                for (let i = 0; i < $scope.section.section5_CATESLIDE.length; i++) {
                  var controller_a = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + $scope.section.section5_CATESLIDE[i].CatID;
                  tabsRequest.push(baseService.POST(controller_a, param_a));
                }
            
                Promise.all(tabsRequest).then(values => {
                  values.forEach((res_a, i) => {
                    var oList = {
                      tab: "tab" + $scope.section.section5_CATESLIDE[i].CatID,
                      list: res_a
                    }
                    $scope.section.section5_SLIDE.push(oList);
                  })
            
                  $timeout(function () {
                    $scope.item_5 = $scope.section.section5_SLIDE;
                    loadData_5($scope.item_5);
                  }, 0);
                }, function (err) {
                  console.log(err);
                })
              }, function (err) {
                console.log(err);
              });
  
              function loadData_5(x){
                $timeout(function(){
                  $scope.itemAs_5 = x;
                  console.log($scope.section);
                  $scope.dataLoaded_5 = true;
                  if($scope.dataLoaded_5 && $scope.dataLoaded_4 && $scope.dataLoaded_2){
                  }
                }, 0);
              };
            }
          }
        },
      };

      var rebuild = function() {
        destroyFullPage();
  
        $('#fullpage3').fullpage(options);
      };
  
      var destroyFullPage = function() {
        if ($.fn.fullpage.destroy) {
          $.fn.fullpage.destroy('all');
        }
      };
      rebuild();

      function EventWindows() {
        $(".all-dot-top a").on("mouseenter click", function (e) {
          e.preventDefault(), e.stopPropagation(), $(".all-dot-top a, .note-facilities li").removeClass("current"),
            $(this).addClass("current"), $(".show-box-pic").removeClass("showup");
          var t = $(this).attr("data-name"),
            a = $(this).offset().left,
            o = $(this).offset().top,
            l = $(this).attr("data-box"),
            i = $(".show-box-pic[data-pic='" + l + "']").innerHeight(),
            s = $(".show-box-pic[data-pic='" + l + "']").innerHeight();
          var widthText = $(".show-box-pic[data-pic='" + l + "']").innerWidth();

          return $(window).width() > 1100 ? ($(".show-box-pic.no-pic[data-pic='" + l + "']").css({
            left: i > 80 ? a - ((widthText / 2) - 15) : widthText + 20 > a ? a + 20 : a - (widthText + 10),
            top: i > 80 ? o + 35 : o - 10
          }).addClass("showup"), $(".show-box-pic:not(.no-pic)[data-pic='" + l + "']").css({
            left: a + 60,
            top: o - i / 2
          }).addClass("showup"), $(".note-facilities li[data-text='" + t + "']").addClass("current")) : ($(".show-box-pic[data-pic='" + l + "']").css({
            left: widthText + 20 > a ? a + 10 : a - (widthText + 10),
            top: i > 80 ? o - 20 : o - 10
          }).addClass("showup"), $(".note-facilities li[data-text='" + t + "']").addClass("current")), !1
        }),
        $(".note-facilities li, .all-dot-top a").on("mouseleave", function () {
          $(".all-dot-top a, .note-facilities li").removeClass("current"), $(".show-box-pic").removeClass("showup")
        }),
        $(".all-dot-top a:not(.no-pic)").on("click", function (e) {
          if (e.preventDefault(), e.stopPropagation(), $(".show-box-pic").removeClass("current"), $(window).width() > 1100) {
            var t = $(this).attr("data-name");
            if ($(".show-box-pic[data-pic='" + t + "']").removeClass("showup").addClass("current"), "" !== t) {
              var a = $(".show-box-pic[data-pic='" + t + "']").find("img").attr("data-src"),
                o = $(".show-box-pic[data-pic='" + t + "']").find(".faci-text h3").text();
              ThumbZoom(a, o), $(".all-dot-top a, .note-facilities li").removeClass("current")
            }
          }
          return !1
        }), $(".note-facilities li").on("mouseenter click", function (e) {
          e.preventDefault(), e.stopPropagation(), $(".all-dot-top a, .note-facilities li").removeClass("current"), $(".show-box-pic").removeClass("showup"), $(this).addClass("current");
          var t = $(this).attr("data-text");
          $(".all-dot-top a[data-name='" + t + "']").trigger("mouseenter")
        }), $(".show-box-pic:not(.no-pic)").on("click", function (e) {
          e.preventDefault(), e.stopPropagation(), $(".all-dot-top a, .note-facilities li").removeClass("current"), $(".show-box-pic").removeClass("current"), $(this).removeClass("showup").addClass("current");
          var t = $(this).find("img").attr("data-src"),
            a = $(this).find(".faci-text h3").text();
          return ThumbZoom(t, a), $(".show-box-pic").removeClass("showup"), !1
        }), $(".show-box-pic.no-pic").on("click", function (e) {
          return e.preventDefault(), e.stopPropagation(), $(".show-box-pic").removeClass("showup"), $(".all-dot-top a, .note-facilities li").removeClass("current"), !1
        }), $(".container").on("click", function (e) {
          return e.preventDefault(), $(".all-dot-top a, .note-facilities li").removeClass("current"), $(".show-box-pic").removeClass("current"), $(".show-box-pic").removeClass("showup"), !1
        }),

        $(window).width() <= 1100 && $(".note-facilities").on('mousewheel',function (e, t) {
          $(this).scrollLeft(this.scrollLeft + 40 * -t), e.preventDefault()
        }), $(window).width() > 1100 && $(".box-nav li.current").length && setTimeout(function () {
          $(".box-nav li.current a").trigger("click")
        }, 1e3);
      }

      function ResizeWindows() {
        var e = ($(window).height() > $(window).width(),
            $(window).height() <= $(window).width()),
          t = $(window).width(),
          a = $(window).height(),
          i = t / (2400 + t*0.25),
          o = a / 1100,
          n = t / (2000);

        if (440 >= t)
          var r = t / (1500 + t);
        else
          var r = t / (1400 + t);
       
        if (1200 >= t)
          1 == e ? 
            ($(".facilities-map").css({
              height: $(".facilities-bg").height() * n
            }),$(".facilities-bg").css('transform', 'scale('+n+')')) : 
            ($(".facilities-map").css({
              height: $(".facilities-bg").height() * r
            }),
            $(".facilities-bg").css('transform', 'scale('+r+')')),
          440 >= t ? (
            $(".facilities-bg").css({
              left: t / 2 - 1170,
              top: $(".facilities-map").height() / 2 - 560
            })) : (
            $(".facilities-bg").css({
              left: t / 2 - 1150,
              top: $(".facilities-map").height() / 2 - 580
            }));
        else if (t > 1200) {
          if (a / t > 1 ? ($(".facilities-bg").css('transform', 'scale('+o+')')) : ($(".facilities-bg").css('transform', 'scale('+i+')')),
            $(".facilities-map").height(a),
            t > 1200 && 750 > a ? $(".facilities-bg").css({
              left: t / 2 - 1350,
              top: a / 2 - 640
            }) : $(".facilities-bg").css({
                left: t / 2 - 1350,
                top: a / 2 - 640
            })
          ) {}
        }

        var heightIamgeTienIch = $(".tien-ich-tang-wrapper .facilities-map").innerHeight();
        if (1200 >= t) {
          $(".tien-ich-tang-wrapper .fp-controlArrow").css({
            top: (heightIamgeTienIch / 2) + 80
          })
        } else if (t > 1200) {
          $(".tien-ich-tang-wrapper .fp-controlArrow").css({
            top: (heightIamgeTienIch / 2) + 20
          })
        }

      }

      
      $scope.dataLoaded_2 = false;
      $scope.dataLoaded_3 = false;
      $scope.dataLoaded_4 = false;
      $scope.dataLoaded_5 = false;
      $scope.section = {
        section2_SLIDE: [],
        section2_CATESLIDE: [],
        section4_SLIDE: [],
        section4_CATESLIDE: [],
        section5_CATESLIDE: [],
        section5_SLIDE: []
      };

      function getServiceHot() {
        var cSection2_2 = requestApiHelper.CATEGORY.GET + '/' + 'vi';
              var data_Section2_2 = {
                  type: 'admin',
                  dataType: 6
              };
              baseService.POST(cSection2_2, data_Section2_2).then(function (res2) {
                $scope.section.section2_CATESLIDE = res2;
                var param_a = {
                  type: 'admin'
                };
                var tabsRequest = [];
                for (let i = 0; i < $scope.section.section2_CATESLIDE.length; i++) {
                  var controller_a = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + $scope.section.section2_CATESLIDE[i].CatID;
                  tabsRequest.push(baseService.POST(controller_a, param_a));
                }
            
                Promise.all(tabsRequest).then(values => {
                  values.forEach((res_a, i) => {
                    var oList = {
                      tab: "tab" + $scope.section.section2_CATESLIDE[i].CatID,
                      list: res_a
                    }
                    $scope.section.section2_SLIDE.push(oList);
                  })
            
                  $timeout(function () {
                    $scope.item_2 = $scope.section.section2_SLIDE;
                    loadData_2($scope.item_2);
                  }, 0);
                }, function (err) {
                  console.log(err);
                })
              }, function (err) {
                console.log(err);
              });
  
              function loadData_2(x){
                $timeout(function(){
                  $scope.itemAs_2 = x;
                  $scope.dataLoaded_2 = true;
                }, 0);
              };

              var cSection4_2 = requestApiHelper.CATEGORY.GET + '/' + 'vi';
              var data_Section4_2 = {
                  type: 'admin',
                  dataType: 7
              };
              baseService.POST(cSection4_2, data_Section4_2).then(function (res4) {
                $scope.section.section4_CATESLIDE = res4;
                var param_a = {
                  type: 'admin'
                };
                var tabsRequest = [];
                for (let i = 0; i < $scope.section.section4_CATESLIDE.length; i++) {
                  var controller_a = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + $scope.section.section4_CATESLIDE[i].CatID;
                  tabsRequest.push(baseService.POST(controller_a, param_a));
                }
            
                Promise.all(tabsRequest).then(values => {
                  values.forEach((res_a, i) => {
                    var oList = {
                      tab: "tab" + $scope.section.section4_CATESLIDE[i].CatID,
                      list: res_a
                    }
                    $scope.section.section4_SLIDE.push(oList);
                  })
            
                  $timeout(function () {
                    $scope.item_4 = $scope.section.section4_SLIDE;
                    loadData_4($scope.item_4);
                  }, 0);
                }, function (err) {
                  console.log(err);
                })
                console.log($scope.section);
              }, function (err) {
                console.log(err);
              });
  
              function loadData_4(x){
                $timeout(function(){
                  $scope.itemAs_4 = x;
                  $scope.dataLoaded_4 = true;
                }, 0);
              };


              var cSection5_2 = requestApiHelper.CATEGORY.GET + '/' + 'vi';
              var data_Section5_2 = {
                  type: 'admin',
                  dataType: 8
              };
              baseService.POST(cSection5_2, data_Section5_2).then(function (res5) {
                $scope.section.section5_CATESLIDE = res5;
                var param_a = {
                  type: 'admin'
                };
                var tabsRequest = [];
                for (let i = 0; i < $scope.section.section5_CATESLIDE.length; i++) {
                  var controller_a = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + $scope.section.section5_CATESLIDE[i].CatID;
                  tabsRequest.push(baseService.POST(controller_a, param_a));
                }
            
                Promise.all(tabsRequest).then(values => {
                  values.forEach((res_a, i) => {
                    var oList = {
                      tab: "tab" + $scope.section.section5_CATESLIDE[i].CatID,
                      list: res_a
                    }
                    $scope.section.section5_SLIDE.push(oList);
                  })
            
                  $timeout(function () {
                    $scope.item_5 = $scope.section.section5_SLIDE;
                    loadData_5($scope.item_5);
                  }, 0);
                }, function (err) {
                  console.log(err);
                })
              }, function (err) {
                console.log(err);
              });
  
              function loadData_5(x){
                $timeout(function(){
                  $scope.itemAs_5 = x;
                  console.log($scope.section);
                  $scope.dataLoaded_5 = true;
                  if($scope.dataLoaded_5 && $scope.dataLoaded_4 && $scope.dataLoaded_2){
                  }
                }, 0);
              };  
      }

      function getSeo(){
        var controllerSeo = requestApiHelper.TEMPLATE.SEO;
          var data = {
              meta: 'can-ho',
          };
          baseService.POST(controllerSeo, data).then(function (response) {
  
              ngMeta.setTitle(response.SeoTitle);
              ngMeta.setTag('description', response.SeoDescribes);
              ngMeta.setTag('keywords',  response.SeoKeyword);
              ngMeta.setTag('og:image',  baseService.ROOT_URL.concat(response.Image));
              ngMeta.setTag('og:locale',  'vi');
              ngMeta.setTag('author', 'summerland');
          }, function (err) {
              console.log(err);
          });
        
      };
  
      getSeo();
      
    }
  
  })();