;(function() {
  'use strict';

  angular
    .module('vpApp', [
      'fullPage.js',
      'ui.router', 
      'ngRoute',
      'ckeditor', 
      'ngSanitize', 
      'angular-md5', 
      'ngFlash', 
      'ui.bootstrap.datetimepicker', 
      'angularUtils.directives.dirPagination', 
      'checklist-model', 
      'ngMap', 
      'angular-js-xlsx',
      'slick',
      'ngMeta'
    ]).run(['ngMeta', function (ngMeta) {
      ngMeta.init();
    }]);;
  app.value('version', '0.3.4');

})();
