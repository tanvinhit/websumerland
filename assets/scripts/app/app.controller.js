;
(function () {
  'use strict';

  angular
    .module('vpApp')
    .controller('AppController', AppController);

  AppController.$inject = ['$scope', '$rootScope', '$state', '$location', "requestApiHelper", "baseService"];

  function AppController($scope, $rootScope, $state, $location, requestApiHelper, baseService) {

    $rootScope.changePage = changePage;

    function changePage() {
      $state.go($rootScope.chosenPage);
    }

    $scope.navigate = function (link, anchor) {
      $location.path(link);
      var deviceWidth = $(window).width();
      if(deviceWidth < 1200) {
        setTimeout(() => {
          $.fn.fullpage && $.fn.fullpage.moveTo(anchor);
        }, 1250);
      }else{
        setTimeout(() => {
          $.fn.fullpage && $.fn.fullpage.moveTo(anchor);
        }, 900);
      }
      
    }
    init();
    function init() {
      var controller = requestApiHelper.SCRIPT.ALL;
        baseService.GET(controller).then(function(response){
            if(response[0].script !== null && response[0].script.trim() !== ""){
              $("head").append(response[0].script);
            }
        }, function(err){
            console.log(err);
        });
    }
  }

})();