angular.module('vpApp').controller('categoryCtrl',['$scope', '$rootScope', '$window', '$location', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $location, baseService, requestApiHelper){
    $scope.numberPage = '10';
    $scope.numOfPage = 5;      
    $scope.selectedCate = {}; 
    $scope.filterLang = 'vi';
    $scope.notice = {};  
    var typeLink = $location.path();
    $scope.listToDelete = [];
    var selectedAll = 0;
    $rootScope.currentPage = {
        parent: (typeLink.indexOf('productCategories') !== -1)? 'Product Category' : 'Category',
        child: ''        
    };
    $rootScope.dataType = (typeLink.indexOf('productCategories') !== -1)? 2 : 1;
    $rootScope.getCategories($scope.filterLang, $rootScope.dataType);
    $rootScope.getLanguages();
    $scope.event = {   
        add: function(){
            $rootScope.selectLang = $scope.filterLang;
            if($rootScope.dataType === 1)
                $location.path('admin/category/add'); 
            else
                $location.path('admin/productCategory/add'); 
        },
        edit: function(item){
            $scope.selectedCate = {};
            $scope.selectedCate = item;   
            $rootScope.selectLang = $scope.filterLang; 
            if($rootScope.dataType === 1)
                $location.path('admin/category/edit/' + item.CatID);
            else
                $location.path('admin/productCategory/edit/' + item.CatID);
        },     
        del: function(){                        
            var controller = requestApiHelper.CATEGORY.DELETE;            
            var param = {cateId : $scope.selectedCate.CatID};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response){
                    $rootScope.getCategories($scope.filterLang, $rootScope.dataType);          
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
                else{
                    baseService.showToast('Delete fail!', 'danger');
                }
            }, function(err){
                baseService.showToast('Delete fail!', 'danger');
                console.log(err.message);
            });
        },
        getItem: function(item){
            $scope.selectedCate = {};
            $scope.selectedCate = item;            
        },
        selectLang: function(code, id){    
            $scope.filterLang = code;            
            if(id === 0){                
                $rootScope.getCategories($scope.filterLang, $rootScope.dataType);
                $('#tab0').addClass('active');             
            }
            else{               
                $rootScope.getCategories($scope.filterLang, $rootScope.dataType);
                $('#tab'+id).addClass('active');     
            }
            var checkTabAll = $('#tab0').hasClass('active');
            if(checkTabAll && id !== 0){
                $('#tab0').removeClass('active');
            }
            else{
                for(var i=0;i<$rootScope.languages.length;i++){
                    var checkClass = $('#tab'+$rootScope.languages[i].ID).hasClass('active');
                    var idLang = $rootScope.languages[i].ID;
                    if(checkClass && id !== idLang){
                        $('#tab'+$rootScope.languages[i].ID).removeClass('active');
                        break;
                    }
                }
            }
        },
        checkAll: function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $rootScope.categories.length; i++){                    
                    $scope.listToDelete.push($rootScope.categories[i].CatID);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.length; i++){                    
                    $scope.listToDelete.splice(i, 1);                
                } 
            }
        },
        delAllItem: function(){
            var controller = requestApiHelper.CATEGORY.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    $rootScope.getCategories($scope.filterLang, $rootScope.dataType);
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            })
        }
    };
}]);