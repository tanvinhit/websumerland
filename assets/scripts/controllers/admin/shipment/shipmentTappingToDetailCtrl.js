angular.module('vpApp')
    .controller('shipmentTappingToDetailCtrl',['$scope', '$rootScope', '$location', '$routeParams', '$timeout', 'baseService', 'requestApiHelper', function ($scope, $rootScope, $location, $routeParams, $timeout, baseService, requestApiHelper) {
        $scope.meta = $location.path();
        var shipmentId = $routeParams.id;
        $scope.filterLang = "vi";
        $scope.shipment = {};
        $scope.selectedShipment = {};
        $scope.isUpdate = false;
        $scope.fromPlace = '';
        $scope.toPlace = '';
        $scope.shipmentCustomId = '';
        $rootScope.currentPage = {
            parent: 'Chuyến thư đến',
            child: 'Chi tiết túi thư'
        };
        $scope.listPackages = [];
        $scope.shipmentDetails = [];
        $scope.listTappingTo = {
            listShipmentDetails: []
        };
        function init(){
            $scope.isUpdate = true;
            baseService.GET(requestApiHelper.SHIPMENT.GET_BY_ID.concat('/', shipmentId)).then(function (respone) {
                if (respone){
                    $scope.shipment = respone;
                }
                else console.log('Không có dữ liệu chuyến thư');
            });

            getShipmentDetails();            
        }

        init();

        $scope.submitForm = function () {
            baseService.POST(requestApiHelper.SHIPMENT.UPDATE, $scope.shipment).then(function (respone) {
                if (respone) {
                    var message = 'Cập nhật chuyến thư thành công!';
                    baseService.showToast(message, 'success');
                } else {
                    var message = 'Cập nhật chuyến thư không thành công!';
                    baseService.showToast(message, 'danger');
                }
            });
        };

        $scope.event = {
            getItem: function(item){
                $scope.selectedShipment = {};
                $scope.selectedShipment = item;            
            },  
            checkAll: function(){
                if($scope.selectAll){
                    for(let i = 0; i < $scope.shipmentDetails.length; i++){                    
                        $scope.listTappingTo.listShipmentDetails.push($scope.shipmentDetails[i]);                
                    }            
                }
                else{                  
                    $scope.listTappingTo.listShipmentDetails = [];
                }
            },
            saveAll: function(){
                var controller = requestApiHelper.SHIPMENT.TAPPING_TO_ALL_PACKAGE;
                baseService.POST(controller, $scope.listTappingTo.listShipmentDetails).then(function(response){
                    if(response){
                        $scope.listTappingTo.listShipmentDetails = [];
                        baseService.showToast('Thành công!', 'success');
                        $('#myModal').modal('hide');
                        getShipmentDetails();
                    }
                    else if(response.redirect !== undefined){
                        $window.location.href = response.redirect;
                    }
                    else{
                        baseService.showToast('Thất bại!', 'danger');
                    }
                }, function(err){
                    baseService.showToast(err.data.message, 'danger');
                });
            },
            save: function(shipDetailId, isReceived){
                var controller = requestApiHelper.SHIPMENT.TAPPING_TO_PACKAGE.concat('/', shipDetailId, '/', isReceived);
                baseService.GET(controller).then(function(response){
                    if(response){
                        baseService.showToast('Thành công!', 'success');
                        $('#myModal').modal('hide');
                        getShipmentDetails();
                    }
                    else if(response.redirect !== undefined){
                        $window.location.href = response.redirect;
                    }
                    else{
                        baseService.showToast('Thất bại!', 'danger');
                    }
                }, function(err){
                    baseService.showToast(err.data.message, 'danger');
                });
            }
        };

        function getShipmentDetails() {
            baseService.GET(requestApiHelper.SHIPMENT.GET_DETAIL_BY_ID.concat('/', shipmentId)).then(function (respone) {
                if (respone){
                    $scope.shipmentDetails = respone;
                }
                else console.log('Không có dữ liệu chuyến thư');
            });
        }
}]);