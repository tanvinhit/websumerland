angular.module('vpApp').controller('packageTappingToCtrl',['$scope', '$rootScope', '$window', '$location', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $location, baseService, requestApiHelper){
    $scope.numberPage = '10';        
    $scope.numOfPage = 5;
    $scope.packages = [];
    $scope.selectedPackage = {};
    $scope.listToDelete = {
        listPackIds: []
    };
    var selectedAll = 0;
    var typeLink = $location.path();
    $rootScope.currentPage = {
        parent: 'Bảng kê tải đến',
        child: '',
        class: 'fa fa-edit'
    };         
    function init(){        
        var controller = requestApiHelper.PACKAGE.GET_TAPPING_TO;
        baseService.GET(controller).then(function(response){                           
            $scope.packages = response;                     
        }, function(error){
            console.log(error.message);
        });
    };            
    init();                 
    $scope.event = {
        edit: function(x){
            $scope.isUpdate = true;
            $scope.selectedPackage = {};
            $scope.selectedPackage = x;
            $location.path(`admin/tapping-to/package/detail/${x.packageId}`);
        }
    };  
}]);