angular.module('vpApp').controller('packageCtrl',['$scope', '$rootScope', '$window', '$location', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $location, baseService, requestApiHelper){
    $scope.numberPage = '10';        
    $scope.numOfPage = 5;
    $scope.packages = [];
    $scope.selectedPackage = {};
    $scope.listToDelete = {
        listPackIds: []
    };
    var selectedAll = 0;
    $scope.isUpdate = false;
    var typeLink = $location.path();
    $rootScope.currentPage = {
        parent: 'Bảng kê tải',
        child: '',
        class: 'fa fa-edit'
    };         
    function init(){        
        var controller = requestApiHelper.PACKAGE.GET;
        baseService.GET(controller).then(function(response){                           
            $scope.packages = response;                     
        }, function(error){
            console.log(error.message);
        });
    };            
    init();                 
    $scope.event = {
        add: function(){
            $scope.isUpdate = false;
            $scope.selectedPackage = {};
            $location.path('admin/package/add');
        },
        edit: function(x){
            $scope.isUpdate = true;
            $scope.selectedPackage = {};
            $scope.selectedPackage = x;
            $location.path(`admin/package/detail/${x.packageId}`);
        },
        del: function(){
            var controller = requestApiHelper.PACKAGE.DELETE.concat("/", $scope.selectedPackage.packageId);
            baseService.GET(controller).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response){
                    init();
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
                else{
                    baseService.showToast('Delete fail!', 'danger');
                }
            }, function(err){
                console.log(err.message);
                baseService.showToast('Delete fail!', 'danger');
            });
        },
        getItem: function(item){
            $scope.selectedPackage = {};
            $scope.selectedPackage = item;            
        },  
        checkAll: function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $scope.packages.length; i++){                    
                    $scope.listToDelete.listPackIds.push($scope.packages[i].packageId);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.listPackIds.length; i++){                    
                    $scope.listToDelete.listPackIds.splice(i, 1);                
                } 
            }
        },
        delAllItem: function(){
            var controller = requestApiHelper.PACKAGE.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete.listPackIds).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    init();
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            });
        }
    };  
}]);