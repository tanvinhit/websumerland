angular.module('vpApp').controller('MaterplanCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Mặt bằng',
        child: ''
    };
    $scope.filterLang = 'vi';
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };
    $scope.notice = {
        class: '',
        message: ''
    };
    $scope.plusServiceList = [];
    $scope.selectedMasterPlanType = {};
    function init(){
        var controller = requestApiHelper.MASTER_PLAN.ALL;
        baseService.GET(controller).then(function(response){
            $scope.plusServiceList = response; 
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    getMasterPlanType();
    init();    
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Sửa mặt bằng';
            $scope.isUpdate = true;
            $scope.selectedMasterPlanType = {};            
            $scope.selectedMasterPlanType = item;             
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        add: function(){
            $scope.label.title = 'Thêm mặt bằng';
            $scope.isUpdate = false;
            $scope.selectedMasterPlanType = {
                image: '',
                floor_name: '',
                name: '',
                description: '',
                path_name: '',
                sort: 0,
                Status: 1
            };
            $('#img').attr('src', '');
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        submitForm: function(){
            //var noimage = 'assets/includes/upload/images/users/noimage.jpg';
            if($scope.Form.$valid){
                if($scope.isUpdate){
                    var controller = requestApiHelper.MASTER_PLAN.UPDATE_MASTER_PLAN;
                    delete $scope.selectedMasterPlanType.name;
                    baseService.POST(controller, $scope.selectedMasterPlanType).then(function(response){
                        if(response){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Cập nhật thành công!', 'success');
                        }  
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Cập nhật thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
                else{
                    var controller = requestApiHelper.MASTER_PLAN.ADD_MASTER_PLAN;
                   // $scope.selectedMasterPlanType.image = ($scope.selectedMasterPlanType.Avatar === '')? noimage : $scope.selectedMasterPlanType.image;
                    baseService.POST(controller, $scope.selectedMasterPlanType).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Thêm thành công!', 'success');
                        } 
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Thêm thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
            }          
        },
        del: function(){                        
            var controller = requestApiHelper.MASTER_PLAN.DELETE_MASTER_PLAN;
            var param = {id: $scope.selectedMasterPlanType.id};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();
                    $('#delAlert').modal('hide');  
                    baseService.showToast('Xóa thành công!', 'success');
                } 
                else{
                    baseService.showToast('Xóa thất bại!', 'danger');
                }
            }, function(err){                
                console.log(err);
            })
        },
        getItem: function(item){
            $scope.selectedMasterPlanType = {};                   
            $scope.selectedMasterPlanType = item;                     
        },
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);
                    $('#txt_url').val(newUrl);
                    $scope.selectedMasterPlanType.image = newUrl;
                }
            });
        },
        
    };
    function getMasterPlanType() {
        var controller = requestApiHelper.MASTER_PLAN_TYPE.ALL;
        baseService.GET(controller).then(function(response){
            $scope.masterplantype = response; 
        }, function(err){
            console.log(err);
        });
    };
}]);