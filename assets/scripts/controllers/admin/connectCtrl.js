angular.module('vpApp').controller('connectCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Trang liên kết/ kết nối',
        child: ''
    };
    $scope.filterLang = 'vi';
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };
    $scope.notice = {
        class: '',
        message: ''
    };
    $scope.connectList = [];
    $scope.selectedConnect = {};
    function init(){
        var controller = requestApiHelper.CONNECT.ALL;
        baseService.GET(controller).then(function(response){
            $scope.connectList = response; 
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    getMasterPlanType();
    init();    
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Sửa thông tin';
            $scope.isUpdate = true;
            $scope.selectedConnect = {};            
            $scope.selectedConnect = item;             
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        add: function(){
            $scope.label.title = 'Thêm mặt bằng';
            $scope.isUpdate = false;
            $scope.selectedConnect = {
                image: '',
                floor_name: '',
                name: '',
                description: '',
                path_name: '',
                sort: 0,
                Status: 1
            };
            $('#img').attr('src', '');
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        submitForm: function(){
            //var noimage = 'assets/includes/upload/images/users/noimage.jpg';<span class="char1">G</span>
            if($scope.Form.$valid){
                if($scope.isUpdate){
                    var controller = requestApiHelper.CONNECT.UPDATE_CONNECT;
                    baseService.POST(controller, $scope.selectedConnect).then(function(response){
                        if(response){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Cập nhật thành công!', 'success');
                        }  
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Cập nhật thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
                else{
                    var controller = requestApiHelper.MASTER_PLAN.ADD_MASTER_PLAN;
                   // $scope.selectedConnect.image = ($scope.selectedConnect.Avatar === '')? noimage : $scope.selectedConnect.image;
                    baseService.POST(controller, $scope.selectedConnect).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Thêm thành công!', 'success');
                        } 
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Thêm thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
            }          
        },
        del: function(){                        
            var controller = requestApiHelper.MASTER_PLAN.DELETE_MASTER_PLAN;
            var param = {id: $scope.selectedConnect.id};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();
                    $('#delAlert').modal('hide');  
                    baseService.showToast('Xóa thành công!', 'success');
                } 
                else{
                    baseService.showToast('Xóa thất bại!', 'danger');
                }
            }, function(err){                
                console.log(err);
            })
        },
        getItem: function(item){
            $scope.selectedConnect = {};                   
            $scope.selectedConnect = item;                     
        },
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);
                    $('#txt_url').val(newUrl);
                    $scope.selectedConnect.image = newUrl;
                }
            });
        },
        
    };
    function getMasterPlanType() {
        var controller = requestApiHelper.MASTER_PLAN_TYPE.ALL;
        baseService.GET(controller).then(function(response){
            $scope.masterplantype = response; 
        }, function(err){
            console.log(err);
        });
    };
}]);