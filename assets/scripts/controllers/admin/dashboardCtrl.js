angular.module('vpApp').controller('dashboardCtrl',['$scope', '$rootScope', 'baseService', 'requestApiHelper', function($scope, $rootScope, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Bảng tin',
        child: '',
        class: 'fa fa-dashboard'
    };    
    if($rootScope.user.IdUser !== undefined)
        $rootScope.dashboardStatistic();    
}]);