angular.module('vpApp').controller('articleCtrl',['$scope', '$rootScope', '$window', '$location', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $location, baseService, requestApiHelper){
    $scope.numberPage = '10';        
    $scope.numOfPage = 5;        
    $scope.filterLang = 'vi';
    $scope.articles = [];
    $scope.selectedArt = {};
    $scope.listToDelete = [];
    var selectedAll = 0;
    var typeLink = $location.path();
    $rootScope.currentPage = {
        parent: 'Article',
        child: '',
        class: 'fa fa-edit'
    };               
    function init(){        
        var controller = requestApiHelper.ARTICLE.GET + '/' + $scope.filterLang;
        var param = {type: 'admin'};
        baseService.POST(controller,param).then(function(response){                           
            $scope.articles = response;                     
        }, function(error){
            console.log(error.message);
        });
    };            
    init();
    $rootScope.getLanguages();                     
    $scope.event = {
        edit: function(item){                        
            $scope.selectedArt = {};
            $scope.selectedArt = item;   
            $rootScope.selectLang = $scope.filterLang;
            $location.path('admin/article/edit/' + item.ArtID);            
        },
        add: function(){                                      
            $rootScope.selectLang = $scope.filterLang;
            $location.path('admin/article/add');            
        },
        del: function(){
            var idPro = $scope.selectedArt.ArtID;
            var controller = requestApiHelper.ARTICLE.DELETE;
            var param = {
                id: idPro,
                user: $rootScope.user.IdUser
            };
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
                else{
                    baseService.showToast('Delete fail!', 'danger');
                }
            }, function(err){
                console.log(err.message);
                baseService.showToast('Delete fail!', 'danger');
            });
        },
        getItem: function(item){
            $scope.selectedArt = {};
            $scope.selectedArt = item;            
        },             
        selectLang: function(code, id){    
            $scope.filterLang = code;
            if(id === 0){                
                init();
                $('#tab0').addClass('active');             
            }
            else{               
                init();
                $('#tab'+id).addClass('active');     
            }
            var checkTabAll = $('#tab0').hasClass('active');
            if(checkTabAll && id !== 0){
                $('#tab0').removeClass('active');
            }
            else{
                for(var i=0;i<$rootScope.languages.length;i++){
                    var checkClass = $('#tab'+$rootScope.languages[i].ID).hasClass('active');
                    var idLang = $rootScope.languages[i].ID;
                    if(checkClass && id !== idLang){
                        $('#tab'+$rootScope.languages[i].ID).removeClass('active');
                        break;
                    }
                }
            }
        },
        checkAll: function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $scope.articles.length; i++){                    
                    $scope.listToDelete.push($scope.articles[i].ArtID);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.length; i++){                    
                    $scope.listToDelete.splice(i, 1);                
                } 
            }
        },
        delAllItem: function(){
            var controller = requestApiHelper.ARTICLE.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    init();
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            })
        }
    };    
}]);