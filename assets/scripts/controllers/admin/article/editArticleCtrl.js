angular.module('vpApp').controller('editArticleCtrl',['$scope', '$rootScope', '$window', '$location', '$routeParams', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $location, $routeParams, baseService, requestApiHelper){
    $scope.meta = $location.path();
    var ArtID = $routeParams.id;
    $scope.options = baseService.ckeditorOptions;
    $scope.selectedArt = {}; 
    var title = 'Article';    
    $rootScope.currentPage = {
        parent:  title,
        child: ($scope.meta.indexOf('edit') !== -1)? 'Edit ' + title : 'Add ' + title,
        class: 'fa fa-edit'
    };   
    $rootScope.dataType = 1;  
    $scope.cateList = [];  
    function init(){        
        if($scope.meta.indexOf('add') !== -1){
            $('#img').attr('src', 'assets/admin/img/no_image.png');                    
            $scope.selectedArt = { 
                CatId: '',
                Author: $rootScope.user.IdUser,
                Username: $rootScope.user.Username,
                DateCreated: baseService.getDatetimeNow(),
                ViewCount: 0,
                ArtLang: $rootScope.selectLang,
                Status: '1',
                Image: 'assets/admin/img/no_image.png'                
            };      
        }
        else{
            var controller = requestApiHelper.ARTICLE.EDIT + '/' + ArtID;
            baseService.GET(controller).then(function(response){
                $scope.selectedArt = response[0]; 
                var meta = $scope.selectedArt.ArtMeta;
                $scope.selectedArt.ArtMeta = meta.substring(0, meta.lastIndexOf('.'));                                             
                if($scope.selectedArt.CatId !== null && $scope.selectedArt.CatId !== '' && $scope.selectedArt.CatId !== undefined){
                    $scope.cateList = $scope.selectedArt.CatId.split(',');
                    $scope.cateList.splice(0,1); $scope.cateList.splice($scope.cateList.length-1,1);                    
                }
                console.log($scope.selectedArt.CatId);
            }, function(err){
                console.log(err);
            });
        }
    };
    init();
    $rootScope.getCategories($rootScope.selectLang, $rootScope.dataType);        
    $rootScope.getLanguages();
    $rootScope.getTemplates();
    $('.collapse-link:not(.binded)').addClass("binded").click(function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });
    $scope.event = {
        submitForm: function(){            
            if($scope.Form.$valid){                                               
                if($scope.cateList.length > 0){
                    $scope.selectedArt.CatId = ',' + $scope.cateList.join(',') + ',';
                }
                if ($scope.selectedArt.Username !== undefined)
                    delete $scope.selectedArt.Username;                  
                if($scope.meta.indexOf('edit') !== -1){
                    var controller = requestApiHelper.ARTICLE.UPDATE;                                        
                    baseService.POST(controller, $scope.selectedArt).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            baseService.showToast('Update successful!', 'success');                       
                        }
                        else{
                            baseService.showToast('Update fail!', 'danger');
                        }
                    }, function(err){                        
                        console.log(err.message);
                        baseService.showToast('Update fail!', 'danger');
                    });
                }
                else{
                    var controller = requestApiHelper.ARTICLE.ADD;                                 
                    baseService.POST(controller, $scope.selectedArt).then(function(response){    
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response.success){                           
                            //init();
                            $location.path('admin/article/edit/'.concat(response.last));
                            baseService.showToast('Post successful!', 'success');                              
                        }
                        else{
                            baseService.showToast('Post fail!', 'danger');
                        }
                    }, function(err){                        
                        console.log(err.message);
                        baseService.showToast('Post fail!', 'danger');
                    }); 
                }
            }
        },        
        selectedChange: function(){            
            var Alias = '';              
            if($scope.selectedArt.ArtName !== undefined || $scope.selectedArt.ArtName !== ''){
                Alias = baseService.getAlias($scope.selectedArt.ArtName);
                $scope.selectedArt.ArtMeta = Alias;
                $scope.selectedArt.SeoTitle = $scope.selectedArt.ArtName;
                $scope.selectedArt.SeoKeyword = $scope.selectedArt.ArtName;
            }            
        },
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);                    
                    $scope.selectedArt.Image = newUrl;
                }                
            });
        }        
    };    
    $scope.$watch('selectedArt.ArtMeta', function(){
        $scope.selectedArt.SeoCanonica = baseService.ROOT_URL.concat($scope.selectedArt.ArtMeta);
    });
}]);