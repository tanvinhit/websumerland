angular.module('vpApp').controller('postalChargesCtrl',['$scope', '$rootScope', '$window', '$location', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $location, baseService, requestApiHelper){
    $scope.numberPage = '10';        
    $scope.numOfPage = 5;
    $scope.postalCharges = [];
    $scope.selectedPostal = {};
    $scope.listToDelete = [];
    var selectedAll = 0;
    $scope.isUpdate = false;
    var typeLink = $location.path();
    $scope.fromPlace = '';
    $scope.toPlace = '';
    $rootScope.currentPage = {
        parent: 'Bảng giá cước',
        child: '',
        class: 'fa fa-edit'
    };     
    $scope.notice = {
        class: '',
        message: ''
    };          
    function init(){        
        var controller = requestApiHelper.POSTAL_CHARGE.GET;
        var param = {type: 'admin'};
        baseService.POST(controller,param).then(function(response){                           
            $scope.postalCharges = response;                     
        }, function(error){
            console.log(error.message);
        });
    };            
    init();                 
    $scope.event = {
        add: function(){
            $scope.isUpdate = false;
            $scope.selectedPostal = {};
            $scope.notice = {
                class: '',
                message: ''
            };
            $scope.fromPlace = '';
            $scope.toPlace = '';
        },
        edit: function(x){
            $scope.isUpdate = true;
            $scope.selectedPostal = {};
            $scope.selectedPostal = x; 
            $scope.fromPlace = $scope.selectedPostal.fromPlace;
            $scope.toPlace = $scope.selectedPostal.toPlace;
            $scope.notice = {
                class: '',
                message: ''
            };
        },
        del: function(){
            var controller = requestApiHelper.POSTAL_CHARGE.DELETE.concat("/", $scope.selectedPostal.priceId);
            baseService.GET(controller).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response == 1){
                    init();
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete successful!', 'success');
                }
                else{
                    baseService.showToast('Delete fail!', 'danger');
                }
            }, function(err){
                console.log(err.message);
                baseService.showToast('Delete fail!', 'danger');
            });
        },
        getItem: function(item){
            $scope.selectedPostal = {};
            $scope.selectedPostal = item;            
        },  
        checkAll: function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $scope.postalCharges.length; i++){                    
                    $scope.listToDelete.push($scope.postalCharges[i].priceId);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.length; i++){                    
                    $scope.listToDelete.splice(i, 1);                
                } 
            }
        },
        delAllItem: function(){
            var controller = requestApiHelper.POSTAL_CHARGE.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    init();
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            });
        },
        submitForm: function(){
            if($scope.isUpdate){
                let controller = requestApiHelper.POSTAL_CHARGE.UPDATE;                
                baseService.POST(controller, $scope.selectedPostal).then((response) => {
                    if(response.redirect !== undefined){
                        $window.location.href = response.redirect;
                    }
                    else if(response == 1){
                        init();
                        $('#myModal').modal('hide');  
                        baseService.showToast('Cập nhật thành công!', 'success');
                    }  
                    else{
                        $scope.notice.class = 'text-danger';
                        $scope.notice.message = 'Cập nhật thất bại!';
                    }
                }, (err) => {
                    console.log(err.message);
                    baseService.showToast('Xảy ra lỗi!', 'danger');
                });
            }
            else{
                let controller = requestApiHelper.POSTAL_CHARGE.INSERT;                
                baseService.POST(controller, $scope.selectedPostal).then((response) => {
                    if(response.redirect !== undefined){
                        $window.location.href = response.redirect;
                    }
                    else if(response.success){
                        init();
                        $('#myModal').modal('hide');  
                        baseService.showToast('Thêm thành công!', 'success');
                    }  
                    else{
                        $scope.notice.class = 'text-danger';
                        $scope.notice.message = 'Cập nhật thất bại!';
                    }
                }, (err) => {
                    console.log(err.message);
                    baseService.showToast('Xảy ra lỗi!', 'danger');
                });
            }
        }
    };  
    
    $scope.getDistrictFromById = (districtId) => {
        if(!!districtId){
            var controller = requestApiHelper.ADDRESS.GET_DISTRICTS_BY_ID;
            baseService.GET(controller.concat('/', districtId)).then(function(response) {
                if(!!response && response.length > 0){
                    let district = response[0];
                    $scope.fromPlace = (district.TEN_TINH + ', ' + district.TEN_QUANHUYEN);
                    $scope.selectedPostal.fromPlace = districtId;
                }
                else
                    $scope.selectedPostal.fromPlace = districtId;
            }, function(err){
                console.log(err);
            });
        }
    }

    $scope.getDistrictToById = (districtId) => {
        if(!!districtId){
            var controller = requestApiHelper.ADDRESS.GET_DISTRICTS_BY_ID;
            baseService.GET(controller.concat('/', districtId)).then(function(response) {
                if(!!response && response.length > 0){
                    let district = response[0];
                    $scope.toPlace = (district.TEN_TINH + ', ' + district.TEN_QUANHUYEN);                    
                    $scope.selectedPostal.toPlace = districtId;
                }
                else
                    $scope.selectedPostal.toPlace = districtId;
            }, function(err){
                console.log(err);
            });
        }
    }

    /* Import Excel */
	$scope.read = (file) => {
		if (!!file.Sheet1 && file.Sheet1.length > 0) {
			let data = [];
            let api = requestApiHelper.POSTAL_CHARGE.CREATE_MANY;
            let i = 0;
			file.Sheet1.forEach(item => {
                if(i > 0){
                    let obj = {
                        fromPlace: !!item[0] ? item[0] : "",
                        toPlace: !!item[1] ? item[1] : "",
                        service: !!item[2] ? item[2] : "",
                        dcpWeight: !!item[3] ? item[3] : "",
                        dcpToWeight: !!item[4] ? item[4] : "",
                        dcpPrice: !!item[5] ? item[5] : ""
                    };

                    data.push(obj);
                }
                i++;
			});
			baseService.POST(api, data).then(rsp => {
				if (rsp.success) {
                    baseService.showToast("Nhập file excel thành công!", "success");	
                    init();				
				}
				else {
					baseService.showToast("Nhập file excel không thành công!", "warning");					
                }
			}).catch(ex => {
				console.log(ex);
			});
		}

		$scope.$apply();
	};

}]);