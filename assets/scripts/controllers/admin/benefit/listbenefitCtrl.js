angular.module('vpApp').controller('listbenefitCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', 
    function($scope, $rootScope, $window, baseService, requestApiHelper){
    $rootScope.currentPage = {
        parent: 'Danh sách lợi ích',
        child: ''
    };
    $scope.filterLang = 'vi';
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };
    $scope.notice = {
        class: '',
        message: ''
    };
    $scope.benefitList = [];
    $scope.selectedBenefitList = {};
    function init(){
        var controller = requestApiHelper.BENEFIT.GETALL;
        baseService.GET(controller).then(function(response){
            console.log(response);
            $scope.benefitList = response; 
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    init();
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Sửa thông tin';
            $scope.isUpdate = true;
            $scope.selectedBenefitList = {};            
            $scope.selectedBenefitList = item;             
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        add: function(){
            $scope.label.title = 'Thêm thông tin';
            $scope.isUpdate = false;
            $scope.selectedBenefitList = {
                title: "",
                icon: "",
                content: ""
            };
            $scope.notice.class = '';
            $scope.notice.message = '';  
        },
        submitForm: function(){
            if($scope.Form.$valid){
                if($scope.isUpdate){
                    var controller = requestApiHelper.BENEFIT.UPDATE;
                    baseService.POST(controller, $scope.selectedBenefitList).then(function(response){
                        if(response){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Cập nhật thành công!', 'success');
                        }  
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Cập nhật thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
                else{
                    var controller = requestApiHelper.BENEFIT.ADD;
                    baseService.POST(controller, $scope.selectedBenefitList).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Thêm thành công!', 'success');
                        } 
                        else{
                            $scope.notice.class = 'text-danger';
                            $scope.notice.message = 'Thêm thất bại!';
                        }
                    }, function(err){                        
                        console.log(err);
                    });
                }
            }          
        },
        del: function(){                        
            var controller = requestApiHelper.BENEFIT.DELETE;
            var param = {id: $scope.selectedBenefitList.id};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response){
                    init();
                    $('#delAlert').modal('hide');  
                    baseService.showToast('Xóa thành công!', 'success');
                } 
                else{
                    baseService.showToast('Xóa thất bại!', 'danger');
                }
            }, function(err){                
                console.log(err);
            })
        },
        getItem: function(item){
            $scope.selectedBenefitList = {};                   
            $scope.selectedBenefitList = item;                     
        },
        browserBack: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);
                    $('#txt_url').val(newUrl);
                    $scope.selectedBenefitList.icon = newUrl;
                }
            });
        },
    };
}]);