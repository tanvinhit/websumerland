angular.module('vpApp').controller('slideCtrl',['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', '$routeParams', function($scope, $rootScope, $window, baseService, requestApiHelper, $routeParams){
    $scope.slideCateId = $routeParams.cateId;
    $rootScope.currentPage = {
        parent: 'Slide',
        child: '',
        class: 'fa fa-sliders'
    };
    $scope.typeArticle = {
        product: 2,
        article: 1
    };
    $scope.slideList = [];
    $scope.filterLang = 'vi';
    $scope.isUpdate = false;
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.label = {
        title: ''        
    };
    $scope.notice = {};
    $scope.selectedSlide = {};
    $scope.listToDelete = [];
    var selectedAll = 0;
    function init(){
        $scope.notice = {
            class: '',
            message: ''
        };
        var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + $scope.slideCateId;
        var param = {type: 'admin'};
        baseService.POST(controller, param).then(function(response){
            $scope.slideList = response; 
            console.log($scope.slideList);
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    init();
    $rootScope.getCategories($scope.filterLang, $scope.typeArticle.product);
    $rootScope.getArticles($scope.filterLang, $scope.typeArticle.product);
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Edit slide';
            $scope.isUpdate = true;
            $scope.selectedSlide = {};       
            $scope.selectedSlide = item;
            $scope.selectedSlide.CateId = $scope.slideCateId;
        },
        add: function(){
            $scope.label.title = 'Add slide';
            $scope.isUpdate = false;
            $scope.selectedSlide = {
                ProLink: '0',
                CateLink: '0',
                sort: 0,
                Language: $scope.filterLang,
                CateId: $scope.slideCateId
            };            
            $('#img').attr('src', '');
        },
        submitForm: function(){
            if($scope.Form.$valid){
                if($scope.isUpdate){
                    var controller = requestApiHelper.SLIDE.UPDATE;
                    $scope.selectedSlide.Language = $scope.filterLang;
                    $scope.selectedSlide.sort = parseInt($scope.selectedSlide.sort);
                    baseService.POST(controller, $scope.selectedSlide).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Update successful!', 'success');
                        }                        
                    }, function(err){
                        $scope.notice.class = 'text-danger';
                        $scope.notice.message = 'Update fail!';
                        console.log(err);
                    });
                }
                else{
                    var controller = requestApiHelper.SLIDE.ADD;
                    $scope.selectedSlide.Language = $scope.filterLang;
                    baseService.POST(controller, $scope.selectedSlide).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Add successful!', 'success');
                        }                        
                    }, function(err){
                        $scope.notice.class = 'text-danger';
                        $scope.notice.message = 'Add fail!';
                        console.log(err);
                    });
                }
            }          
        },
        del: function(){                        
            var controller = requestApiHelper.SLIDE.DELETE;
            var param = {ID: $scope.selectedSlide.ID};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();
                    $('#delAlert').modal('hide');  
                    baseService.showToast('Delete successful!', 'success');
                }                        
            }, function(err){
                baseService.showToast('Delete fail!', 'danger');
                console.log(err);
            })
        },
        getItem: function(item){
            $scope.selectedSlide = {};                   
            $scope.selectedSlide = item;                     
        },        
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);
                    $('#txt_url').val(newUrl);
                    $scope.selectedSlide.Image = newUrl;
                }
            });
        },
        selectLang: function (code, id) {
            $scope.filterLang = code;            
            init();
            $('#tab-' + code).addClass('active');                                    
            for (var i = 0; i < $rootScope.languages.length; i++) {
                var checkClass = $('#tab-' + $rootScope.languages[i].Code).hasClass('active');
                var idLang = $rootScope.languages[i].ID;
                if (checkClass && id !== idLang) {
                    $('#tab-' + $rootScope.languages[i].Code).removeClass('active');
                    break;
                }
            }            
        },
        checkAll: function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $scope.slideList.length; i++){                   
                    $scope.listToDelete.push($scope.slideList[i].ID);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.length; i++){                    
                    $scope.listToDelete.splice(i, 1);                
                } 
            }
        },
        delAllItem: function(){
            var controller = requestApiHelper.SLIDE.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    init();
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            })
        }
    };
}]); 