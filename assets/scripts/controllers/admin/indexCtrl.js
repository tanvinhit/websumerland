angular.module('vpApp').controller('adminCtrl',['$scope', '$rootScope', '$window', '$sce', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $sce, baseService, requestApiHelper){    
    $rootScope.user = {};
    $rootScope.trustAsHtml = $sce.trustAsHtml;
    $rootScope.languages = [];
    $rootScope.templates = [];
    $rootScope.categories = [];
    $rootScope.currentPage = {
        parent: '',
        child: ''
    };   
    $rootScope.selectLang = 'vi';   
    $rootScope.mapList = [];    
    $rootScope.mailList = [];
    $rootScope.dashboard = [];
    $rootScope.dataType = 0;
    $scope.selectedMail = {};    
    $rootScope.coordinate = {
        lat: 0,
        lng: 0
    };
    $rootScope.provinces = [];
	$rootScope.districts = [];
	$rootScope.wards = [];
    var childActive = false;
    function init(){
        var controller = requestApiHelper.USER.CURRENT;
        baseService.GET(controller).then(function(response){
            if(response.redirect === undefined){
                $rootScope.user = response;  
                console.log($rootScope.user);
                $rootScope.dashboardStatistic();                
            }
            else{
                $window.location.url = response.redirect;
            }
        }, function(err){
            console.log(err);
        }); 
    }; 
    $rootScope.dashboardStatistic = function(){
        var controller = requestApiHelper.DASHBOARD;
        var param = {UserId: $rootScope.user.IdUser};
        baseService.POST(controller, param).then(function(response){
            $rootScope.dashboard = response;            
        }, function(err){
            console.log(err);
        });  
    };
    $rootScope.getMailList = function(){
        var controller = requestApiHelper.CONTACT.GET;
        baseService.GET(controller).then(function(response){
           $rootScope.mailList = response; 
        }, function(err){
            console.log(err);
        });
    };
    $rootScope.getMapList = function(){        
        var controller = requestApiHelper.MAP.GET;
        baseService.GET(controller).then(function(response){
            $rootScope.mapList = response;
            var i = 0;
            var count = 0;
            var avgLat = 0;
            var avgLng = 0;
            for(i = 0; i < $rootScope.mapList.length; i++){
                count++;
                avgLat += parseFloat($rootScope.mapList[i].Lattitude);
                avgLng += parseFloat($rootScope.mapList[i].Longitude);
                $rootScope.mapList[i].Title = 'Địa chỉ ' + (i + 1);
            }
            $rootScope.coordinate.lat = avgLat / count;
            $rootScope.coordinate.lng = avgLng / count;
        }, function(err){
            console.log(err);
        });    
    };
    $rootScope.getCategories = function(lang, type){
        var controller = requestApiHelper.CATEGORY.GET + '/' + lang;
        var param = {type: 'admin', dataType: type};
        baseService.POST(controller,param).then(function(response){                           
            $rootScope.categories = response;                      
        }, function(error){
            console.log(error.message);
        });
    };    
    $rootScope.getAllCategories = function(lang){
        var controller = requestApiHelper.CATEGORY.ALL + '/' + lang;
        var param = {type: 'admin'};
        baseService.POST(controller,param).then(function(response){                           
            $rootScope.categories = response;                      
        }, function(error){
            console.log(error.message);
        });
    };
    $rootScope.getLanguages = function(){
        var controller = requestApiHelper.LANGUAGE.GET;
        baseService.GET(controller).then(function(response){            
            $rootScope.languages = response;                            
        }, function(err){
            console.log(err); 
        });
    };
    $rootScope.getTemplates = function(){
        var controller = requestApiHelper.TEMPLATE.GET;        
        baseService.POST(controller).then(function(response){            
            $rootScope.templates = response;                      
        }, function(err){
            console.log(err); 
        });  
    };
    $rootScope.getAllArticles = function(lang){
        var controller = requestApiHelper.ARTICLE.ALL + '/' + lang;
        var param = {type: 'admin'};
        baseService.POST(controller,param).then(function(response){                           
            $rootScope.articles = response;                      
        }, function(error){
            console.log(error.message);
        });  
    };
    $rootScope.getArticles = function(lang, type){
        var controller = requestApiHelper.ARTICLE.GET + '/' + lang;
        var param = {type: 'admin', dataType: type};
        baseService.POST(controller,param).then(function(response){                           
            $rootScope.articles = response;                      
        }, function(error){
            console.log(error.message);
        });  
    };    
    init();   
    $rootScope.getMailList();
    $scope.listMenu = [
        {class: 'fa fa-dashboard fa-fw', text: 'Dashboard', link: 'admin/dashboard', id: 0},                      
        {class: 'fa fa-desktop fa-fw', text: 'Display', link: '', id: 1}, 
        {class: 'fa fa-files-o fa-fw', text: 'Article', link: '', id: 2},
        {class: 'fa fa-shopping-cart fa-fw', text: 'Product', link: '', id: 3},               
        {class: 'fa fa-envelope fa-fw', text: 'Mail', link: 'admin/contactmail', id: 4},        
        {class: 'fa fa-folder-o fa-fw', text: 'Resource', link: 'admin/resources', id: 5},
        {class: 'fa fa-map-marker fa-fw', text: 'Address', link: 'admin/address', id: 6},
        {class: 'fa fa-language fa-fw', text: 'Language', link: 'admin/languages', id: 7},
        {class: 'fa fa-user fa-fw', text: 'Member', link: 'admin/users', id: 8},
        {class: 'fa fa-briefcase fa-fw', text: 'Customers', link: 'admin/customer', id: 9},
        {class: 'fa fa-thumbs-up fa-fw', text: 'Services', link: 'admin/service', id: 9},        
        {class: 'fa fa-clipboard fa-fw', text: 'Templates', link: 'admin/templates', id: 12}
    ];    
    $scope.childMenu = [
        {class: 'fa fa-list-alt fa-fw', text: 'Category', link: 'admin/categories', parent: 2, id: 0},
        {class: 'fa fa-edit fa-fw', text: 'Article List', link: 'admin/article', parent: 2, id: 1},
        {class: 'fa fa-list-alt fa-fw', text: 'Category', link: 'admin/productCategories', parent: 3, id: 2},
        {class: 'fa fa-edit fa-fw', text: 'Product List', link: 'admin/product', parent: 3, id: 3},        
        {class: 'fa fa-flask fa-fw', text: 'Widgets', link: 'admin/widgets', parent: 1, id: 4},
        {class: 'fa fa-sitemap fa-fw', text: 'Menu', link: 'admin/menu', parent: 1, id: 5},
        {class: 'fa fa-sliders fa-fw', text: 'Slideshow', link: 'admin/slide', parent: 1, id: 6}    
    ];   
    $scope.funcOfUser = [
        {class: 'fa fa-user fa-fw', text: 'Profile', link: 'admin/profile'},
        {class: 'fa fa-calendar fa-fw', text: 'Calendar', link: 'admin/calendar'},
        {class: 'fa fa-sign-out fa-fw', text: 'Logout', link: 'login'}        
    ];  
    $rootScope.getProvinces = function(){
		var controller = requestApiHelper.ADDRESS.GET_PROVINCES;
        baseService.GET(controller).then(function(response) {
            $rootScope.provinces = response;
        }, function(err){
            console.log(err);
        });
	};
	$rootScope.getDistricts = function(provinceId){
        if(!!provinceId){
            var controller = requestApiHelper.ADDRESS.GET_DISTRICTS_BY_PROVINCE_ID;
            baseService.GET(controller.concat('/', provinceId)).then(function(response) {
                $rootScope.districts = response;
            }, function(err){
                console.log(err);
            });
        }
    };
	$rootScope.getWards = function(districtId){
        if(!!districtId){
            var controller = requestApiHelper.ADDRESS.GET_WARDS_BY_DISTRICT_ID;
            baseService.GET(controller.concat('/', districtId)).then(function(response) {
                $rootScope.wards = response;
            }, function(err){
                console.log(err);
            });
        }
	};  
    $scope.event = {        
        navigationPage: function(url){                                    
            var controller = requestApiHelper.USER.LOGOUT;
            baseService.GET(controller).then(function (response){
                $window.location.href = 'login';                 
            }, function (error){
                console.log(error);
            });
        },        
        view: function(item){
            $rootScope.selectedMail = {};
            $rootScope.selectedMail = item;
            $('#Modal').modal('show');
        },
        activeParentMenu: function(id){   
            if(!childActive){
                var checkLi = {};
                for(var i = 0; i < $scope.listMenu.length; i++){
                    checkLi = $('#parent' + $scope.listMenu[i].id);
                    if(checkLi.hasClass('active'))
                        checkLi.removeClass('active');
                    else{
                        checkLi.removeClass('active');
                        checkLi.removeClass('open');
                    }
                }            
                var li = $('#' + id);
                li.addClass('active');            
            }
            else{                
                childActive = false;                 
            }
        },
        activeChildMenu: function(id){
            childActive = true;
            var childLi = {};            
            for(var j = 0; j < $scope.childMenu.length; j++){
                childLi = $('#child' + $scope.childMenu[j].id);
                if(childLi.hasClass('active')){
                    childLi.removeClass('active');
                    break;
                }
            }            
            $('#' + id).addClass('active');            
        }
    };          
    $window.onbeforeunload = function(event){                
        var controller = requestApiHelper.USER.DESTROY_SESSION;
        baseService.GetNotAsync(controller);        
    };        
}]);