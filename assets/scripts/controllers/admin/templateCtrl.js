angular.module('vpApp').controller('templateCtrl',['$scope', '$rootScope', 'baseService', '$window', 'requestApiHelper', function($scope, $rootScope, baseService, $window, requestApiHelper){
    $scope.numberPage = '10';
    $scope.numOfPage = 5;      
    $scope.selectedTemp = {};
    $scope.newTemp = {};
    $scope.notice = {}; 
    $rootScope.currentPage = {
        parent: 'Template',
        child: '',
        class: 'fa fa-clipboard'
    };  
    $scope.listToDelete = [];
    var selectedAll = 0;
    function init(){
        $scope.newTemp = {
            Filename: '',
            Status: '1',
            Type: ''
        };  
    };
    init();
    $rootScope.getTemplates(); 
    $rootScope.getLanguages();
    $('.collapse-link:not(.binded)').addClass("binded").click( function() {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });
    $scope.event = {
        edit: function(item){            
            $scope.selectedTemp = {};            
            $scope.selectedTemp = item;   
            $scope.notice = {
                class: '',
                message: ''
            }; 
        },        
        submitForm: function(isUpdate){            
            if(isUpdate){ 
                if($scope.Form.$valid){
                    var controller = requestApiHelper.TEMPLATE.UPDATE; 
                    if($scope.selectedTemp.$$hashKey)
                        delete $scope.selectedTemp.$$hashKey;
                    baseService.POST(controller, $scope.selectedTemp).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            $rootScope.getTemplates();
                            $('#myModal').modal('hide');
                            baseService.showToast('Update is successful!', 'success');
                        }
                        else{
                            $scope.notice.class = 'left text-danger';
                            $scope.notice.message = 'Update is not successful!';
                        }
                    }, function(err){
                        console.log(err.message);
                    });
                }
            }
            else{ 
                if($scope.addForm.$valid){
                    var controller = requestApiHelper.TEMPLATE.ADD;
                    baseService.POST(controller, $scope.newTemp).then(function(response){    
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            $rootScope.getTemplates();        
                            init();
                            baseService.showToast('Add is successful!', 'success');
                        }  
                        else{                            
                            baseService.showToast('Add is not successful!', 'danger');
                        }
                    }, function(err){
                        console.log(err.message);
                    });
                }
            }            
        },
        del: function(){                        
            var controller = requestApiHelper.TEMPLATE.DELETE;
            var id = $scope.selectedTemp.TempID;
            var param = {ID : id};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    $rootScope.getTemplates();                  
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete is successful!', 'success');
                }
                else{
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete is not successful!', 'danger');
                }
            }, function(err){
                console.log(err.message);
            });
        },
        getItem: function(item){
            $scope.selectedTemp = {};
            $scope.selectedTemp = item;            
        },
        checkAll: function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $rootScope.templates.length; i++){                    
                    $scope.listToDelete.push($rootScope.templates[i].TempID);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.length; i++){                    
                    $scope.listToDelete.splice(i, 1);                
                } 
            }
        },
        delAllItem: function(){
            var controller = requestApiHelper.TEMPLATE.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    $rootScope.getTemplates(); 
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            })
        },
        meta_Focus: function(isUpdate){
            if(!isUpdate){
                if($scope.newTemp.Type === 'page'){
                    $scope.newTemp.Meta = baseService.getAlias($scope.newTemp.TempName);
                    $scope.newTemp.SeoCanonica = baseService.ROOT_URL.concat($scope.newTemp.Meta);
                }
            }
            else{
                if($scope.selectedTemp.Type === 'page'){
                    $scope.selectedTemp.Meta = baseService.getAlias($scope.selectedTemp.TempName);
                    $scope.selectedTemp.SeoCanonica = baseService.ROOT_URL.concat($scope.selectedTemp.Meta);
                }
            }
        },
        type_HasChanged: function(isUpdate){
            if(!isUpdate){
                if($scope.newTemp.Type === 'page'){
                    $scope.newTemp.SeoTitle = $scope.newTemp.TempName;
                    $scope.newTemp.SeoKeyword = $scope.newTemp.TempName;
                }
            }
            else{
                if($scope.selectedTemp.Type === 'page'){
                    $scope.selectedTemp.SeoTitle = $scope.selectedTemp.TempName;
                    $scope.selectedTemp.SeoKeyword = $scope.selectedTemp.TempName;
                }
            }
        }
    };
}]);