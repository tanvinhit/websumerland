angular.module('vpApp').controller('chtiCateSlideCtrl', ['$scope', '$rootScope', '$window', 'baseService', 'requestApiHelper', function ($scope, $rootScope, $window, baseService, requestApiHelper) {
    $scope.numberPage = '10';
    $scope.numOfPage = 5;
    $scope.selectedCustomer = {};
    $scope.filterLang = 'vi';
    $scope.notice = {};
    $scope.listCate = [];
    var selectedAll = 0;
    $scope.listToDelete = {
        listCatId: []
    }
    $scope.isUpdate = false;
    $rootScope.currentPage = {
        parent: 'Tiện ích',
        child: ''
    }
    
    $scope.category = {
        CatID: 0,
        CatName: '',
        CatMeta: 'ch-tien-ich',                
        CatDescribes: '',                 
        CatLang: $scope.filterLang,
        CatParent: '0',
        Image: '',
        SeoTitle: '',
        SeoKeyword: '',
        SeoDescribes: '',
        SeoCanonica: '',
        MetaRobot: '',
        Type: 6,
        Status: '1'
    }

    function init() {
        $scope.cate = {
            Language: $scope.filterLang
        };
        $scope.notice = {
            class: '',
            message: ''
        };
        var controller = requestApiHelper.CATEGORY.GET + '/' + $scope.filterLang;
        var data = {
            type: 'admin',
            dataType: 6
        };
        baseService.POST(controller, data).then(function (response) {
            $scope.listCate = response;
        }, function (err) {
            console.log(err);
        });
    };
    $rootScope.getLanguages();
    init();
    
    
    $scope.SubmitForm = function(){
        $scope.category.CatName = $scope.cateSlideName;
        $scope.category.Image = $scope.Image;
        if($scope.isUpdate){
            var controller = requestApiHelper.CATEGORY.UPDATE;
            baseService.POST(controller, $scope.category).then(function (response) {
                if(response === 'true'){
                    $scope.listCate = response;
                    baseService.showToast('Update successful!', 'success');
                    init();
                    $('.modal').hide();
                }
                else baseService.showToast('Update fail!', 'danger');
            }, function (err) {
                console.log(err);
            });
        }else{
            var controller = requestApiHelper.CATEGORY.ADD;
            baseService.POST(controller, $scope.category).then(function (response) {
                if(response === 'true'){
                    $scope.listCate = response;
                    baseService.showToast('Insert successful!', 'success');
                    init();
                    $('.modal').hide();
                }
                else baseService.showToast('Insert fail!', 'danger');
            }, function (err) {
                console.log(err);
            });
        }
    }
    $scope.Read = function(item){
        $scope.category = item;
        $scope.isUpdate = true;
        $scope.frmTitle = 'Cập nhật tiện ích';
        $scope.cateSlideName = item.CatName;
        $scope.Image = item.Image;
        $('#addModal').modal('show');
    };
    $scope.getItem = function(item){
        $scope.category = item;
    };
    $scope.addModal = function(){
        $scope.isUpdate = false;
        $scope.frmTitle = 'Thêm mới tiện ích';
        $scope.cateSlideName = '';
        $scope.code = 'tien-ich';
    };
    
    $scope.delete = function(){
        var controller = requestApiHelper.CATEGORY.DELETE;
        let param = {
            cateId: $scope.category.CatID
        }
        baseService.POST(controller, param).then(function (response) {
            if(response === 'true'){
                baseService.showToast('Delete successful!', 'success');
                init();
                $('.modal').hide();
            }
            else baseService.showToast('Delete fail!', 'danger');
        }, function (err) {
            console.log(err);
        });
    }
    $scope.checkAll = function(){
        if(selectedAll === 0){
            selectedAll = 1;
            for(var i = 0; i < $scope.listCate.length; i++){                    
                $scope.listToDelete.listCatId.push($scope.listCate[i].CatID);                
            }            
        }
        else{
            selectedAll = 0;
            for(var i = 0; i < $scope.listToDelete.listCatId.length; i++){                    
                $scope.listToDelete.listCatId.splice(i, 1);                
            } 
        }
    };
    $scope.deleteMulti = function(){
        var controller = requestApiHelper.CATEGORY.DEL_MULTI;
        baseService.POST(controller, $scope.listToDelete.listCatId).then(function (response) {
            if(response === 'true'){
                baseService.showToast('Delete successful!', 'success');
                init();
                $('.modal').hide();
            }
            else baseService.showToast('Delete fail!', 'danger');
        }, function (err) {
            console.log(err);
        });
    };
    $scope.selectLang = function(code, id){    
        $scope.filterLang = code;            
        if(id === 0){                
            init();
            $('#tab0').addClass('active');             
        }
        else{               
            init();
            $('#tab'+id).addClass('active');     
        }
        var checkTabAll = $('#tab0').hasClass('active');
        if(checkTabAll && id !== 0){
            $('#tab0').removeClass('active');
        }
        else{
            for(var i=0;i<$rootScope.languages.length;i++){
                var checkClass = $('#tab'+$rootScope.languages[i].ID).hasClass('active');
                var idLang = $rootScope.languages[i].ID;
                if(checkClass && id !== idLang){
                    $('#tab'+$rootScope.languages[i].ID).removeClass('active');
                    break;
                }
            }
        }
    };
    $scope.event = {
        edit: function(item){
            $scope.label.title = 'Edit slide';
            $scope.isUpdate = true;
            $scope.selectedSlide = {};            
            $scope.selectedSlide = item;
            $scope.selectedSlide.CateId = $scope.slideCateId;
        },
        add: function(){
            $scope.label.title = 'Add slide';
            $scope.isUpdate = false;
            $scope.selectedSlide = {
                ProLink: '0',
                CateLink: '0',
                sort: 0,
                Language: $scope.filterLang,
                CateId: $scope.slideCateId
            };            
            $('#img').attr('src', '');
        },
        submitForm: function(){
            if($scope.Form.$valid){
                if($scope.isUpdate){
                    var controller = requestApiHelper.SLIDE.UPDATE;
                    $scope.selectedSlide.Language = $scope.filterLang;
                    baseService.POST(controller, $scope.selectedSlide).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Update successful!', 'success');
                        }                        
                    }, function(err){
                        $scope.notice.class = 'text-danger';
                        $scope.notice.message = 'Update fail!';
                        console.log(err);
                    });
                }
                else{
                    var controller = requestApiHelper.SLIDE.ADD;
                    $scope.selectedSlide.Language = $scope.filterLang;
                    delete $scope.selectedSlide.ParentName;
                    baseService.POST(controller, $scope.selectedSlide).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init();
                            $('#myModal').modal('hide');  
                            baseService.showToast('Add successful!', 'success');
                        }                        
                    }, function(err){
                        $scope.notice.class = 'text-danger';
                        $scope.notice.message = 'Add fail!';
                        console.log(err);
                    });
                }
            }          
        },
        del: function(){                        
            var controller = requestApiHelper.SLIDE.DELETE;
            var param = {ID: $scope.selectedSlide.ID};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    init();
                    $('#delAlert').modal('hide');  
                    baseService.showToast('Delete successful!', 'success');
                }                        
            }, function(err){
                baseService.showToast('Delete fail!', 'danger');
                console.log(err);
            })
        },
        getItem: function(item){
            $scope.selectedSlide = {};                   
            $scope.selectedSlide = item;                     
        },        
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);
                    $('#txt_url').val(newUrl);
                    $scope.Image = newUrl;
                }
            });
        },
        selectLang: function (code, id) {
            $scope.filterLang = code;            
            init();
            $('#tab-' + code).addClass('active');                                    
            for (var i = 0; i < $rootScope.languages.length; i++) {
                var checkClass = $('#tab-' + $rootScope.languages[i].Code).hasClass('active');
                var idLang = $rootScope.languages[i].ID;
                if (checkClass && id !== idLang) {
                    $('#tab-' + $rootScope.languages[i].Code).removeClass('active');
                    break;
                }
            }            
        },
        checkAll: function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $scope.slideList.length; i++){                   
                    $scope.listToDelete.push($scope.slideList[i].ID);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.length; i++){                    
                    $scope.listToDelete.splice(i, 1);                
                } 
            }
        },
        delAllItem: function(){
            var controller = requestApiHelper.SLIDE.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    init();
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            })
        }
    };
}]);