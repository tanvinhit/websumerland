angular.module('vpApp').controller('languageCtrl',['$scope', '$rootScope', 'baseService', '$window', 'requestApiHelper', function($scope, $rootScope, baseService, $window, requestApiHelper){
    $scope.numberPage = '10';
    $scope.numOfPage = 5;     
    $scope.selectedLang = {};
    $scope.newLang = {};
    $scope.notice = {};
    $scope.listToDelete = [];
    var selectedAll = 0;
    $rootScope.currentPage = {
        parent: 'Language',
        child: '',
        class: 'fa fa-language'
    };       
    function init(){
        $scope.newLang = {
            Icon: '',
            Status: '1' 
        };  
        $scope.notice = {
            class: '',
            message: ''
        }; 
    };
    init();
    $rootScope.getLanguages();
    $('.collapse-link:not(.binded)').addClass("binded").click( function() {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });
    $scope.event = {
        edit: function(item){           
            $scope.selectedLang = {};            
            $scope.selectedLang = item;           
        },        
        submitForm: function(isUpdate){            
            if(isUpdate){ 
                if($scope.Form.$valid){
                    var controller = requestApiHelper.LANGUAGE.UPDATE;  
                    if($scope.selectedLang.$$hashKey !== undefined)
                        delete $scope.selectedLang.$$hashKey;
                    baseService.POST(controller, $scope.selectedLang).then(function(response){
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            init();
                            $rootScope.getLanguages();
                            $('#myModal').modal('hide');
                            baseService.showToast('Update is successful!', 'success');
                        }
                        else{
                            $scope.notice.class = 'left text-danger';
                            $scope.notice.message = 'Update is fail!';
                        }
                    }, function(err){
                        $scope.notice.class = 'left text-danger';
                        $scope.notice.message = 'Update is fail!';
                        console.log(err.message);
                    });
                }
            }
            else{ 
                if($scope.addForm.$valid){
                    var controller = requestApiHelper.LANGUAGE.ADD;
                    baseService.POST(controller, $scope.newLang).then(function(response){    
                        if(response.redirect !== undefined){
                            $window.location.href = response.redirect;
                        }
                        else if(response === 'true'){
                            $rootScope.getLanguages();       
                            init();
                            baseService.showToast('Add is successful!', 'success');
                        }  
                        else{                            
                            baseService.showToast('Add is fail!', 'danger');
                        }
                    }, function(err){
                        baseService.showToast('Add is fail!', 'danger');
                        console.log(err.message);
                    });
                }
            }            
        },
        del: function(){                        
            var controller = requestApiHelper.LANGUAGE.DELETE;
            var id = $scope.selectedLang.ID;
            var param = {ID : id};
            baseService.POST(controller, param).then(function(response){
                if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else if(response === 'true'){
                    $rootScope.getLanguages(); 
                    init();
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete is successful!', 'success');
                }
                else{
                    $('#delAlert').modal('hide');
                    baseService.showToast('Delete is fail!', 'danger');
                }
            }, function(err){
                console.log(err.message);
                baseService.showToast('Delete is fail!', 'danger');
            });
        },
        getItem: function(item){
            $scope.selectedLang = {};
            $scope.selectedLang = item;            
        },        
        browserImg: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#img').attr('src',url);
                    $('#txt_url').val(newUrl);
                    $scope.selectedLang.Icon = newUrl;
                }
            });
        },
        browserEditImg: function(){
            CKFinder.popup({
                chooseFiles: true,  
                selectActionFunction: function(url){
                    var newUrl = baseService.getImageUrl(url);
                    $('#imgEdit').attr('src',url);
                    $('#txt_editUrl').val(newUrl);
                    $scope.newLang.Icon = newUrl;
                }
            });
        },
        checkAll: function(){
            if(selectedAll === 0){
                selectedAll = 1;
                for(var i = 0; i < $rootScope.languages.length; i++){                    
                    $scope.listToDelete.push($rootScope.languages[i].ID);                
                }            
            }
            else{
                selectedAll = 0;
                for(var i = 0; i < $scope.listToDelete.length; i++){                    
                    $scope.listToDelete.splice(i, 1);                
                } 
            }
        },
        delAllItem: function(){
            var controller = requestApiHelper.LANGUAGE.DEL_MULTI;
            baseService.POST(controller, $scope.listToDelete).then(function(response){
                if(response === 'true'){
                    baseService.showToast('Xóa nhiều mục thành công!', 'success');
                    $('#delAllAlert').modal('hide');
                    $rootScope.getLanguages();
                }
                else if(response.redirect !== undefined){
                    $window.location.href = response.redirect;
                }
                else{
                    baseService.showToast('Xóa nhiều mục thất bại!', 'danger');
                }
            }, function(err){
                baseService.showToast(err.data.message, 'danger');
            })
        }
    };
}]);