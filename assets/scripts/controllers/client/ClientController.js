angular.module('ClientApp')
    .controller('ClientController', function ($scope, $location, $rootScope, $sce, $compile, $routeParams, MyFactory) {
        $scope.langID = 1;
        $rootScope.language = {};
        $rootScope.banner = {};
        $rootScope.LIST_MENU = [];
        $scope.LIST_WIDGET_SIDEBAR_RIGHT = [];
        $scope.txt_search = '';
        if ($location.path() === '/en')
            $scope.langID = 2;
        MyFactory.GET(MyFactory.HOST_NAME + MyFactory.API.LANG + '/'+$scope.langID).then(function (respone) {
            $rootScope.language = respone;
            getMenuForClient();
        });
        
        MyFactory.GET(MyFactory.HOST_NAME + MyFactory.API.BANNER).then(function (respone) {
            $rootScope.banner = respone.banner;
        });
    
        MyFactory.GET(MyFactory.HOST_NAME + MyFactory.API.WIDGETS + '/sidebarRight/' + $scope.langID).then(function (respone) {
            if (respone) {
                $rootScope.LIST_WIDGET_SIDEBAR_RIGHT = respone;
            } else console.log('K có widget nào');
        });
        MyFactory.GET(MyFactory.HOST_NAME + MyFactory.API.SLIDE).then(function (respone) {
            if (respone) {
                $rootScope.LIST_SLIDE = respone;
            } else console.log('K có slideshow nào');
        });
        $scope.event = {
            hover: function (id) {
                $('#menu-' + id).addClass('open');
            },
            leave: function (id) {
                $('#menu-' + id).removeClass('open');
            }
        };

        $scope.activeMenu = function (id) {
            var countListAllMenu = $scope.LIST_ALL_MENU.length;
            for (var i = 0; i < countListAllMenu; i++) {
                if (id === parseInt($scope.LIST_ALL_MENU[i].ID)) {
                    $('#menu-' + id).addClass('active');
                    $('#menu-' + $scope.LIST_ALL_MENU[i].parent).addClass('active');
                } else $('#menu-' + $scope.LIST_ALL_MENU[i].ID).removeClass('active');
            }
        }

        function getMenuForClient() {
            MyFactory.GET(MyFactory.HOST_NAME + MyFactory.API.MENU.GET_FOR_CLIENT).then(function (respone) {
                if (respone) {
                    $rootScope.LIST_MENU = $sce.trustAsHtml(respone);
                } else $rootScope.LIST_MENU = [];
            });
        }
        MyFactory.GET(MyFactory.HOST_NAME + MyFactory.API.MENU.ALL).then(function (respone) {
            if (respone) {
                $scope.LIST_ALL_MENU = respone;
            } else $rootScope.LIST_MENU = [];
        });
        $scope.html = function (content) {
            return $sce.trustAsHtml(content);
        }
        
        function setUrl(lang){
            switch(lang){
                case 2: $('#a-logo').prop({href: 'en'});
                default: $('#a-logo').prop({href: 'vi'});
            }
        }
        $rootScope.cut = function (content, max_length) {
            if (content.length > max_length) {
                var str = content.substr(0, max_length);
                var cnt = str.substr(0, str.lastIndexOf(' '));
                return cnt + ' [...]';
            } else return content;
        }        
        MyFactory.GET(MyFactory.HOST_NAME + MyFactory.API.VISITOR_COUNTER.LOG).then(function(respone){});
        MyFactory.GET(MyFactory.HOST_NAME + MyFactory.API.WIDGETS + '/footer/'+$scope.langID).then(function (respone) {
            if (respone) {
                $scope.LIST_WIDGET_FOOTER = respone;
            } else console.log('K có widget footer nào');
        });
    
        $('#myTab, .nav-tabs a').click(function(e){
            alert();
            e.preventDefault();
        })
    })
    .directive("compileHtml", function ($parse, $sce, $compile) {
        return {
            restrict: "A",
            link: function (scope, element, attributes) {

                var expression = $sce.parseAsHtml(attributes.compileHtml);

                var getResult = function () {
                    return expression(scope);
                };

                scope.$watch(getResult, function (newValue) {
                    var linker = $compile(newValue);
                    element.append(linker(scope));
                });
            }
        }
    })
    .directive('dynamic', function ($compile) {
        return {
            restrict: 'A',
            replace: true,
            link: function (scope, ele, attrs) {
                scope.$watch(attrs.dynamic, function (html) {
                    ele.html(html);
                    $compile(ele.contents())(scope);
                });
            }
        };
    })
    .directive('setClassWhenAtTop', function ($window) {
    var $win = angular.element($window); // wrap window object as jQuery object

    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var topClass = attrs.setClassWhenAtTop, // get CSS class from directive's attribute value
                offsetTop = element.offset().top; // get element's offset top relative to document

            $win.on('scroll', function (e) {
                if ($win.scrollTop() >= offsetTop) {
                    element.addClass(topClass);
                } else {
                    element.removeClass(topClass);
                }
            });
        }
    };
})
.directive('dynFbCommentBox', function () {
    function createHTML(href, numposts, colorscheme, width) {
        return '<div class="fb-comments" ' +
                       'data-href="' + href + '" ' +
                       'data-numposts="' + numposts + '" ' +
                       'data-colorsheme="' + colorscheme + '" ' +
                        'data-width="100%">' +
               '</div>';
    }

    return {
        restrict: 'A',
        scope: {},
        link: function postLink(scope, elem, attrs) {
            attrs.$observe('pageHref', function (newValue) {
                var href        = newValue;
                var numposts    = attrs.numposts    || 5;
                var colorscheme = attrs.colorscheme || 'light';

                elem.html(createHTML(href, numposts, colorscheme));
                FB.XFBML.parse(elem[0]);
            });
        }
    };
});
