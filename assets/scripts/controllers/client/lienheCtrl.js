angular.module('vpApp').controller('lienheCtrl',['$scope', '$rootScope', '$window', '$sce', 'baseService', 'requestApiHelper', function($scope, $rootScope, $window, $sce, baseService, requestApiHelper){    
    $scope.contactList = {};
    $scope.slideDVPT = [];
    $scope.slideDVTT = [];
    $scope.slideDVPP = [];
    function getContact(){
        var controller = requestApiHelper.CONTACT_PAGE.ALL;
        baseService.GET(controller).then(function(response){
            $scope.contactList = response[0]; 
            console.log($scope.contactList);
        }, function(err){
            console.log(err);
        });
    };
    function getDvPhatTrien(){
        var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 29;
        var param = {type: 'admin'};
        baseService.POST(controller, param).then(function(response){
            $scope.slideDVPT = response; 
            console.log($scope.slideDVPT);
        }, function(err){
            console.log(err);
        });
      };
    
      function getDvTongThau(){
        var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 30;
        var param = {type: 'admin'};
        baseService.POST(controller, param).then(function(response){
            $scope.slideDVTT = response; 
            console.log($scope.slideDVPT);
        }, function(err){
            console.log(err);
        });
      };
    
      function getDvPhanPhoi(){
        var controller = requestApiHelper.SLIDE.GET_BY_CATEID + '/' + 31;
        var param = {type: 'admin'};
        baseService.POST(controller, param).then(function(response){
            $scope.slideDVPP = response; 
            console.log($scope.slideDVPT);
        }, function(err){
            console.log(err);
        });
      };

    getContact();   
    getDvPhatTrien();
      getDvTongThau();
      getDvPhanPhoi();
}]);