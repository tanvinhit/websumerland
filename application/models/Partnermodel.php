<?php
defined('BASEPATH') OR exit('');

class Partnermodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }  
    
    public function getAll(){

        $result = $this->db->query('select * from partner');
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function update($data){

        $this->db->where('id', $data['id']);        
        $result = $this->db->update('partner', $data); 
        if($result !== null){
            return true;
        }
        else{
            return null;
        } 
    }
    
    public function insert($data){
        $result = $this->db->insert('partner', $data);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }

    public function delete($id){
        if(is_array($id)){
            $this->db->where_in('id', $id);
        }else{
            $this->db->where('id', $id);
        }
        $delete = $this->db->delete('partner');
        return $delete ? true : false;
    }
}

?>
