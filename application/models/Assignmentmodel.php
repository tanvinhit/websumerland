<?php
defined('BASEPATH') OR exit('');

class Assignmentmodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
        $this->load->helper('date');
    }
    
    public function addAssignment($data){
        if($data['status_assign'] == 2){

            $result = $this->db->insert('assignment', $data);

            $data_update = array(
                'status' => 1
            );
            $this->db->where('billCustomId', $data['billCustomId']);
            $query2 = $this->db->update('bills', $data_update);

        }else{
            $result = $this->db->insert('assignment', $data);
        }
        
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }

    public function updateAssign($billId,$user_id,$prov_name){
        $billCustomId = $this->db->query("select billCustomId from bills where billId = ".$billId." ")->row()->billCustomId;
        $data = array(
            'user_id' => $user_id,
            'status_assign' => 1,
            'assignDate' => date('Y-m-d'),
            'area' => $prov_name
        );
    
        $this->db->where('billCustomId', $billCustomId);
        $query = $this->db->update('assignment', $data);
        
        $UserName = $this->db->query("select UserName from users where UserID = ".$user_id." ")->row()->UserName;
        $assignment_Id = $this->db->query("select id from assignment where billCustomId = '".$billCustomId."' ")->row()->id;
        $data_history = array(
            'assignment_Id' => $assignment_Id,
            'category_id' => 4,
            'billCustomId' => $billCustomId,
            'date' => date('Y-m-d'),
            'time' => mdate('%h:%i',NOW()),
            'name' => 'Phân công cho bưu tá',
            'name_detail' => "",
        );

        $result = $this->db->insert('history', $data_history);
    
        $this->db->where('billCustomId', $billCustomId);
        $query = $this->db->update('assignment', $data);


        if($query !== null && $result !== null){
            return true;
        }
        else{
            return false;
        }
    }

    public function deleteAssign($billCustomId){

        $data = array(
            'user_id' => 0,
            'status_assign' => 0,
            'assignDate' => null
        );
    
        $this->db->where('billCustomId', $billCustomId);
        $query = $this->db->update('assignment', $data);
        
        $data_history = array(
            'assignment_Id' => 0,
            'category_id' => 4,
            'billCustomId' => $billCustomId,
            'date' => date('Y-m-d'),
            'time' => mdate('%h:%i',NOW()),
            'name' => 'Hủy phân công bưu tá',
            'name_detail' => "",
        );

        $result = $this->db->insert('history', $data_history);

        if($query !== null && $result !== null){
            return true;
        }
        else{
            return false;
        }
    }

    public function approveAssign($billCustomId,$user_id,$assignment_Id){
        $data = array(
            'status_assign' => 2,
            'status' => 1,
            'assignDate' => date('Y-m-d')
        );
        $this->db->where('billCustomId', $billCustomId);
        $query = $this->db->update('assignment', $data);
        
        $data_history = array(
            'assignment_Id' => $assignment_Id,
            'category_id' => 4,
            'billCustomId' => $billCustomId,
            'date' => date('Y-m-d'),
            'time' => mdate('%h:%i',NOW()),
            'name' => 'Đang giao hàng',
            'name_detail' => "",
        );

        $result = $this->db->insert('history', $data_history);

        $data_update = array(
            'user_id' => $user_id,
        );
        $this->db->where('billCustomId', $billCustomId);
        $query = $this->db->update('bills', $data_update);

        if($query !== null && $result !== null && $data_update !== null){
            return true;
        }
        else{
            return false;
        }
    }

    public function deliveryAssign($billCustomId,$user_id,$assignment_Id,$status,$image,$receiveIsName,$Comment){
        $data = array(
            'status' => $status
        );

        $this->db->where('billCustomId', $billCustomId);
        $query = $this->db->update('assignment', $data);
        
        // $UserName = $this->db->query("select UserName from users where UserID = ".$user_id." ")->row()->UserName;
        // $assignment_Id = $this->db->query("select id from assignment where billCustomId = '".$billCustomId."' ")->row()->id;
        $name = '';
        $namedetail = '';
        $category = 0;

        if($status === 2){
            $name = 'Phát thành công';
            $namedetail = "Tên người nhận: ".$receiveIsName." ";
            $category = 1;
        }
        if($status === 3){
            $name = 'Tồn - Thông báo chuyển hoàn';
            $namedetail = "Lí do: ".$Comment."";
            $category = 2;
        }

        $data_history = array(
            'assignment_Id' => $assignment_Id,
            'category_id' => $category,
            'billCustomId' => $billCustomId,
            'date' => date('Y-m-d'),
            'time' => mdate('%h:%i',NOW()),
            'name' => $name,
            'name_detail' => $namedetail,
        );

        $result = $this->db->insert('history', $data_history);

        
        $data_update = array(
            'status' => $status,
            'image' => $image,
            'receiveIsName' => $receiveIsName,
            'Comment' => $Comment
        );
        $this->db->where('billCustomId', $billCustomId);
        $query = $this->db->update('bills', $data_update);

        if($query !== null && $result !== null && $data_update !== null){
            return true;
        }
        else{
            return false;
        }
    }

    public function getById($id){
        $query = $this->db->query("select *
                                from history
                                where billCustomId = '".$id."' ORDER BY id desc");
        if($query->num_rows() > 0)
            return $query->result_array();
        else return null;
    }

    public function getByIdofAssign($id){
        $query = $this->db->query("select *
                                from assignment
                                where billCustomId = '".$id."' ");
        if($query->num_rows() > 0)
            return $query->result_array();
        else return null;
    }

    public function getByUserId($user_id,$assignDate){

        $query = $this->db->query("select *
                                from assignment
                                where user_id = ".$user_id." and assignDate = '".$assignDate."' and status_assign = 1 and (status = 0 or status = 1)");
        
        if($query->num_rows() > 0)
            return $query->result_array();
        else return null;
    }

    public function sendApproveByUserId($id,$assignDate,$assignment_Id,$billCustomId){
        $data_update = array(
            'status_assign' => 2,
            'assignDate' => date('Y-m-d'),
        );
        $this->db->where('user_id', $id);
        $this->db->where('assignDate', $assignDate);
        $this->db->where('status_assign', 1);
        $query = $this->db->update('assignment', $data_update);

        $data_history = array(
            'assignment_Id' => $assignment_Id,
            'category_id' => 4,
            'billCustomId' => $billCustomId,
            'date' => date('Y-m-d'),
            'time' => mdate('%h:%i',NOW()),
            'name' => 'Đang vận chuyển',
            'name_detail' => "",
        );

        $result = $this->db->insert('history', $data_history);
$data_bill = array(
            'user_id' => $user_id,
            'status' => 1
        );
        $this->db->where('billCustomId', $billCustomId);
        $query = $this->db->update('bills', $data_bill);
        if($query !== null && $result !== null && $data_update !== null){
            return true;
        }
        else{
            return false;
        }
    }

    public function getPrintById($id){
        $query = $this->db->query("select *
                                from assignment
                                where user_id = ".$id." and status_assign <> 3 and (status = 0 or status = 1)");

        if($query->num_rows() > 0){
            $data = $query->result_array(); 
            return $data;
        }
        return array(); 
    }
                
}

?>