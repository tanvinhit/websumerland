<?php
defined('BASEPATH') OR exit('');

class Tongquanvtmodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }  
    
    public function getAll(){

        $result = $this->db->query('select * from positon_from_to');
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function update($data){

        $this->db->where('id', $data['id']);        
        $result = $this->db->update('positon_from_to', $data); 
        if($result !== null){
            return true;
        }
        else{
            return null;
        } 
    }
    
    public function insert($data){
        $result = $this->db->insert('positon_from_to', $data);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }

    public function delete($id){
        if(is_array($id)){
            $this->db->where_in('id', $id);
        }else{
            $this->db->where('id', $id);
        }
        $delete = $this->db->delete('positon_from_to');
        return $delete ? true : false;
    }
}

?>
