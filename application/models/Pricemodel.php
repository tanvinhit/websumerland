<?php
defined('BASEPATH') OR exit('');

class Pricemodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
    }    
        
    public function getAll(){
        $result = array();
        $currentLang = $this->phpsession->getCookie('monpham_language');
        $result = $this->db->query('select *
                                    from prices a
                                    order by priceId desc;');
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }

    public function getByCondition($fromPlace, $toPlace, $dcpWeight, $serviceId){
        $result = array();
        $result = $this->db->query("select *
                                    from prices a
                                    where fromPlace = '".$fromPlace."' and toPlace = '".$toPlace."' and service = '".$serviceId."' and ".$dcpWeight." between dcpWeight and dcpToWeight
                                    order by priceId desc;");
        if($result->num_rows() > 0){
            return $result->result()[0];            
        }
        else{
            return null;
        }
    }

    public function getExists($fromPlace, $toPlace, $dcpWeight, $dcpToWeight){
        $result = array();
        $result = $this->db->query("select *
                                    from prices a
                                    where fromPlace = '".$fromPlace."' and toPlace = '".$toPlace."' and dcpWeight = ".$dcpWeight." and dcpToWeight = ".$dcpToWeight.
                                    "order by priceId desc;");
        if($result->num_rows() > 0){
            return $result->result()[0];            
        }
        else{
            return null;
        }
    }

    public function deleteByArray($params){
        $result = array();
        $count = count($params);
        $fromPlaces = array();
        $toPlaces = array();
        $dcpWeights = array();
        for($i = 0; $i < $count; $i++){
            array_push($fromPlaces, $params[$i]['fromPlace']);
            array_push($toPlaces, $params[$i]['toPlace']);
            array_push($dcpWeights, $params[$i]['dcpWeight']);
        }
        if(count($fromPlaces) > 0 && count($toPlaces)){
            $this->db->where_in('fromPlace', $fromPlaces);
            $this->db->where_in('toPlace', $toPlaces);
            if(count($dcpWeights) > 0)
                $this->db->where_in('dcpWeight', $dcpWeights);

            $delete = $this->db->delete('prices');
            return $delete ? true : false;
        }
        return true;
    }
    
    public function update($data){
        $this->db->where('priceId', $data['priceId']);
        $result = $this->db->update('prices', $data);
        if($result !== null){
            return true;            
        }
        else{
            return false;
        }
    }
    
    public function insert($data){
        $result = $this->db->insert('prices', $data);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
        
    public function delete($id){
        if(is_array($id)){
            $this->db->where_in('priceId', $id);
        }else{
            $this->db->where('priceId', $id);
        }
        $delete = $this->db->delete('prices');
        return $delete ? true : false;
    }
}

?>