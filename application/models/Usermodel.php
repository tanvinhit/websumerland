<?php
defined('BASEPATH') OR exit('');

class Usermodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }    
    
    public function loginAccount($Account, $password){
        $password = md5($password);
        $this->db->select('UserID');
            $this->db->select('UserName');
            $this->db->select('Phone');
            $this->db->select('Email');
            $this->db->select('Permission');
            $this->db->select('Avatar'); 
            $this->db->select('Password'); 
            $this->db->from('users');
            $this->db->where('Account', $Account);
            $this->db->where('Password', $password);
            $this->db->where('Status', 1);
            $this->db->limit(1);
            $result = $this->db->get();
        if($result->num_rows() !== 0){
            return $result->result_array();
        }
        else{
            return null;
        }        
    }
    
    public function getAllUsers($type){

        if($type !== 'Bưu tá'){
            $result = $this->db->query('select UserID, Account, UserName,Phone,Email,Permission,Avatar,Status,IsContact,Facebook,Google,Youtube,Twitter
                                    from users where Permission <> "Bưu tá"');
        }else{
            
            $result = $this->db->query('select UserID, Account, UserName, Address, Phone,Email,Permission,Avatar,Status,IsContact, prov_id, prov_name
                                    from users WHERE Permission = "Bưu tá";');
        }
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function updateUser($data){
        if(isset($data['Password']))
            $data['Password'] = md5($data['Password']);
        $this->db->where('UserID', $data['UserID']);        
        $result = $this->db->update('users', $data);  
        
        $this->db->select('UserID');
        $this->db->select('UserName');
        $this->db->select('Phone');
        $this->db->select('Email');
        $this->db->select('Password');        
        $this->db->select('Permission');
        $this->db->select('Avatar'); 
        $this->db->select('Account');        
        $this->db->from('users');
        $this->db->where('UserID', $data['UserID']);        
        $this->db->limit(1);
        $result = $this->db->get();
        if($result->num_rows() !== 0){
            return $result->result_array();
        }
        else{
            return null;
        } 
    }   
            
    public function addUser($data){
        $data['Password'] = md5($data['Password']);
        $result = $this->db->insert('users', $data);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function deleteUser($userId){
        $param = array(
            'UserID' => $userId  
        );
        $result = $this->db->delete('users', $param);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function checkEmail($email){
        $this->db->select('UserID');
        $this->db->from('users');
        $this->db->where('Email', $email);
        $result = $this->db->get();
        if($result->num_rows() !== 0){
            return 1;
        }
        else{
            return 0;
        }        
    }

    public function checkAccount($account){
        $this->db->select('UserID');
        $this->db->from('users');
        $this->db->where('Account', $account);
        $result = $this->db->get();
        if($result->num_rows() !== 0){
            return 1;
        }
        else{
            return 0;
        }        
    }
    
    public function checkSessionUser($email, $pwd){
        $pwd = md5($pwd);
        $this->db->select('UserID');
        $this->db->from('users');
        $this->db->where('Email', $email);
        $this->db->where('Password', $pwd);
        $result = $this->db->get();
        return ($result->num_rows() > 0)? true : false;
    }

    public function searchProvince($name){
        $this->db->select('prov_name');
        $this->db->from('devvn_tinhthanhpho');
        $this->db->like('prov_name', $name, 'both');
        $this->db->limit(5);
        $result = $this->db->get();
        if($result->num_rows() !== 0){
            return $result->result_array();
        }
        else{
            return null;
        }     
    }

    public function searchPostman($name,$prov_name){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->like('UserName', $name, 'both');
        $this->db->where('prov_name', $prov_name);
        $this->db->where('Status', 1);
        $this->db->limit(5);
        $result = $this->db->get();
        if($result->num_rows() !== 0){
            return $result->result_array();
        }
        else{
            return null;
        }     
    }

    public function checkProvince($prov_name){
        $this->db->select('prov_id,prov_name');
        $this->db->from('devvn_tinhthanhpho');
        $this->db->where('prov_name', $prov_name);
        $result = $this->db->get();
        if($result->num_rows() !== 0){
            return $result->result_array();
        }
        else{
            return null;
        }          
    }

    public function getByProvince($prov_id){
        $query = $this->db->query("select *
                                    from users
                                    where prov_id = '".$prov_id."' ");
        if($query->num_rows() > 0)
            return $query->result_array();
        else return null;
    }
    
}

?>