<?php
defined('BASEPATH') OR exit('');

class Templatemodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();         
    }    
       
    public function getTemplates(){
        $result = $this->db->query('select TempID,TempName,Filename,Status,Type,Meta,Language,SeoTitle,SeoKeyword,SeoDescribes,IsLanding,SeoCanonica,MetaRobot
                          from templates
                          order by TempID desc');
        if($result->num_rows() > 0)
            return $result->result_array();
        return array();
    }
    
    public function getPages($data){
        $result = $this->db->query('select TempID,TempName,Meta,Language
                                    from templates
                                    where Type = "'.$data['type'].'" and Language = "'.$data['lang'].'"
                                    order by TempID desc;');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
    
    public function updateTemplate($data){    
        $query = $this->db->query('select Meta from templates where TempID = "'.$data['TempID'].'";');
        $row = $query->result();
        $oldMeta = $row[0]->Meta;       
        $this->db->where('TempID', $data['TempID']);        
        $result = $this->db->update('templates', $data);
        $this->db->query('update navigates
        set NavMeta = "'.$data['Meta'].'"
        where NavMeta = "'.$oldMeta.'";');    
        if($result !== null){
            return true;            
        }
        else{
            return false;
        }
    }
    
    public function addTemplate($data){        
        $result = $this->db->insert('templates', $data);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function deleteTemplate($id){
        $param = array(
            'TempID' => $id  
        );
        $result = $this->db->delete('templates', $param);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
        
    public function getTemplateByMeta($meta){
        $result = $this->db->query('select a.TempId,Filename,b.Type,b.SeoTitle,b.SeoKeyword,b.SeoDescribes,IsLanding,b.SeoCanonica,b.MetaRobot, CatMeta as Meta
        from categories a inner join templates b on a.TempId = b.TempID 
        where CatMeta = "'.$meta.'"
        union all
        select a.TempId,Filename,Type,b.SeoTitle,b.SeoKeyword,b.SeoDescribes,IsLanding,b.SeoCanonica,b.MetaRobot, ArtMeta as Meta
        from articles a inner join templates b on a.TempId = b.TempID 
        where ArtMeta = "'.$meta.'.html"
        union all
        select a.TempId,Filename,Type,b.SeoTitle,b.SeoKeyword,b.SeoDescribes,IsLanding,b.SeoCanonica,b.MetaRobot, ProMeta as Meta
        from products a inner join templates b on a.TempId = b.TempID 
        where ProMeta = "'.$meta.'.html"
        union all
        select TempId,Filename,Type,SeoTitle,SeoKeyword,SeoDescribes,IsLanding,SeoCanonica,MetaRobot, Meta
        from templates
        where Meta = "'.$meta.'.html";');
        if($result->num_rows() > 0){
            $row = $result->result();            
            return $row[0];
        }
        else{
            return null;
        }
    }

    public function getMetaByFileName($filename, $lang){
        $result = $this->db->query('select Meta from templates where Filename = "'.$filename.'" and Language = "'.$lang.'";');
        if($result->num_rows() > 0){
            $row = $result->result();            
            return $row[0]->Meta;
        }
        else{
            return '';
        }
    }
            
}

?>