<?php
defined('BASEPATH') OR exit('');

class Customermodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
    }    
    
    public function getAllCustomers($type, $lang){
        $currentLang = $this->phpsession->getCookie('monpham_language');
        if($type === 'admin'){ 
            if($lang === null){
                $result = $this->db->query('select ID,Name,Estimate,Image,Address,Phone,Language,Email, companyName, district, districtDetail
                                            from customers;');
            }
            else{
                $result = $this->db->query('select ID,Name,Estimate,Image,Address,Phone,Language,Email, companyName, district, districtDetail
                                            from customers    
                                            where Language = "'.$lang.'";');
            }
        }
        else{
            $result = $this->db->query('select ID,Name,Estimate,Image,Address,Phone,Language,Email, companyName, district, districtDetail
                                        from customers
                                        where Language = "'.$currentLang.'";');
        }
        if($result->num_rows() > 0)
            return $result->result_array();                            
        return array();
        
    }
    
    public function updateCustomer($Id, $name, $company, $est, $img, $addr, $phone, $lang, $email, $district, $districtDetail,$province){
        $param = array(            
            'Name' => $name,
            'Estimate' => $est,            
            'Image' => $img,            
            'Address' => $addr,
            'Phone' => $phone,
            'Language' => $lang,
            'Email' => $email,
            'companyName' => $company,
            'district' => $district,
            'districtDetail' => $districtDetail,
            'province' => $province
        );        
        $this->db->where('ID', $Id);        
        $result = $this->db->update('customers', $param);    
        if($result !== null)
            return true;       
        return false;
    }
    
    public function addCustomer($name, $company, $est, $img, $addr, $phone, $lang, $email, $district, $districtDetail, $province){
        $param = array(            
            'Name' => $name,
            'Estimate' => $est,            
            'Image' => $img,            
            'Address' => $addr,
            'Phone' => $phone,
            'Language' => $lang,
            'Email' => $email,
            'companyName' => $company,
            'district' => $district,
            'districtDetail' => $districtDetail,
            'province' => $province
        ); 
        $result = $this->db->insert('customers', $param);
        if($result !== null)
            return true;       
        return false;        
    }
    
    public function deleteCustomer($Id){
        $param = array(
            'ID' => $Id  
        );
        $result = $this->db->delete('customers', $param);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
        
    public function getById($customerId){
        $result = $this->db->query('select *
        from customers
        where ID = '.$customerId.';');
        return ($result->num_rows() > 0)? $result->result()[0] : null;
    }
}

?>