<?php
defined('BASEPATH') OR exit('');

class Articlemodel extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
        $this->load->model('Categorymodel');
    }    
    
    public function getAllArticles($type, $lang){
        
        $currentLang = $this->phpsession->getCookie('monpham_language');
        if($type === 'admin'){
            if($lang === null){
                $result = $this->db->query('SELECT ArtID, ArtName, ArtDescribes, ArtMeta, Image, DateCreated, Author, UserName, ArtLang, ViewCount, Content, articles.Status, articles.TempId,Video, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                FROM articles                        
                JOIN users ON articles.Author = users.UserID                   
                order by DateCreated desc;'); 
            }
            else{
                $result = $this->db->query('SELECT ArtID, ArtName, ArtDescribes, ArtMeta, Image, DateCreated, Author, UserName, ArtLang, ViewCount, Content, articles.Status, articles.TempId,Video, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                FROM articles                        
                JOIN users ON articles.Author = users.UserID 
                where ArtLang = "'.$lang.'"
                order by DateCreated desc;'); 
            }
        }
        else{
            $result = $this->db->query('SELECT ArtID, ArtName, ArtDescribes, ArtMeta, Image, DateCreated, Author, UserName, ArtLang, ViewCount, Content, articles.Status, articles.TempId,Video, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                FROM articles                        
                JOIN users ON articles.Author = users.UserID 
                where ArtLang = "'.$currentLang.'" and articles.Status = 1
                order by DateCreated desc;');
        }
                
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function getArticles($type, $lang){
        //$store_procedure = 'call sp_getAllArticles()';
        //$result = $this->db->query($store_procedure);
        
        $currentLang = $this->phpsession->getCookie('monpham_language');
        if($type === 'admin'){
            if($lang === null){
                $result = $this->db->query('SELECT ArtID, ArtName, ArtDescribes, ArtMeta, Image, DateCreated, Author, UserName, ArtLang, ViewCount, Content, articles.Status, articles.TempId,Video, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot, DAY(DateCreated) as DayBinding, DATE_FORMAT(DateCreated,"%m-%Y") as MonthYearBinding
                FROM articles                        
                JOIN users ON articles.Author = users.UserID                   
                order by DateCreated desc;');
            }
            else{
                $result = $this->db->query('SELECT ArtID, ArtName, ArtDescribes, ArtMeta, Image, DateCreated, Author, UserName, ArtLang, ViewCount, Content, articles.Status, articles.TempId,Video, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot, DAY(DateCreated) as DayBinding, DATE_FORMAT(DateCreated,"%m-%Y") as MonthYearBinding
                FROM articles                        
                JOIN users ON articles.Author = users.UserID 
                where ArtLang = "'.$lang.'"
                order by DateCreated desc;'); 
            }
        }
        else{
            $result = $this->db->query('SELECT ArtID, ArtName, ArtDescribes, ArtMeta, Image, DateCreated, Author, UserName, ArtLang, ViewCount, Content, articles.Status, articles.TempId,Video, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                FROM articles                        
                JOIN users ON articles.Author = users.UserID 
                where ArtLang = "'.$currentLang.'" and articles.Status = 1
                order by DateCreated desc;');
        }
                
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
                        
    public function updateArticle($data){
        $query = $this->db->query('select ArtMeta from articles where ArtID = "'.$data['ArtID'].'";');
        $row = $query->result();
        $oldMeta = $row[0]->ArtMeta;
        $this->db->where('ArtID', $data['ArtID']);
        $result = $this->db->update('articles', $data);  
        $this->db->query('update navigates
                          set NavMeta = "'.$data['ArtMeta'].'"
                          where NavMeta = "'.$oldMeta.'";');
        if($result !== null){
            return true;            
        }
        else{
            return false;
        }
    }

    public function addArticle($data){     
        $result = $this->db->insert('articles', $data);
        if($result !== null){
            return array(
                'success' => true,
                'last' => $this->db->insert_id()
            );            
        }
        else{
            return array(
                'success' => false,
                'last' => 0
            ); 
        }
    }
    
    public function deleteArticle($Id){
        $param = array(
            'ArtID' => $Id  
        );
        $result = $this->db->delete('articles', $param);        
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
         
    function addView($artId){
        $this->db->query('update articles
                          set ViewCount = ViewCount + 1
                          where ArtID ='.$artId.';'); 
    }
    
    function getRelatedPost($artId, $lang, $cateID){
        if(is_numeric($artId) && $cateID != ''){
            $queryString = "select ArtID,ArtName,ArtDescribes,ArtMeta,Image,DateCreated,a.CatId,Author,Username,ViewCount,Video
                                from articles a left join users c on a.Author = c.UserID
                                where (a.CatID";
            $arr = explode(',',$cateID);
            $count = count($arr);
            for($i = 1; $i < $count - 1; $i++){                
                if($i === 0)
                    $queryString .= " like '%,".$arr[$i]."%,'";
                else $queryString .= " or a.CatID like '%,".$arr[$i]."%,'";
            }
            
            $queryString .= ") and ArtID <> ".$artId." and a.Status = 1 and ArtLang = '".$lang."' order by DateCreated desc limit 6";            
            $result = $this->db->query($queryString);

            if($result->num_rows() != 0)
                return $result->result_array();
            else return array();
        }
        else return array();        
    }
    
    public function getArt($meta, $lang){        
        if(!isset($lang))
            $lang = $this->phpsession->getCookie('monpham_language');
        $result = $this->db->query('select ArtID,ArtName,ArtMeta,Content,a.CatId,DateCreated,Username,ViewCount,ArtDescribes,Image,Video, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot, DAY(a.DateCreated) as DayBinding, DATE_FORMAT(a.DateCreated,"%m-%Y") as MonthYearBinding
        from articles a join users c on a.Author = c.UserID
        where ArtMeta = "'.$meta.'" and ArtLang = "'.$lang.'" and a.Status = 1;');
                        
        if($result->num_rows() > 0){
            $row = $result->result();
            $articles = $row[0];
            //update visit counter
            $this->addView($articles->ArtID); 
            return $articles;
        }        
        return null;
    }
    
    public function editArticle($artId){
        $result = $this->db->query('select ArtID,ArtName,ArtDescribes,ArtMeta,Image,DateCreated,CatId,Author,UserName as Username,ArtLang,ViewCount,Content,a.Status,TempId,Video, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
        from articles a join users b on a.Author = b.UserID
        where ArtID = '.$artId.';');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
            
    public function getFeatureArt($limit, $lang){
        $result = $this->db->query('select ArtID,ArtName,ArtMeta,ArtDescribes as Describes,Image,Video, UserName, Avatar, DateCreated, ViewCount
                                    from articles a join users b on a.Author = b.UserID
                                    where a.Status = 1 and ArtLang = "'.$lang.'"
                                    order by ViewCount desc
                                    limit '.$limit.';');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
    
    public function getAllArt($lang){
        $result = $this->db->query('SELECT ArtID, ArtName,ArtMeta,ArtDescribes as Describes, Image, Video, UserName, Avatar, DateCreated, ViewCount        
                                    FROM articles a join users b on a.Author = b.UserID                              
                                    where ArtLang = "'.$lang.'" and a.Status = 1
                                    order by DateCreated desc;');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
    
    public function getNewArt($limit, $lang){        
        $result = $this->db->query('SELECT ArtID,ArtName,ArtMeta,ArtDescribes as Describes,Image,Video,UserName,Avatar, DateCreated, ViewCount
                FROM articles a join users b on a.Author = b.UserID                              
                where a.Status = 1 and ArtLang = "'.$lang.'"
                order by DateCreated desc
                limit '.$limit.';');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
    
    function getArtListForCate($cateId, $lang){
        $artList = array();
        if(isset($cateId) && $cateId !== ''){
            $artList = $this->db->query('select ArtID,ArtName,ArtMeta,ArtDescribes,Image,DateCreated,a.CatId,Author, Avatar,UserName,ViewCount,Video
                from articles a join users c on a.Author = c.UserID
                where a.CatId LIKE "%,'.$cateId.',%" and ArtLang = "'.$lang.'" and a.Status = 1
                order by ArtID desc;');
        }
        
        return ($artList->num_rows() > 0)? $artList->result_array() : array();
    }
    
    function getArtListForCateWithPaging($cateId, $lang, $per_page, $page){
        $artList = array();
        if(isset($cateId) && $cateId !== ''){
            $artList = $this->db->query('select ArtID,ArtName,ArtMeta,ArtDescribes,Image,DateCreated,a.CatId,Author, Avatar,UserName,ViewCount,Video
                from articles a join users c on a.Author = c.UserID
                where a.CatId LIKE "%,'.$cateId.',%" and ArtLang = "'.$lang.'" and a.Status = 1
                
                order by ArtID desc Limit '.$page.', '.$per_page.';');
        }
        
        return ($artList->num_rows() > 0)? $artList->result_array() : array();
    }

    public function recordByCateCount($cateId, $lang){
        $artList = array();
        if(isset($cateId) && $cateId !== ''){
            $artList = $this->db->query('select * from articles a
                where a.CatId LIKE "%'.$cateId.'%" and ArtLang = "'.$lang.'" and a.Status = 1;');
        }
        
        return $artList->num_rows();
    }    
}

?>