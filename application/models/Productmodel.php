<?php
defined('BASEPATH') OR exit('');

class Productmodel extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
        $this->load->model('Categorymodel');
    }    
    
    public function getAllProducts($type, $lang){
        //$store_procedure = 'call sp_getAllProducts()';
        //$result = $this->db->query($store_procedure);
        
        $currentLang = $this->phpsession->getCookie('monpham_language');
        if($type === 'admin'){
            if($lang === null){
                $result = $this->db->query('SELECT ProID, ProName, ProDescribes, ProMeta, Image, DateCreated, Author, UserName, ProLang, ViewCount, Content, products.Status, products.TempId,Price,Discount,Manufacture,Estimate,ProductCode,Video,ImageList, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                FROM products                        
                JOIN users ON products.Author = users.UserID                   
                order by DateCreated desc;'); 
            }
            else{
                $result = $this->db->query('SELECT ProID, ProName, ProDescribes, ProMeta, Image, DateCreated, Author, UserName, ProLang, ViewCount, Content, products.Status, products.TempId,Price,Discount,Manufacture,Estimate,ProductCode,Video,ImageList, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                FROM products                        
                JOIN users ON products.Author = users.UserID 
                where ProLang = "'.$lang.'"
                order by DateCreated desc;'); 
            }
        }
        else{
            $result = $this->db->query('SELECT ProID, ProName, ProDescribes, ProMeta, Image, DateCreated, Author, UserName, ProLang, ViewCount, Content, products.Status, products.TempId,Price,Discount,Manufacture,Estimate,ProductCode,Video,ImageList, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                FROM products                        
                JOIN users ON products.Author = users.UserID 
                where ProLang = "'.$currentLang.'" and products.Status = 1
                order by DateCreated desc;');
        }
                
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function getProducts($type, $lang){
        //$store_procedure = 'call sp_getAllproducts()';
        //$result = $this->db->query($store_procedure);
        
        $currentLang = $this->phpsession->getCookie('monpham_language');
        if($type === 'admin'){
            if($lang === null){
                $result = $this->db->query('SELECT ProID, ProName, ProDescribes, ProMeta, Image, DateCreated, Author, UserName, ProLang, ViewCount, Content, products.Status, products.TempId,Price,Discount,Manufacture,Estimate,ProductCode,Video,ImageList, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                FROM products                        
                JOIN users ON products.Author = users.UserID                   
                order by DateCreated desc;');
            }
            else{
                $result = $this->db->query('SELECT ProID, ProName, ProDescribes, ProMeta, Image, DateCreated, Author, UserName, ProLang, ViewCount, Content, products.Status, products.TempId,Price,Discount,Manufacture,Estimate,ProductCode,Video,ImageList, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                FROM products                        
                JOIN users ON products.Author = users.UserID 
                where ProLang = "'.$lang.'"
                order by DateCreated desc;'); 
            }
        }
        else{
            $result = $this->db->query('SELECT ProID, ProName, ProDescribes, ProMeta, Image, DateCreated, Author, UserName, ProLang, ViewCount, Content, products.Status, products.TempId,Price,Discount,Manufacture,Estimate,ProductCode,Video,ImageList, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
                FROM products                        
                JOIN users ON products.Author = users.UserID 
                where ProLang = "'.$currentLang.'" and products.Status = 1
                order by DateCreated desc;');
        }
                
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function getProForCate($cateId, $limit, $lang){     
        $result = array();
        if(isset($cateId) && $cateId !== ''){
            $result = $this->db->query('SELECT ProID, ProName, ProMeta, Image, ProDescribes, Video, UserName, Avatar, DateCreated, ViewCount
                    FROM products a join users b on a.Author = b.UserID                               
                    where ProLang = "'.$lang.'" and CatId LIKE "%'.$cateId.'%" and a.Status = 1
                    order by DateCreated desc
                    limit '.$limit.';');
        }
        
        return ($result->num_rows() > 0)? $result->result_array() : array();        
    }
                
    public function updateProduct($data){
        $query = $this->db->query('select ProMeta from products where ProID = "'.$data['ProID'].'";');
        $row = $query->result();
        $oldMeta = $row[0]->ProMeta;
        $this->db->where('ProID', $data['ProID']);
        $result = $this->db->update('products', $data);  
        $this->db->query('update navigates
                          set NavMeta = "'.$data['ProMeta'].'"
                          where NavMeta = "'.$oldMeta.'";');
        if($result !== null){
            return true;            
        }
        else{
            return false;
        }
    }

    public function addProduct($data){     
        $result = $this->db->insert('products', $data);
        if($result !== null){
            return array(
                'success' => true,
                'last' => $this->db->insert_id()
            );            
        }
        else{
            return array(
                'success' => false,
                'last' => 0
            ); 
        }
    }
    
    public function deleteProduct($Id){
        $param = array(
            'ProID' => $Id  
        );
        $result = $this->db->delete('products', $param);        
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
         
    function addView($ProID){
        $this->db->query('update products
                          set ViewCount = ViewCount + 1
                          where ProID ='.$ProID.';'); 
    }
    
    function getRelatedPost($ProID, $lang, $cateID){                      
        if(is_numeric($ProID) && $cateID != ''){
            $queryString = "select ProID,ProName,ProDescribes,ProMeta,Image,DateCreated,Author,Username,ViewCount,Price,Discount,Manufacture,Video
                                from products a left join users c on a.Author = c.UserID
                                where (a.CatID";
            $arr = explode(',',$cateID);
            $count = count($arr);
            for($i = 1; $i < $count - 1; $i++){                
                if($i === 0)
                    $queryString .= " like '%,".$arr[$i]."%,'";
                else 
                    $queryString .= " or a.CatID like '%,".$arr[$i]."%,'";
            }
            
            $queryString .= ") and ProID <> ".$ProID." and a.Status = 1 and ProLang = '".$lang."' order by DateCreated desc limit 6";            
            $result = $this->db->query($queryString);

            if($result->num_rows() != 0)
                return $result->result_array();
            else return array();
        }
        else return array(); 
    }
    
    public function getPro($meta, $lang){   
        if(isset($lang))     
            $lang = $this->phpsession->getCookie('monpham_language');        
        $result = $this->db->query('select ProID,ProName,ProMeta,Content,a.CatId,DateCreated,Username,ProductCode,ViewCount,ProDescribes,Image,ImageList,Price,Discount,Manufacture,Estimate,Video, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
        from products a join users c on a.Author = c.UserID
          where ProMeta = "'.$meta.'" and ProLang = "'.$lang.'" and a.Status = 1;');
                        
        if($result->num_rows() > 0){            
            $row = $result->result();
            $products = $row[0];
            //update visit counter
            $this->addView($products->ProID); 
            return $products;           
        }
        else{
            return null;
        }
    }       
        
    public function editProduct($ProID){
        $result = $this->db->query('select ProID,ProName,ProDescribes,ProMeta,Image,DateCreated,CatId,Author,UserName as Username,ProLang,ViewCount,Content,a.Status,TempId,Price,Discount,Manufacture,Estimate,Video,ImageList,ProductCode, SeoTitle, SeoKeyword, SeoDescribes, SeoCanonica, MetaRobot
        from products a join users b on a.Author = b.UserID
        where ProID = '.$ProID.';');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
            
    public function getFeaturePro($limit, $lang){
        $result = $this->db->query('select ProID,ProName,ProMeta,ProDescribes as Describes,Image,Video, UserName, Avatar, DateCreated, ViewCount,Price,Discount
                                    from products a join users b on a.Author = b.UserID
                                    where a.Status = 1 and ProLang = "'.$lang.'"
                                    order by ViewCount desc
                                    limit '.$limit.';');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
    
    public function getAllPro($lang){
        $result = $this->db->query('SELECT ProID, ProName,ProMeta,ProDescribes as Describes, Image, Video, UserName, Avatar, DateCreated, ViewCount ,Price,Discount       
                                    FROM products a join users b on a.Author = b.UserID                              
                                    where ProLang = "'.$lang.'" and a.Status = 1
                                    order by DateCreated desc;');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
    
    public function getNewPro($limit, $lang){        
        $result = $this->db->query('SELECT ProID,ProName,ProMeta,ProDescribes as Describes,Image,Video,UserName,Avatar, DateCreated, ViewCount
                FROM products a join users b on a.Author = b.UserID                              
                where a.Status = 1 and ProLang = "'.$lang.'"
                order by DateCreated desc
                limit '.$limit.';');
        return ($result->num_rows() > 0)? $result->result_array() : array();
    }
        
    public function recordByCateCount($cateId, $lang){
        $artList = array();
        if(isset($cateId) && $cateId !== ''){
            $artList = $this->db->query('select * from products a
                where a.CatId LIKE "%'.$cateId.'%" and ProLang = "'.$lang.'" and a.Status = 1;');
        }
        
        return $artList->num_rows();
    }

    function getProListForCate($cateId, $lang){
        $artList = array();
        if(isset($cateId) && $cateId !== ''){
            $artList = $this->db->query('select ProID,ProName,ProMeta,ProDescribes,Image,DateCreated,a.CatId,Author, Avatar,UserName,ViewCount,Video
                from products a join users c on a.Author = c.UserID
                where a.CatId LIKE "%,'.$cateId.',%" and ProLang = "'.$lang.'" and a.Status = 1
                order by ProID desc;');
        }
        
        return ($artList->num_rows() > 0)? $artList->result_array() : array();
    }
    
    function getProListForCateWithPaging($cateId, $lang, $per_page, $page){
        $artList = array();
        if(isset($cateId) && $cateId !== ''){
            $artList = $this->db->query('select ProID,ProName,ProMeta,ProDescribes,Image,DateCreated,a.CatId,Author, Avatar,UserName,ViewCount,Video, Price, Discount
                from products a join users c on a.Author = c.UserID
                where a.CatId LIKE "%,'.$cateId.',%" and ProLang = "'.$lang.'" and a.Status = 1
                order by ProID desc
                limit '.$page.', '.$per_page.';');
        }
        
        return ($artList->num_rows() > 0)? $artList->result_array() : array();
    }
}

?>