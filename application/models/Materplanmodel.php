<?php
defined('BASEPATH') OR exit('');

class Materplanmodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }  
    
    public function getAll(){

        $result = $this->db->query('select * from master_plan where Status = 1 ORDER BY sort ASC');
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }

    public function getClient(){

        $result = $this->db->query('SELECT * from master_plan a
        left join master_plan_detail b on a.id = b.master_plan_id
        where a.Status = 1
        order by a.sort, b.sort ASC');
        if($result->num_rows() > 0){
            return $result->result_array();            
        }
        else{
            return array();
        }
    }
    
    public function updateMasterPlan($data){

        $this->db->where('id', $data['id']);        
        $result = $this->db->update('master_plan', $data); 
        if($result !== null){
            return true;
        }
        else{
            return null;
        } 
    }   
            
    public function insertMasterPlan($data){
        $result = $this->db->insert('master_plan', $data);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
    
    public function deleteMasterPlan($id){
        $param = array(
            'Status' => 0  
        );
        $this->db->where('id', $id);
        $result = $this->db->update('master_plan', $param);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }
    
}

?>
