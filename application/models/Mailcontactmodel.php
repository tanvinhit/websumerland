<?php
defined('BASEPATH') OR exit('');

class Mailcontactmodel extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('phpsession');
    }    
    
    public function getMailList(){        
        $result = $this->db->query('select *
                                    from mailcontact order by DateSent desc;');            
        if($result->num_rows() > 0)
            return $result->result_array();                            
        return array();
        
    }
            
    public function addMail($name, $email, $sub, $content, $date, $phone, $deviceType, $ip){
        $param = array(            
            'Name' => $name,
            'Email' => $email,            
            'Subject' => $sub,                        
            'Content' => $content,
            'DateSent' => $date,
            'Phone' => $phone,
            'DeviceType' => $deviceType,
            'IP' => $ip,
        );  
        $result = $this->db->insert('mailcontact', $param);
        if($result !== null)
            return true;       
        return false;        
    }
    
    public function deleteMail($Id){
        $param = array(
            'ID' => $Id  
        );
        $result = $this->db->delete('mailcontact', $param);
        if($result !== null){
            return true;
        }
        else{
            return false;
        }
    }

    public function sendMail(){  
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session != null){                           
            $requestData = $this->input->post('data');         
            $subject = $requestData['Subject'];
            $body = $requestData['Message'];
            $type = $requestData['Type']; 
            $name = isset($requestData['Name'])? $requestData['Name'] : 'MON PHAM';
            $to_email = $requestData['Email'];
            $from_email = 'vgangnamstylev@gmail.com';
            $dateSent = $requestData['DateSent'];
            
            if($type === 'reply'){                
                $config = getDefaultConfig();
            }
            else{                
                $pass = '01648872550';
                $config = GetandSetConfig('ssl://smtp.googlemail.com', 465, $from_email, $pass);
            }
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            echo json_encode($not_login);
        }
    }
        
}

?>