<?php
defined('BASEPATH') OR exit('');
class Urlmodel extends CI_Model{
 
    public function __construct(){
        $this->load->database();
    }
    
    public function getURLS(){ 
        $query = $this->db->query('select CatMeta as Url, 1 as Type from categories
                                   union all
                                   select ArtMeta as Url, 2 as Type from articles
                                   union all
                                   select ProMeta as Url, 3 as Type from products
                                   union all
                                   select Meta as Url, 4 as Type from templates where Meta <> "";');
        return $query->result_array();
    }
}
?>