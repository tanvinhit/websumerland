<?php
class Billmodel extends CI_Model{
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }
    
    public function getAll($type,$user_id,$fromDate,$toDate){
        if($type === 'ALL'){
            $query = $this->db->query('select a.*, b.Name as CustomerName, b.Phone as CustomerPhone, b.Address as CustomerAddress, b.Email as CustomerEmail, c.UserName, d.Name
            from bills a left join customers b on a.customerSend = b.ID 
                            left join users c on a.user_id = c.UserID 
                           left join services d on a.serviceId = d.ServiceId
                       where isDeleted = false 
                       order by billId desc');
        }
        if($type === 'HISTORY'){
            $query = $this->db->query("select a.*, b.Name as CustomerName, b.Phone as CustomerPhone, b.Address as CustomerAddress, b.Email as CustomerEmail, c.UserName, d.Name, e.*
            from bills a 
            left join customers b on a.customerSend = b.ID 
            left join users c on a.user_id = c.UserID 
            left join services d on a.serviceId = d.ServiceId
            inner join assignment e on a.billCustomId = e.billCustomId
            where isDeleted = false and e.assignDate >= '".$fromDate."' and e.assignDate <= '".$toDate."'
            order by billId desc");
        }
        if($type === 'BƯU TÁ'){
            $query = $this->db->query("select a.*, b.Name as CustomerName, b.Phone as CustomerPhone, b.Address as CustomerAddress, b.Email as CustomerEmail, c.UserName, d.Name
            from bills a 
            left join customers b on a.customerSend = b.ID 
            left join users c on a.user_id = c.UserID 
            left join services d on a.serviceId = d.ServiceId
            inner join assignment e on a.billCustomId = e.billCustomId
            where isDeleted = false AND e.user_id = ".$user_id." and e.assignDate >= '".$fromDate."' and e.assignDate <= '".$toDate."'
            order by billId desc");
        }
        if($type === 'APPROVE'){
            $query = $this->db->query("select a.*, b.Name as CustomerName, b.Phone as CustomerPhone, b.Address as CustomerAddress, b.Email as CustomerEmail, c.UserName, d.Name, e.user_id, e.id AS assignment_Id
            from bills a 
            left join customers b on a.customerSend = b.ID 
            left join users c on a.user_id = c.UserID 
            left join services d on a.serviceId = d.ServiceId
            inner join assignment e on a.billCustomId = e.billCustomId
            where isDeleted = false AND e.user_id = ".$user_id." AND e.status_assign = 1
            order by billId desc");
        }
        if($type === 'DELIVERY'){
            $query = $this->db->query("select a.*, b.Name as CustomerName, b.Phone as CustomerPhone, b.Address as CustomerAddress, b.Email as CustomerEmail, c.UserName, d.Name, e.user_id, e.id AS assignment_Id, e.*
            from bills a 
            left join customers b on a.customerSend = b.ID 
            left join users c on a.user_id = c.UserID 
            left join services d on a.serviceId = d.ServiceId
            inner join assignment e on a.billCustomId = e.billCustomId
            where isDeleted = false AND e.user_id = ".$user_id." AND e.status_assign = 2 AND e.status = 1
            order by billId desc");
        }

        if($query->num_rows() > 0)
            return $query->result_array();
        else return false;
    }
    
    public function insert($data){
        $query = $this->db->insert('bills', $data);
        $data_history = array(
            'assignment_Id' => 0,
            'category_id' => 4,
            'billCustomId' => $data['billCustomId'],
            'date' => date('Y-m-d'),
            'time' => mdate('%h:%i',NOW()),
            'name' => 'Đang đóng gói',
            'name_detail' => "Nhập máy phiếu gửi",
        );

        $result = $this->db->insert('history', $data_history);
        $result = $this->db->error();
        if($query !== null)
            return true;       
        return false;
    }
    
    public function update($data){
        if(is_numeric($data['billId'])){
            $this->db->where('billId',$data['billId']);
            $this->db->where('isDeleted', false);
            $query = $this->db->update('bills',$data);
            if($query)
                return true;
            else return false;
        }
    }
    public function updateStatus($data){
        if(is_numeric($data['billId'])){
            $this->db->where('billId',$data['billId']);
            $query = $this->db->update('bills',array('status' => $data['status']));
            if($query)
                return true;
            else return false;
        }
    }
    
    public function delete($id){
        if(is_numeric($id)){
            $this->db->where('billId',$id);
            $query = $this->db->update('bills',array('isDeleted' => true));
            //$query = $this->db->delete('billdetails');
            //$this->db->where('billId',$id);
            //$query = $this->db->delete('bills');
            
            if($query)
                return true;
            else return false;
        }
        else return null;
    }
    
    public function getById($id){
        $query = $this->db->query("select *
                                    from bills
                                    where isDeleted = 0 and billId = ".$id." 
                                    order by billId desc");
        if($query->num_rows() > 0)
            return $query->result()[0];
        else return null;
    }

    public function getPrintById($id){
        $query = $this->db->query("select a.*, b.Name as CustomerName, b.Phone as CustomerPhone, b.Address as CustomerAddress, b.Email as CustomerEmail, c.UserName, d.Name as ServiceName
                                    from bills a left join customers b on a.customerSend = b.ID left join users c on a.userId = c.UserID left join services d on a.serviceId = d.ServiceId
                                    where isDeleted = 0 and billId = ".$id." 
                                    order by billId desc");
        if($query->num_rows() > 0)
            return $query->result()[0];
        else return null;
    }

    public function getByCode($code){
        $query = $this->db->query("select a.*, b.Name as CustomerName, b.Phone as CustomerPhone, b.Address as CustomerAddress, b.Email as CustomerEmail, c.UserName, d.Name as ServiceName
                                from bills a left join customers b on a.customerSend = b.ID left join users c on a.userId = c.UserID left join services d on a.serviceId = d.ServiceId
                                where isDeleted = 0 and billCustomId = '".$code."' 
                                order by billId desc");
        if($query->num_rows() > 0)
            return $query->result()[0];
        else return null;
    }

    public function checkExistsBillId($customId){
        $count = $this->db
                        ->where('isDeleted', false)
                        ->where('billCustomId', $customId)
                        ->count_all_results('bills');           
        if($count > 0)
            return false;
        return true;
    }

    public function getByArray($params){
        $result = array();
        $count = count($params);
        $billIds = array();
        for($i = 0; $i < $count; $i++){
            array_push($billIds, $params[$i]['billCustomId']);
        }
        if(count($billIds) > 0){
            $this->db->where('isDeleted', false);
            $this->db->where_in('billCustomId', $billIds);     
            $this->db->from('bills');       
            $query = $this->db->get();
            if($query->num_rows() > 0)
                return $query->result_array();
            return array();
        }
        return array();
    }

    public function getByProvince($prov_id){
        $query = $this->db->query("select a.* from bills a left join assignment b on b.billCustomId = a.billCustomId
                                    where isDeleted = 0 and a.Status = 0 and b.user_id = 0");
        if($query->num_rows() > 0)
            return $query->result_array();
        else return null;
    }
}
?>