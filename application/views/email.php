<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
</head>

<body>
    <div style="padding:0px;margin:0px">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">

            <tbody>
                <tr>
                    <td height="50" bgcolor="#363636">
                        <table width="700" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tbody>
                                <tr>
                                    <td width="30%" height="50"><img src="http://maydongphucredep.vn/assets/client/img/logo.png" alt="logo Đồng phục Nét Việt" class="CToWUd" style="width:80px; height: auto"></td>
                                    <td width="50%">
                                        <div>
                                            <p style="font-family:tahoma;font-size:12px;color:#b8b8b8;text-align:right">Điện thoại :
                                                <br>Fax :
                                                <br>Mã đơn hàng :
                                        </div>
                                    </td>
                                    <td width="20%" style="border-left:5px #363636 solid">
                                        <div>
                                            <p style="font-family:tahoma;font-size:12px;color:#ffffff;text-align:left"> 0901 402 439                                                
                                                <br><?php echo $orderCode; ?>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td bgcolor="#115c9b" style="border-top:#ff5eaa 2px solid">
                        <table width="700" border="0" cellspacing="0" cellpadding="0" align="center" style="border-top:#115c9b 10px solid;border-bottom:#115c9b 10px solid">
                            <tbody>
                                <tr>
                                    <td>
                                        <h1 style="font-family:tahoma;color:#ffffff;font-size:24px">Thông Báo Đặt Hàng Thành Công</h1>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td bgcolor="#efefef">
                        <table width="700" border="0" cellspacing="0" cellpadding="0" align="center" style="border-bottom:20px solid #efefef">
                            <tbody>                                
                                <tr>
                                    <td height="18" valign="top" style="border-bottom:20px #efefef solid">
                                        <p style="font-family:tahoma;font-size:14px;font-weight:700;color:#363636">Kính chào <?php echo $session_customer['fullName']; ?></p>
                                        <p style="font-family:tahoma;font-size:14px;font-weight:700;color:#363636">Số điện thoại: <?php echo $session_customer['phone']; ?></p>
                                        <p style="font-family:tahoma;font-size:12px;color:#363636;line-height:18px">Cảm ơn Quý khách đã tin tưởng lựa chọn và đồng hành cùng Đồng phục Nét Việt. Hy vọng Quý khách sẽ được tận hưởng trọn vẹn chất lượng sản phẩm mà Đồng phục Nét Việt mang lại. Dưới đây là thông tin đơn hàng của Quý Khách:</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="18" valign="top" bgcolor="#FFFFFF" style="border-bottom:20px #ffffff solid;border-top:20px #ffffff solid;border-left:20px #ffffff solid;border-right:20px #ffffff solid;background:#ffffff">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom:#d8d8d8 1px solid">
                                            <tbody>
                                                <tr bgcolor="#646464">
                                                    <td width="25%" height="50">
                                                        <div align="left" style="font-family:tahoma;font-size:12px;color:#ffffff">&nbsp;&nbsp;&nbsp;Tên sản phẩm</div>
                                                    </td>
                                                    <td width="20%" height="50" style="border-left:#939393 1px solid">
                                                        <div align="center" style="font-family:tahoma;font-size:12px;color:#ffffff">Đơn giá</div>
                                                    </td>
                                                    <td width="20%" height="50" style="border-left:#939393 1px solid;line-height:18px">
                                                        <div align="center" style="font-family:tahoma;font-size:12px;color:#ffffff">Giảm giá</div>
                                                    </td>
                                                    <td width="20%" height="50" style="border-left:#939393 1px solid">
                                                        <div align="center" style="font-family:tahoma;font-size:12px;color:#ffffff">Số lượng</div>
                                                    </td>
                                                    <td width="20%" height="50" style="border-left:#939393 1px solid">
                                                        <div align="center" style="font-family:tahoma;font-size:12px;color:#ffffff">Thành tiền</div>
                                                    </td>
                                                </tr>
                                                <?php
                                                    if(is_array($session_cart) || is_object($session_cart)){
                                                        foreach($session_cart as $item) {
                                                            echo '<tr bgcolor="#ffffff">
                                                            <td height="40">
                                                                <div align="left" style="font-family:tahoma;font-size:12px;color:#363636">
                                                                    '.$item->name.'
                                                                </div>
                                                            </td>
                                                            <td height="40" style="border-left:#cecece 1px solid">
                                                                <div align="center" style="font-family:tahoma;font-size:12px;color:#363636">
                                                                    '.$item->price.'
                                                                </div>
                                                            </td>
                                                            <td height="40" style="border-left:#cecece 1px solid">
                                                                <div align="center" style="font-family:tahoma;font-size:12px;color:#363636">
                                                                    '.$item->sale.'
                                                                </div>
                                                            </td>
                                                            <td height="40" style="border-left:#cecece 1px solid">
                                                                <div align="center" style="font-family:tahoma;font-size:12px;color:#363636">
                                                                    '.$item->quantity.'
                                                                </div>
                                                            </td>
                                                            <td height="40" style="border-left:#cecece 1px solid">
                                                                <div align="center" style="font-family:tahoma;font-size:12px;color:#363636">';
                                                                    if($item->sale > 0)
                                                                        echo $item->quantity * $item->sale;
                                                                    else echo $item->quantity * $item->price;
                                                                echo '</div>
                                                            </td>
                                                        </tr>';
                                                        }
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                        <br>
                                        <p style="width:100%; font-size:15px; font-weight:bold;color:#363636;line-height:20px;margin:5px 0;padding-bottom:10px; text-align:right">
                                            Tổng tiền: <?php echo number_format($sumMoney); ?>
                                        </p>
                                        <p style="font-family:tahoma;font-size:12px;color:#363636;line-height:20px;margin:5px 0;padding-bottom:10px"><b>Lưu Ý:</b> Quý khách kiểm tra lại các thông tin sản phẩm: tên sản phẩm, số lượng, giá <a href="mailto:thuy.maydongphuc@gmail.com" style="text-decoration:none;color:#ff6000;font-weight:bold" target="_blank">identy.com.vn@gmail.com</a> cho chúng tôi ngay khi phát hiện sai hoặc thiếu sót <b>trong vòng 24h</b> kể từ khi nhận thông báo này.
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td bgcolor="#f7f7f7">
                        <table width="700" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tbody>
                                <tr>
                                    <td width="200" height="18" valign="top" style="border-bottom:20px solid #f7f7f7;border-top:20px solid #f7f7f7;border-right:20px solid #f7f7f7;text-align:left">
                                        <p style="font-family:tahoma;font-size:14px;color:#363636;line-height:18px;margin:5px 0 25px 0;font-weight:bold">Hotline hỗ trợ</p>
                                        <p style="font-family:tahoma;font-size:36px;color:#ff0000;line-height:18px;margin:15px 0">0901 402 439</p>
                                        <p style="font-family:tahoma;font-size:14px;color:#363636;line-height:18px;margin:8px 0">Phục vụ khách hàng 24/7</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td bgcolor="#363636" align="center">
                        <table width="700" border="0" cellspacing="0" cellpadding="0" align="center" style="border-top:#727272 1px solid;border-bottom:#363636 10px solid">
                            <tbody>
                                <tr>
                                    <td height="30" colspan="6">
                                        <p style="font-family:tahoma;font-size:14px;font-weight:bold;color:#dedede">Đồng phục Nét Việt</p>
                                        <p style="font-family:tahoma;font-size:12px;color:#dedede">Chuyên Thiết Kế Đồng Phục</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                
            </tbody>
        </table>
        <div class="yj6qo"></div>
        <div class="adL">
        </div>
    </div>
</body>

</html>