<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Vigenere {
    var $keyTable = array();
    
    function __construct(){
        $this->keyTable = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
    }
        
    function createCode($x){		
		$code = '';
        $temp = 0;
		for($i = 0; $i < $x; $i++){			    	
			$temp = rand(1, 26);			
			if($i === 0)
				$code .= $temp;
			else
				$code .= '#' . $temp;
		}
		
		return $code;
	}
    
    function getKey($code){        
		$key = '';
		$arr = array();
		$arr = explode('#', $code);
        $length = count($arr);
		for($i = 0; $i < $length; $i++){
			$key .= $this->keyTable[$arr[$i]];
		}	
        
		return $key;
	}
    
    function createKeyCode($x){		 
		$key = ''; $code = '';		
        try {			
        	$code = $this->createCode($x);				
			$key = $this->getKey($code);
		} catch (Exception $err) {
			// TODO Auto-generated catch block	
            return $err;
		}
        
		return $key;
	}
    
    function encrypt($str){
        $str = $this->toUnsignString($str);     
        $length = strlen($str);
        $key = $this->createKeyCode($length);
        $y = strlen($key);
        $result = "";         
        $n = 0;
        $kt = 0;
        for ($i = 0; $i < $length; $i++) {
            if(ctype_upper($str[$i])){
                $n = ord($str[$i]) - ord('A');
                for ($j = $i; $j < $y; $j++) {
                    if (strcmp($str[$j],'A') < 0 || strcmp($str[$j],'Z') > 0){
                        $result .= $str[$i];                    
                        break;
                    }                
                    if($j < $length){
                        if ((ord($key[$j]) + $n) > 90){
                            $m = 90 - (ord($key[$j]));
                            $n -= $m;
                            $result .= chr(ord('A') + $n - 1);                       
                            break;
                        }
                        else{
                            $kt = ord($key[$j]) + $n;
                            $result .= chr($kt);                       
                            break;
                        }
                    }               
                    else if ($j >= $length)
                        break;
                }
                if ($i >= $y){
                    $result .= $a[$i];                
                }
            }
            else{
                $n = ord($str[$i]) - ord('a');
                for ($j = $i; $j < $y; $j++) {
                    if (strcmp($str[$j],'a') < 0 || strcmp($str[$j],'z') > 0){
                        $result .= $str[$i];                    
                        break;
                    }                
                    if($j < $length){
                        if ((ord($key[$j]) + $n) > 122){
                            $m = 122 - (ord($key[$j]));
                            $n -= $m;
                            $result .= chr(ord('a') + $n - 1);                       
                            break;
                        }
                        else{
                            $kt = ord($key[$j]) + $n;
                            $result .= chr($kt);                       
                            break;
                        }
                    }               
                    else if ($j >= $length)
                        break;
                }
                if ($i >= $y){
                    $result .= $a[$i];                
                }
            }
        }

        $result .= $key;
        return $result;
    }
            
    function decryptString($str){
        $str = $this->toUnsignString($str);
        $length = strlen($str);
        $length = $length / 2;
        $string = substr($str, 0, $length);
        $key = substr($str, $length);
        $y = strlen($key);
        $result = "";        
        $n = 0;
        for ($i = 0; $i < $length; $i++) {
            if(ctype_upper($string[$i])) {
                if (ord($string[$i]) < ord($key[$i]))
                    $n = (90 - ord($key[$i])) + (ord($string[$i]) - ord('A')) + 1;
                else
                    $n = ord($string[$i]) - ord($key[$i]);
                if ($n < 0)
                    $n *= -1;
                for ($j = $i; $j < $y; $j++){
                    if (strcmp($string[$j],'A') < 0 || strcmp($string[$j],'Z') > 0){
                        $result .= $string[$i];                    
                        break;
                    }                
                    if ($j < $length){
                        $result .= chr(ord('A') + $n);                    
                        break;
                    }
                    else if ($j >= $length)
                        break;
                }
                if ($i >= $y){
                    $result .= $string[i];                
                }
            }
            else {
                if (ord($string[$i]) < ord($key[$i]))
                    $n = (122 - ord($key[$i])) + (ord($string[$i]) - ord('a')) + 1;
                else
                    $n = ord($string[$i]) - ord($key[$i]);
                if ($n < 0)
                    $n *= -1;
                for ($j = $i; $j < $y; $j++){
                    if (strcmp($string[$j],'a') < 0 || strcmp($string[$j],'z') > 0){
                        $result .= $string[$i];                    
                        break;
                    }                
                    if ($j < $length){
                        $result .= chr(ord('a') + $n);                    
                        break;
                    }
                    else if ($j >= $length)
                        break;
                }
                if ($i >= $y){
                    $result .= $string[i];                
                }
            }
        }
        return $result;
    }

    function toUnsignString ($str){        
        $unicode = array(        
            'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',        
            'd'=>'đ',        
            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',        
            'i'=>'í|ì|ỉ|ĩ|ị',        
            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',        
            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',        
            'y'=>'ý|ỳ|ỷ|ỹ|ỵ',        
            'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',        
            'D'=>'Đ',        
            'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',        
            'I'=>'Í|Ì|Ỉ|Ĩ|Ị',        
            'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',        
            'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',        
            'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',        
        );        
        foreach($unicode as $nonUnicode=>$uni){            
            $str = preg_replace("/($uni)/i", $nonUnicode, $str);            
        }                   
        return trim($str);        
    }
}

?>