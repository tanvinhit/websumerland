<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientcontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
    }
    
    public function index(){        
        $this->load->view('client/index.html');  
    }
    
    public function loadView($view, $folder = ''){
        if($folder !== '')
            $view = $folder.'/'.$view;
        $this->load->view('client/'.$view.'.html');    
    }
    
}

?>