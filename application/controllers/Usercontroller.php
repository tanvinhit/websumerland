<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usercontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');        
        $this->load->library(array('phpsession', 'vigenere'));
        $this->load->model('Usermodel');
    }
            
    public function actionLogin(){
        $request = $this->input->post('data');
        $Account = $request['Account'];
        $password = $this->vigenere->decryptString($request['password']);  
        $rememberMe = $request['isRemember'];
                           
        $result = $this->Usermodel->loginAccount($Account, $password);   
                    
        if($result === null){
            $error = array(
                'message' => 'Account và mật khẩu không đúng!'
            );
            $json = json_encode($error);
            echo $json;
        }
        else{            
            $session_user = array(
                'IdUser' => $result[0]['UserID'],
                'Username' => $result[0]['UserName'],                
                'Phone' => $result[0]['Phone'],
                'Email' => $result[0]['Email'],                
                'Level' => $result[0]['Permission'],
                'Avatar' => $result[0]['Avatar'],
                'Password' => $request['password'],
                'Remember' => $rememberMe,
            );                            
            $this->phpsession->save(null, $session_user, 'monpham_user');
            $success = array(
                'state' => 'OK',
                'link' => base_url().'admin'
            );
            $json = json_encode($success);
            echo $json;               
        }
    }
    
    public function actionLogout(){        
        $this->phpsession->clear(null, 'monpham_user');
        echo 'Success!';
    }
    
    public function getCurrentUser(){                
        $session = $this->phpsession->get(null, 'monpham_user');          
        if($session !== null){
            $return_session = array(
                'IdUser' => $session['IdUser'],
                'Username' => $session['Username'],                
                'Phone' => $session['Phone'],
                'Email' => $session['Email'],                
                'Level' => $session['Level'],
                'Avatar' => $session['Avatar'],                
                'Remember' => $session['Remember']
            );
            $json = json_encode($return_session); 
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login); 
            echo $json;
        }        
    }
    
    public function destroySession(){
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $isRemember = $session['Remember'];
            if($isRemember === 'false'){
                $this->phpsession->clear(null, 'monpham_user');
            }
        }
    }
    
    public function loadUsers(){
        $request = $this->input->post('data');
        $type = isset($request['type'])? $request['type'] : '';
        $result = $this->Usermodel->getAllUsers($type);
        $json = '';        
        $json = json_encode($result);            
                
        echo $json;
    }
    
    public function updateUser(){
        $request = $this->input->post('data');
              
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = array();
            if(isset($request['Password']))
                $request['Password'] = $this->vigenere->decryptString($request['Password']);
            $result = $this->Usermodel->updateUser($request);  
            $userCurr = $session['IdUser'];
            if($request['UserID'] === $userCurr){
                $remember = $session['Remember'];
                $session_user = array(
                    'IdUser' => $result[0]['UserID'],
                    'Username' => $result[0]['UserName'],
                    'Phone' => $result[0]['Phone'],
                    'Email' => $result[0]['Email'],  
                    'Password' => (isset($request['Password'])? $request['Password'] : $session['Password']),                  
                    'Level' => $result[0]['Permission'],
                    'Avatar' => $result[0]['Avatar'],                                        
                    'Remember' => $remember,
                    'Account' => $result[0]['Account'],
                );              
                $this->phpsession->save(null, $session_user, 'monpham_user');
            }            
            $json = json_encode($result);            
            
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
    
    public function addUser(){
        $request = $this->input->post('data');                
          
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $request['Password'] = $this->vigenere->decryptString($request['Password']);  
            $result = $this->Usermodel->addUser($request);            
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
    
    public function deleteUser(){
        $request = $this->input->post('data');
        $userId = $request['userId'];
                
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Usermodel->deleteUser($userId);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }
    
    public function checkEmail(){
        $request = $this->input->post('data');
        $email = $request['email'];
        $json = '';
        $result = $this->Usermodel->checkEmail($email);
        $json = json_encode($result);
        echo $json;
    }

    public function checkAccount(){
        $request = $this->input->post('data');
        $account = $request['Account'];
        $json = '';
        $result = $this->Usermodel->checkAccount($account);
        $json = json_encode($result);
        echo $json;
    }

    public function searchProvince(){
        $request = $this->input->post('data');
        $name = $request['name'];
        $json = '';
        $result = $this->Usermodel->searchProvince($name);
        $arr = array_column($result,"prov_name");
        $json = json_encode($arr);
        echo $json;
    }

    public function searchPostman(){
        $request = $this->input->post('data');
        $name = $request['name'];
        $prov_name = $request['prov_name'];
        $json = '';
        $result = $this->Usermodel->searchPostman($name,$prov_name);
        $arr = array_column($result,"UserName");
        $json = json_encode($arr);
        echo $json;
    }

    public function checkProvince(){
        $request = $this->input->post('data');
        $prov_name = $request['prov_name'];
        $json = '';
        $result = $this->Usermodel->checkProvince($prov_name);
        $json = json_encode($result);
        echo $json;
    }

    public function getByProvince(){
        $request = $this->input->post('data');
        $prov_id = $request['prov_id'];
        $query = $this->Usermodel->getByProvince($prov_id);
        if($query)
            echo json_encode($query);
        else echo null;
    }
}

?>