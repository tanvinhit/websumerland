<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customercontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->library('session');
        $this->load->library('phpsession');
        $this->load->model('Customermodel');
    }
                    
    public function getCustomers($lang = null){
        $request = $this->input->post('data');
        $type = $request['type'];
        $result = $this->Customermodel->getAllCustomers($type, $lang);                
        $json = json_encode($result);                            
        echo $json;
    }
    
    public function updateCustomer(){
        $request = $this->input->post('data');
        $Id = $request['ID'];
        $name = $request['Name'];
        $company = ($request['companyName'])? $request['companyName'] : '';
        $est = ($request['Estimate'])? $request['Estimate'] : '';
        $img = isset($request['Image'])? $request['Image'] : '';
        $addr = isset($request['Address'])? $request['Address'] : '';
        $phone = isset($request['Phone'])? $request['Phone'] : '';
        $email = isset($request['Email'])? $request['Email'] : '';   
        $district = isset($request['district'])? $request['district'] : '';   
        $districtDetail = isset($request['districtDetail'])? $request['districtDetail'] : '';  
        $province = isset($request['province'])? $request['province'] : '';    
        $lang = $request['Language'];
                               
        //$session = $this->session->has_userdata('remember_me');        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Customermodel->updateCustomer($Id, $name, $company, $est, $img, $addr, $phone, $lang, $email, $district, $districtDetail, $province);            
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
    
    public function addCustomer(){
        $request = $this->input->post('data');                                
        $name = $request['Name'];
        $company = ($request['companyName'])? $request['companyName'] : '';
        $est = isset($request['Estimate'])? $request['Estimate'] : '';
        $img = isset($request['Image'])? $request['Image'] : '';
        $addr = isset($request['Address'])? $request['Address'] : '';
        $phone = isset($request['Phone'])? $request['Phone'] : '';
        $email = isset($request['Email'])? $request['Email'] : '';    
        $district = isset($request['district'])? $request['district'] : '';   
        $districtDetail = isset($request['districtDetail'])? $request['districtDetail'] : '';        
        $province = isset($request['province'])? $request['province'] : '';    
        $lang = $request['Language'];
        
        //$session = $this->session->has_userdata('remember_me');   
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Customermodel->addCustomer($name, $company, $est, $img, $addr, $phone, $lang, $email, $district, $districtDetail, $province);            
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
    
    public function deleteCustomer(){
        $request = $this->input->post('data');
        $Id = $request['ID'];
        
        //$session = $this->session->has_userdata('remember_me');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Customermodel->deleteCustomer($Id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }

    public function deleteMultiCategory(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Customermodel->deleteCustomer($request[$i]);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
    
    public function getById($customerId){
        $result = $this->Customermodel->getById($customerId);
        $json = json_encode($result);
        echo $json;
    }
}

?>