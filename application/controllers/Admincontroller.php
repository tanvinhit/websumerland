<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admincontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library(array('phpsession', 'vigenere'));
        $this->load->model('Usermodel');
    }
    
    public function index(){        
        $check_session = $this->phpsession->get(null, 'monpham_user');
        if($check_session !== null){
            $email = $check_session['Email'];
            $pwd = $this->vigenere->decryptString($check_session['Password']);
            $isLogin = $this->Usermodel->checkSessionUser($email, $pwd);
            if($isLogin)
                $this->load->view('admin/index.html');                
            else{
                $this->phpsession->clear(null, 'monpham_user');
                $this->load->view('admin/login.html');     
            }
        }
        else{
            $this->load->view('admin/login.html'); 
        }
    }
    
    public function loadView($view, $folder = ''){
        if($folder !== '')
            $view = $folder.'/'.$view;
        $this->load->view('admin/'.$view.'.html');    
    }
    
}

?>