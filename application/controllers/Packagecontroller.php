<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Packagecontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->library('session');
        $this->load->library('phpsession');
        $this->load->model('Packagemodel');
    }
                    
    public function getAll(){
        $result = $this->Packagemodel->getAll();                
        $json = json_encode($result);                            
        echo $json;
    }

    public function getTappingTo(){
        $result = $this->Packagemodel->getTappingTo();                
        $json = json_encode($result);                            
        echo $json;
    }

    public function getById($id){
        $id = intval($id);
        $query = $this->Packagemodel->getById($id);
        
        if($query)
            echo json_encode($query);
        else echo false;
    }

    public function getByCode($id){
        $query = $this->Packagemodel->getByCode($id);
        
        if($query)
            echo json_encode($query);
        else echo false;
    }

    public function getDetailById($id){
        $id = intval($id);
        $query = $this->Packagemodel->getDetailById($id);
        
        if($query)
            echo json_encode($query, JSON_NUMERIC_CHECK);
        else echo false;
    }
    
    public function update(){
        $request = $this->input->post('data');
        
        //$session = $this->session->has_userdata('remember_me');        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $request['isDeleted'] = false;
            unset($request['serviceName']);
            $result = $this->Packagemodel->update($request);            
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
    
    public function insert(){
        $data = $this->input->post('data');                                
        
        //$session = $this->session->has_userdata('remember_me');   
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $isExists = $this->Packagemodel->checkExistsId($data['packageCustomId']);
            if($isExists){
                $query = $this->Packagemodel->insert($data);
                $result = array(
                    'success' => true,
                    'data' => $query
                );
                $json = json_encode($result);
                echo $json;
            }
            else{
                $result = array(
                    'success' => false,
                    'message' => 'Mã phiếu đã tồn tại. Vui lòng đổi mã khác'
                ); 
                $json = json_encode($result);
                echo $json;
            }
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }

    public function insertDetail(){
        $data = $this->input->post('data');       
        if(isset($data)){                         
            $countList = count($data);
            //$session = $this->session->has_userdata('remember_me');   
            $session = $this->phpsession->get(null, 'monpham_user');
            if($session !== null){
                for($i = 0; $i < $countList; $i++){
                    $query = $this->Packagemodel->insertDetail($data[$i]);
                }
                $result = array(
                    'success' => true,
                    'message' => 'Thêm chi tiết tải thành công'
                );
                $json = json_encode($result);
                echo $json;
            }
            else{
                $not_login = array(
                    'redirect' => base_url().'login'
                );
                $json = json_encode($not_login);
                echo $json;
            }
        }
        else{
            $result = array(
                'success' => true,
                'message' => 'Thêm chi tiết tải thành công'
            );
            $json = json_encode($result);
            echo $json;
        }
    }
    
    public function delete($Id){
        //$session = $this->session->has_userdata('remember_me');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Packagemodel->delete($Id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }
    
    public function deleteMulti(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Packagemodel->delete($request[$i]);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }

    public function deleteDetail($Id){        
        //$session = $this->session->has_userdata('remember_me');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Packagemodel->deleteDetail($Id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }
    
    public function deleteMultiDetail(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Packagemodel->deleteDetail($request[$i]['packDetailId']);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }

    public function tappingToBill($packDetailId, $isReceived){
        //$session = $this->session->has_userdata('remember_me');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Packagemodel->tappingToBill($packDetailId, $isReceived);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }

    public function tappingToAllBill(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Packagemodel->tappingToBill($request[$i]['packDetailId'], $request[$i]['isReceived']);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
}

?>