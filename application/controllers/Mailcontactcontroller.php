<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailcontactcontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library(array('vigenere','phpsession','emailhandler'));
        $this->load->model('Mailcontactmodel');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }
                    
    public function getMailList(){
        $request = $this->input->post('data');        
        $result = $this->Mailcontactmodel->getMailList();                
        $json = json_encode($result);
        echo $json;
    }
            
    public function addMail(){
        $request = $this->input->post('data');                                
        $name = $request['Name'];
        $email = $request['Email'];
        $to_email = "tanvinhit@gmail.com";
        $sub = "Thông tin user của Summerland";
        $content = $request['Content'];
        $date = $request['DateSent'];
        $phone = $request['Phone'];                
        $deviceType = $request['DeviceType'];                
        $ip = $request['IP'];                
        $result = $this->Mailcontactmodel->addMail($name, $email, $sub, $content, $date, $phone, $deviceType, $ip);  
       
        $message = '<div style="background-color: #eeeeef; padding: 50px 0; "><div style="max-width:640px; margin:0 auto; "> <div style="color: #fff; text-align: center; background-color:#33333e; padding: 30px; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0;">  <h1>THÔNG TIN KHÁCH HÀNG</h1></div><div style="padding: 20px; background-color: rgb(255, 255, 255);">            <p style="color: rgb(85, 85, 85); font-size: 14px;"><b>Tên: </b>'.$name.',<br><br><b>Số điện thoại: &nbsp;</b> '.$phone.'</p>            <p style="color: rgb(85, 85, 85); font-size: 14px;"><b> Email: </b> '.$email.'</p>            <hr>            <p style="color: rgb(85, 85, 85); font-size: 14px;"><b>Nội dung: &nbsp;</b> '.$content.'</p>            <p style="color: rgb(85, 85, 85); font-size: 14px;"></p>            <p style=""><span style="color: rgb(85, 85, 85); font-size: 14px; line-height: 20px;"><b>IP: &nbsp;</b></span> '.$ip.'<br> </p>            <p style=""><font color="#555555"><span style="font-size: 14px;"><b>Thiết bị: &nbsp;</b></span></font> '.$deviceType.'</p> <p style=""><font color="#555555"><span style="font-size: 14px;"><b>Ngày gửi: &nbsp;</b></span></font> '.$date.'</p>        </div>    </div></div>';
        if(isset($email))
            $this->load->library('email');

            $config['protocol']    = 'smtp';
            $config['smtp_host']    = 'ssl://smtp.gmail.com';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '7';
            $config['smtp_user']    = 'tanvinhit@gmail.com';
            $config['smtp_pass']    = 'femxvuwipanvahxv';
            $config['charset']    = 'utf-8';
            $config['newline']    = "\r\n";
            $config['mailtype'] = 'html'; // or html
            $config['validation'] = TRUE; // bool whether to validate email or not      

            $this->email->initialize($config);

            $this->email->from('tanvinhit@gmail.com', 'SUMMERLAND');
            $this->email->to($email); 
            $this->email->subject('DATA KHÁCH HÀNG SUMMERLAND');
            $this->email->message($message); 
        
                //send email
                if ($this->email->send()) {
                    echo json_encode(array('success' => true, 'message' => $email));
                } else {
                    echo json_encode(array('success' => false, 'message' => 'error_occurred'));
                }
    }
    
    public function deleteMail(){
        $request = $this->input->post('data');
        $Id = $request['ID'];
        $result = $this->Mailcontactmodel->deleteMail($Id);
        $json = json_encode($result);
        echo $json;                    
    }
    
    public function deleteMultiContact(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Mailcontactmodel->deleteMail($request[$i]);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
        
    public function addMail_new(){
        $request = json_decode(file_get_contents('php://input'), true);
        $name = $request['Name'];
        $email = isset($request['Email']) ? $request['Email'] : '';
        $sub = isset($request['Subject']) ? $request['Subject'] : '';
        $content = isset($request['Content']) ? $request['Content'] : '';
        $date = date('Y-m-d H:i:s');
        $phone = $request['Phone'];
        $result = $this->Mailcontactmodel->addMail($name, $email, $sub, $content, $date, $phone);
        
        $message = '<h3 class="cart-title">Thông tin liên hệ</h3><div class="col-md-12 customer-inform"> <div class="form-group"> <strong>Tên khách hàng</strong> '.$name.' </div> <div class="form-group"> <strong>Điện thoại</strong> '.$phone.' </div> <div class="form-group"> <strong>Email</strong> '.$email.' </div> <div class="form-group"> <strong>Tiêu đề</strong> '.$sub.' </div> <div class="form-group"> <strong>Nội dung</strong> '.$content.' </div> </div>';

        if(isset($email))
            $this->emailhandler->sendEmail('TheHeMoi Express Co .,Ltd', 'Liên hệ', 'info@thehemoiexpress.vn', $message);
        
        $json = json_encode($result);
        echo $json;
    }        
}

?>