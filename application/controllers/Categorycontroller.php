<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categorycontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->library('session');
        $this->load->library('phpsession');
        $this->load->model('Categorymodel');
        $this->load->model('Articlemodel');
    }
        
    public function loadAllCategories($lang = null){
        $request = $this->input->post('data');
        $type = $request['type'];        
        $result = $this->Categorymodel->getAllCategories($type, $lang, 0);                
        $json = json_encode($result);                    
        
        echo $json;
    }
    
    //function get cate and product on home
    function showProCateOnHome($query){
        foreach($query as $cate => $item){
            echo '<div class="heading not-visible" when-visible="sidebarRight">
                    <div class="container-body gradient-left-to-right">
                        <h4 class="sidebar">'.$item['CatName'].'</h4>
                    </div>
                    <div class="containter-content-body center">                    
                    <div class="blog-one row">';
                $product = $this->Articlemodel->getArtForCate($item['CatID']);
                foreach($product as $pro => $proItem){
                    echo '<div class="col-md-3 welcome-left animated pulse">
                            <a light-box class="example-image-link no-padding" href="#" data-lightbox="example-set" data-title="">
                                <img class="img-responsive" ng-model="item.Image" ng-init="item.Image = '."'".$proItem['Image']."'".'" src="'.$proItem['Image'].'" alt="'.$proItem['ArtName'].'" />
                            </a>
                                <div class="welcome-btm">
                                    <a href="'.$proItem['ArtMeta'].'">'.$proItem['ArtName'].'</a>
                                </div>
                            </div>';
                }
            echo '</div>
                    <a href="'.$item['CatMeta'].'" translate="SEE_MORE"></a>                    
                </div>
            </div>';
        }
    }
    
    function showCateOnHome($query){       
        foreach($query as $cate => $item){
            echo '<div class="news-top heading">
                        <h3 class="animated fadeInDown">'.$item['CatName'].'</h3>
                        <hr>
                        <p class="animated fadeInUp">'.$item['CatDescribes'].'</p>
                    </div>
                    <div class="news-bottom">';
                $product = $this->Articlemodel->getArtForCate($item['CatID']);
                foreach($product as $pro => $proItem){
                    echo '<div class="col-md-4 news-left animated bounceIn">
                            <a light-box class="example-image-link" href="#" data-lightbox="example-set" data-title="">
                                <img class="img-responsive" ng-model="item.Image" ng-init="item.Image = '."'".$proItem['Image']."'".'" src="'.$proItem['Image'].'" alt="'.$proItem['ArtName'].'" />
                            </a>
                                <div class="mask">
                                    <a href="'.$proItem['ArtMeta'].'"><h4>'.$proItem['ArtName'].'</h4></a>
                                    <p>'.$proItem['ArtDescribes'].'</p>
                                </div>
                            </div>';
                }
            echo '<div class="clearfix"></div>
                </div>';
        }
    }
    
    public function loadCategories($lang = null){
        $request = $this->input->post('data');
        $type = $request['type'];
        $dataType = isset($request['dataType'])? $request['dataType'] : 0;
        $result = $this->Categorymodel->getCategories($type, $lang, $dataType);  
        if($type === 'admin'){
            $json = json_encode($result);                            
            echo $json;
        }
        else{
            echo $this->showCateOnHome($result);
        }
    }
            
    public function updateCategory(){
        $request = $this->input->post('data');
        $data = array(
            'CatID' => $request['CatID'],
            'CatName' => $request['CatName'],
            'CatMeta' => $request['CatMeta'],
            'CatDescribes' => isset($request['CatDescribes'])? $request['CatDescribes'] : '',
            'TempId' => $request['TempId'],
            'CatLang' => $request['CatLang'],
            'Type' => $request['Type'],
            'CatParent' => isset($request['CatParent'])? $request['CatParent'] : 0,
            'Image' => isset($request['Image'])? $request['Image'] : '',
            'SeoTitle' => isset($request['SeoTitle'])? $request['SeoTitle'] : '',
            'SeoKeyword' => isset($request['SeoKeyword'])? $request['SeoKeyword'] : '',
            'SeoDescribes' => isset($request['SeoDescribes'])? $request['SeoDescribes'] : '',
            'SeoCanonica' => isset($request['SeoCanonica'])? $request['SeoCanonica'] : '',
            'MetaRobot' => isset($request['MetaRobot'])? $request['MetaRobot'] : ''
        );
        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Categorymodel->updateCategory($data);
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
    
    public function addCategory(){
        $request = $this->input->post('data');                       
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session != null){
            $result = $this->Categorymodel->addCategory($request);
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
    
    public function deleteCategory(){
        $request = $this->input->post('data');
        $cateId = $request['cateId'];
        
        //$session = $this->session->has_userdata('remember_me');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Categorymodel->deleteCategory($cateId);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }
    
    function showNestableCate($query, $parent = 0, $child = false){
        $menu_tmp = array();

        foreach ($query as $key => $item) {
            // Nếu có parent bằng với $parent hiện tại
            if ((int) $item['CatParent'] == (int) $parent) {
                $menu_tmp[] = $item;
                // Sau khi thêm vào biên lưu trữ menu ở bước lặp
                // thì unset nó ra khỏi danh sách menu ở các bước tiếp theo
                unset($query[$key]);
            }
        }

        # BƯỚC 2: lẶP MENU THEO DANH SÁCH MENU Ở BƯỚC 1
        // Điều kiện dừng của đệ quy là cho tới khi menu không còn nữa
        if ($menu_tmp) 
        {      
            if(!$child)
                echo '<ul>';
            else
                echo '<ul class="child">';
            foreach ($menu_tmp as $item) 
            {
                echo '<li class="margin-bottom-left"><a href="'.$item['CatMeta'].'">'.$item['CatName'].'</a></li>';                
                // Gọi lại đệ quy
                // Truyền vào danh sách menu chưa lặp và parent của menu hiện tại
                $this->showNestableCate($query, $item['CatID'], true);
                echo '</li>';
            }
            echo '</ul>';
        }
    }
    
    public function getNestableCate($type){
        $result = $this->Categorymodel->getAllCategories('client', null, $type);
        echo $this->showNestableCate($result);
    }
    
    public function getCateOrProCate(){
        $request = $this->input->post('data');
        $meta = $request['meta'];
        $json = '';
        $result = $this->Categorymodel->getCategory($meta);
        $json = json_encode($result);
        echo $json;
    }

    public function editCategory($catId){
        $result = $this->Categorymodel->editCategory($catId);
        $json = json_encode($result);
        echo $json;
    }
    
    public function deleteMultiCategory(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Categorymodel->deleteCategory($request[$i]);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
            
}

?>