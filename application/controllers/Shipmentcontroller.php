<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipmentcontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->library('session');
        $this->load->library('phpsession');
        $this->load->model('Shipmentmodel');
    }
                    
    public function getAll(){
        $result = $this->Shipmentmodel->getAll();                
        $json = json_encode($result);                            
        echo $json;
    }

    public function getTappingTo(){
        $result = $this->Shipmentmodel->getTappingTo();
        $json = json_encode($result);                            
        echo $json;
    }

    public function getById($id){
        $id = intval($id);
        $query = $this->Shipmentmodel->getById($id);
        
        if($query)
            echo json_encode($query);
        else echo false;
    }

    public function getDetailById($id){
        $id = intval($id);
        $query = $this->Shipmentmodel->getDetailById($id);
        
        if($query)
            echo json_encode($query, JSON_NUMERIC_CHECK);
        else echo false;
    }
    
    public function update(){
        $request = $this->input->post('data');
        
        //$session = $this->session->has_userdata('remember_me');        
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $request['isDeleted'] = false;
            $result = $this->Shipmentmodel->update($request);            
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
    
    public function insert(){
        $data = $this->input->post('data');                                
        
        //$session = $this->session->has_userdata('remember_me');   
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $isExists = $this->Shipmentmodel->checkExistsId($data['shipmentCustomId']);
            if($isExists){
                $query = $this->Shipmentmodel->insert($data);
                $result = array(
                    'success' => true,
                    'data' => $query
                );
                $json = json_encode($result);
                echo $json;
            }
            else{
                $result = array(
                    'success' => false,
                    'message' => 'Mã phiếu đã tồn tại. Vui lòng đổi mã khác'
                ); 
                $json = json_encode($result);
                echo $json;
            }
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }

    public function insertDetail(){
        $data = $this->input->post('data');                                
        $countList = count($data);
        //$session = $this->session->has_userdata('remember_me');   
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $query = $this->Shipmentmodel->insertDetail($data[$i]);
            }
            $result = array(
                'success' => true,
                'message' => 'Thêm chi tiết tải thành công'
            );
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
    
    public function delete($Id){
        //$session = $this->session->has_userdata('remember_me');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Shipmentmodel->delete($Id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }

    public function tappingToPackage($shipDetailId, $isReceived){
        //$session = $this->session->has_userdata('remember_me');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Shipmentmodel->tappingToPackage($shipDetailId, $isReceived);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }
    
    public function deleteMulti(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Shipmentmodel->delete($request[$i]);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }

    public function tappingToAllPackage(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Shipmentmodel->tappingToPackage($request[$i]['shipDetailId'], $request[$i]['isReceived']);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }

    public function deleteDetail($Id){        
        //$session = $this->session->has_userdata('remember_me');
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Shipmentmodel->deleteDetail($Id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }
    
    public function deleteMultiDetail(){
        $request = $this->input->post('data');
        $countList = count($request);
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            for($i = 0; $i < $countList; $i++){
                $this->Shipmentmodel->deleteDetail($request[$i]['shipDetailId']);
            }
            echo json_encode(true);
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
}

?>