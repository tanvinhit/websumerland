<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materplandetailcontroller extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');        
        $this->load->library(array('phpsession', 'vigenere'));
        $this->load->model('Materplandetailmodel');
    }
    
    public function getAll(){
        $result = $this->Materplandetailmodel->getAll();
        $json = '';        
        $json = json_encode($result);   
        echo $json;
    }
    
    public function updateMasterPlanDetail(){
        $request = $this->input->post('data');
            
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = array();
            $result = $this->Materplandetailmodel->update($request); 
            $json = json_encode($result);
            echo $json; 
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }                
    }
    
    public function insertMasterPlanDetail(){
        $request = $this->input->post('data'); 
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Materplandetailmodel->insert($request);          
            $json = json_encode($result);
            echo $json;    
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }
    }
    
    public function deleteMasterPlanDetail(){
        $request = $this->input->post('data');
        $id = $request['id'];     
        $session = $this->phpsession->get(null, 'monpham_user');
        if($session !== null){
            $result = $this->Materplandetailmodel->delete($id);
            $json = json_encode($result);
            echo $json;
        }
        else{
            $not_login = array(
                'redirect' => base_url().'login'
            );
            $json = json_encode($not_login);
            echo $json;
        }            
    }

    public function editMasterPlanDetail($Id){
        $result = $this->Materplandetailmodel->edit($Id);
        $json = json_encode($result);
        echo $json;
    }
}

?>